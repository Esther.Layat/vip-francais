﻿SetLayerMedia(background bureau-jour)
nameCharacter1.Show()
ShowDB()
Pensées "Chapitre 3 : {wa 1}Alfred {c} Mardi 1er septembre 2024, 7h30"
nameCharacter1.SetPosition(0.2)
nameCharacter1.FaceRight()
nameCharacter1.SetSprite(normal -spd 0.75)
playsfx(grincement-porte)
nameCharacter1.Highlight()
nameCharacter1.Show()
PlayVoice(1-nc1-rentrer)
<nameCharacter1> "(Un matin de plus ici, j’aimerais bien rentrer chez moi…)"
StopVoice(1-nc1-rentrer)
PlayVoice(2-nc1-mois)
"Ça fait déjà presqu’un mois que je suis là."
StopVoice(2-nc1-mois)
nameCharacter2.SetPosition(0.8)
nameCharacter2.FaceLeft()
nameCharacter2.SetSprite(normal -spd 0.75)
playsfx(pas)
wait(1)
PlaySong(MusMus-BGM-116 -v 0.05)
namecharacter2.Highlight()
nameCharacter2.Show()
nameCharacter1.Unhighlight()
PlayVoice(3-nc2-toiaussi)
<nameCharacter2> "Salut, toi aussi tu es encore là aujourd’hui ?"
StopVoice(3-nc2-toiaussi)
nameCharacter1.Highlight()
nameCharacter2.Unhighlight()
nameCharacter1.SetSprite(normal -spd 0.75)
PlayVoice(4-nc1-quitter)
<nameCharacter1> "Oui, mais j’ai pas prévu d’y rester longtemps. {wa 1}Je vais bien faire mon travail et j’espère vite quitter cette agence. {wa 1}Ils sont gentils, mais bon …"
StopVoice(4-nc1-quitter)
nameCharacter2.Highlight()
nameCharacter1.Unhighlight()
PlayVoice(5-nc2-cafe)
<nameCharacter2> "Oui moi aussi. {wa 1}Bon je vais me chercher un café, tu en veux un aussi ?"
StopVoice(5-nc2-cafe)
nameCharacter1.Highlight()
nameCharacter2.Unhighlight()
nameCharacter1.SetSprite(sourit -spd 0.75)
PlayVoice(6-nc1-plaisir)
<nameCharacter1> "Avec plaisir !"
StopVoice(6-nc1-plaisir)
nameCharacter1.SetSprite(normal -spd 0.75)
playsfx(pas)
nameCharacter2.Hide(-i true -spd 0.75)
wait(1)
nameCharacter1.Hide(-i true -spd 0.75)
HideDB()
StopSong()
wait(2)

SetLayerMedia(background -m hall1)
nameCharacter1.Show(-i true -spd 0.75)
Alfred.FaceLeft()
Alfred.SetPosition(1.6 0.2)
Alfred.SetSprite(perdu1 -spd 0.75)
wait(2)
PlaySong(MusMus-BGM-091 -v 0.05)
Alfred.Show(-i true -spd 1.5)
wait(1.5)
ShowDB()
PlayVoice(7-nc1-perdu)
<nameCharacter1> "(Ce monsieur semble un peu perdu, {wa 0.5} je vais l’aider.)"
StopVoice(7-nc1-perdu)
Alfred.Unhighlight()
Alfred.SetSprite(perdu2 -spd 0.75)
PlayVoice(8-nc1-aider)
<nameCharacter1> "Bonjour Monsieur, puis-je vous aider ?"
StopVoice(8-nc1-aider)
Alfred.FaceRight()
Alfred.Highlight()
nameCharacter1.Unhighlight()
Alfred.SetSprite(normal -spd 0.75)
PlayVoice(9-alfred-demande)
Alfred as ?? "Oh bonjour. {wa 1}En effet, je dois réserver une salle chez <i>Book’N’Roll</i> pour y organiser une conférence sur les molécules chimiques, {wa 1}mais ils ne prennent plus que les réservations en ligne. {wa 1}Et vous savez, je n’ai jamais été vraiment à l’aise avec tout ça moi, {wa 0.5} il faudrait que vous m’aidiez."
StopVoice(9-alfred-demande)
Alfred.Unhighlight()
nameCharacter1.Highlight()
nameCharacter1.SetSprite(smile_office -spd 0.75)
PlayVoice(10-nc1-evidemment)
<nameCharacter1> "Évidemment Monsieur, je vais vous aider à réserver cette salle. {wa 1}Venez avec moi au bureau."
StopVoice(10-nc1-evidemment)
StopSong()

HideDB()
Hide(nameCharacter1 Alfred -i true -spd 0.75)
ClearLayerMedia(background -m)
wait(1)
SetLayerMedia(background -m bureau-jour)
nameCharacter1.SetSprite(normal_office -spd 0.75)
Show(nameCharacter1 Alfred -i true -spd 0.75)
wait(0.75)
PlaySong(MusMus-BGM-124 -v 0.05)
ShowDB()
PlayVoice(11-nc1-allumer)
<nameCharacter1> "Allumez votre ordinateur et rendez-vous sur le site de <i>Book’N’Roll</i>."
StopVoice(11-nc1-allumer)

choice "Ensuite, que faut-il dire à Alfred pour l’aider ?"
{
	-N’y a-t-il vraiment aucune autre méthode pour réserver ?
		PlayVoice(12-nc1-autremethode)
		<nameCharacter1> "N’y a-t-il vraiment aucune autre méthode pour réserver ?"
		StopVoice(12-nc1-autremethode)
		playsfx(moinsjauge -v 0.3)
		$VN.score -= 1
		Jauge.SetSprite($VN.score -spd 0.4)
		
		nameCharacter1.Unhighlight()
		Alfred.Highlight()
		Alfred.SetSprite(fache -spd 0.75)
		PlayVoice(13-alfred-fache)
		Alfred "Je vous l’ai dit, ils ne prennent que les réservations en ligne… {wa 1}Et puis, si je viens ici c’est pour que vous m’aidiez non ?"
		StopVoice(13-alfred-fache)
		nameCharacter1.Highlight()
		Alfred.Unhighlight()
		namecharacter1.SetSprite(confused_office -spd 0.75)
		PlayVoice(14-nc1-excuse)
		<nameCharacter1> "Vous avez totalement raison, excusez-moi monsieur. {wa 0.5} Nous allons réserver cette salle ensemble. {wa 1}Il est souvent nécessaire d’avoir un compte pour réserver ce genre de salle, {wa 0.5} avez-vous déjà un compte chez <i>Book’N’Roll</i> ?"
		StopVoice(14-nc1-excuse)

 
	-Avez-vous déjà un compte chez <i>Book’N’Roll</i> ? 
		PlayVoice(15-nc1-compte)
		<nameCharacter1> "Il est souvent nécessaire d’avoir un compte pour réserver ce genre de salle, avez-vous déjà un compte chez <i>Book’N’Rooll</i> ?"
		StopVoice(15-nc1-compte)
		playsfx(+jauge -v 0.8)
		$VN.score += 1
		Jauge.SetSprite($VN.score -spd 0.4)
		

 
	-Commencez par vous connecter à votre compte.
		PlayVoice(16-nc1-connecter)
		<nameCharacter1> "Commencez par vous connecter à votre compte."
		StopVoice(16-nc1-connecter)
}

nameCharacter1.Unhighlight()
Alfred.Highlight()
Alfred.SetSprite(normal -spd 0.75)
PlayVoice(17-alfred-pascompte)
Alfred "Je n’ai pas encore de compte."
StopVoice(17-alfred-pascompte)
nameCharacter1.Highlight()
Alfred.Unhighlight()
nameCharacter1.SetSprite(normal_office -spd 0.75)
PlayVoice(18-nc1-creercompte)
<nameCharacter1> "D’accord, nous allons alors commencer par vous créer un compte."
StopVoice(18-nc1-creercompte)

choice "Alfred doit se créer un compte, comment le guider ?"
{
	-Allez sur la page de connexion et entrez vos informations.
		PlayVoice(19-nc1-pageconnexion)
		<nameCharacter1> "Allez sur la page de connexion, et entrez vos informations."
		StopVoice(19-nc1-pageconnexion)
		nameCharacter1.Unhighlight()
		Alfred.Highlight()
		Alfred.SetSprite(étonné -spd 0.75)
		PlayVoice(20-alfred-comprendpas)
		Alfred "Heu, mais je ne comprends pas, c’est où la page de connexion ?"
		StopVoice(20-alfred-comprendpas)
		nameCharacter1.Highlight()
		Alfred.Unhighlight()
		PlayVoice(21-nc1-pasclaire)
		<nameCharacter1> "Je n’ai pas été assez claire, excusez-moi. {wa 1}Cliquez sur le bouton <br>« s’inscrire » et remplissez les informations qui vous sont demandées."
		StopVoice(21-nc1-pasclaire)
 
	-Demandez à un ami de créer votre compte pour vous.
		PlayVoice(22-nc1-ami)
		<nameCharacter1> "Demandez à un ami de créer votre compte pour vous {wa 0.5} et revenez ici quand vous aurez un compte. {wa 1}Je pourrai vous aider plus facilement."
		StopVoice(22-nc1-ami)
		playsfx(moinsjauge -v 0.3)
		$VN.score -= 1
		Jauge.SetSprite($VN.score -spd 0.4)
		
		nameCharacter1.Unhighlight()
		Alfred.Highlight()
		Alfred.SetSprite(fache -spd 0.75)
		PlayVoice(23-alfred-revientpas)
		Alfred "D’accord, je vais aller demander de l’aide à quelqu’un d’autre, mais je ne reviendrai pas … {wa 1}Si vous ne pouvez même pas m’aider à créer un compte …"
		StopVoice(23-alfred-revientpas)
		nameCharacter1.Highlight()
		Alfred.Unhighlight()
		namecharacter1.SetSprite(confused_office -spd 0.75)
		PlayVoice(24-nc1-partezpas)
		<nameCharacter1> "Non monsieur ne partez pas ! {wa 0.5} Je vais vous aider… {wa 1}Allez sur le site, cliquez sur le bouton « s’inscrire », {wa 0.5} et remplissez les informations qui vous sont demandées."
		StopVoice(24-nc1-partezpas)
 
	-Cliquez sur le bouton « s’inscrire » et remplissez les informations qui vous sont demandées.
		PlayVoice(25-nc1-bouton)
		<nameCharacter1> "Cliquez sur le bouton « s’inscrire », {wa 0.6}et remplissez les informations qui vous sont demandées."
		StopVoice(25-nc1-bouton)
		playsfx(+jauge -v 0.8)
		$VN.score += 1
		Jauge.SetSprite($VN.score -spd 0.4)
		
}
HideDB()
SetLayerMedia(background bureau-proche)
playsfx(tic-tac-horloge -loop true -v 0.8)
Highlight(nameCharacter1 Alfred)
namecharacter1.SetSprite(normal_office)
Alfred.SetSprite(normal)
wait(3)
SetLayerMedia(background bureau-fin-journee)
wait(3)
stopsfx(tic-tac-horloge)

nameCharacter1.Unhighlight()
Alfred.Highlight()
mail.Highlight()
jauge.Highlight()
ShowDB()
Alfred "..."
nameCharacter1.Highlight()
Alfred.Unhighlight()
<nameCharacter1> "..."
nameCharacter1.Unhighlight()
Alfred.Highlight()
PlayVoice(26-alfred-pasmail)
Alfred "J'ai rempli mes informations, mais je ne reçois pas l'e-mail de confirmation."
StopVoice(26-alfred-pasmail)

nameCharacter1.Highlight()
Alfred.Unhighlight()
choice "Comment aider Alfred pour l’e-mail de confirmation ?"
{
	-Ignorez l'email et essayez de vous connecter sans confirmation.
		PlayVoice(27-nc1-ignorez)
		<nameCharacter1> "Ignorez l’e-mail et essayez de vous connecter sans confirmation."
		StopVoice(27-nc1-ignorez)
		playsfx(tic-tac-horloge -loop true -v 0.8)
		Pensées "... {wa 1}… {wa 1}…"
		stopsfx(tic-tac-horloge)

	-Vérifiez votre dossier de spam ou de courrier indésirable.
		PlayVoice(28-nc1-spam)
		<nameCharacter1> "Vérifiez votre dossier de spam ou de courrier indésirable."
		StopVoice(28-nc1-spam)
		playsfx(+jauge -v 0.8)
		$VN.score += 1
		Jauge.SetSprite($VN.score -spd 0.4)
		

	-Essayez de créer un autre compte avec une autre adresse e-mail.
		PlayVoice(29-nc1-autrecompte)
		<nameCharacter1> "Essayez de créer un autre compte avec une autre adresse e-mail."
		StopVoice(29-nc1-autrecompte)
		playsfx(moinsjauge -v 0.3)
		$VN.score -= 1
		Jauge.SetSprite($VN.score -spd 0.4)
		
		nameCharacter1.Unhighlight()
		Alfred.Highlight()
		Alfred.SetSprite(fache -spd 0.75)
		PlayVoice(30-alfred-marchepas)
		Alfred "Non mais vous croyez que j’ai plusieurs adresses e-mail, ça ne marche pas votre truc."
		StopVoice(30-alfred-marchepas)
		nameCharacter1.SetSprite(confused_office -spd 0.75)
		nameCharacter1.Highlight()
		Alfred.Unhighlight()
		PlayVoice(31-nc1-spam)
		<nameCharacter1> "Alors regardez dans vos spams ou courriers indésirables."
		StopVoice(31-nc1-spam)
}

nameCharacter1.Unhighlight()
Alfred.Highlight()
Alfred.SetSprite(normal -spd 0.75)
PlayVoice(32-alfred-prochaineetape)
Alfred "J'ai trouvé l'e-mail et confirmé mon compte. {wa 0.5} Quelle est la prochaine étape ?"
StopVoice(32-alfred-prochaineetape)
nameCharacter1.SetSprite(normal_office -spd 0.75)
nameCharacter1.Highlight()
Alfred.Unhighlight()
PlayVoice(33-nc1-reserver)
<nameCharacter1> "Parfait, nous allons maintenant pouvoir réserver la salle."
StopVoice(33-nc1-reserver)

choice "Finaliser la réservation de la salle avec Alfred"
{
	-Vous n’avez plus qu’à choisir la date et l’heure.
		PlayVoice(34-nc1-date)
		<nameCharacter1> "Vous n’avez plus qu’à choisir la date et l’heure dans l’espace de réservation."
		StopVoice(34-nc1-date)
		nameCharacter1.Unhighlight()
		Alfred.Highlight()
		Alfred.SetSprite(étonné -spd 0.75)
		PlayVoice(35-alfred-comment)
		Alfred "D’accord, mais comment je fais pour faire ça …"
		StopVoice(35-alfred-comment)
		nameCharacter1.Highlight()
		Alfred.Unhighlight()
		PlayVoice(36-nc1-reserver)
		<nameCharacter1> "Cliquez sur «Réserver une salle» et sélectionnez la date et l'heure."
		StopVoice(36-nc1-reserver)

	-Appelez directement la salle pour faire la réservation.
		PlayVoice(37-nc1-appeler)
		<nameCharacter1> "Appelez directement la salle pour faire la réservation."
		StopVoice(37-nc1-appeler)
		playsfx(moinsjauge -v 0.3)
		$VN.score -= 1
		Jauge.SetSprite($VN.score -spd 0.4)
		
		nameCharacter1.Unhighlight()
		Alfred.Highlight()
		Alfred.SetSprite(fache -spd 0.75)
		PlayVoice(38-alfred-appeler)
		Alfred "On a passé tout ce temps à me créer un compte pour que je doive appeler maintenant ?"
		StopVoice(38-alfred-appeler)
		nameCharacter1.SetSprite(confused_office -spd 0.75)
		nameCharacter1.Highlight()
		Alfred.Unhighlight()
		PlayVoice(39-nc1-reserver)
		<nameCharacter1> "Vous avez raison, euh alors cliquez sur «Réserver une salle» et sélectionnez la date et l'heure."
		StopVoice(39-nc1-reserver)

	-Cliquez sur «Réserver une salle» et sélectionnez la date et l'heure.
		PlayVoice(40-nc1-cliquer)
		<nameCharacter1> "Cliquez sur «Réserver une salle» et sélectionnez la date et l’heure."
		StopVoice(40-nc1-cliquer)
		playsfx(+jauge -v 0.8)
		$VN.score += 1
		Jauge.SetSprite($VN.score -spd 0.4)
		
}
 
nameCharacter1.Unhighlight()
Alfred.Highlight()
Alfred.SetSprite(souriant -spd 0.75)
PlayVoice(41-alfred-felicitation)
Alfred "C’est écrit : «Félicitations, vous avez réservé la salle. Nous vous avons envoyé une confirmation par email.» {wa 1}Merci beaucoup madame !"
StopVoice(41-alfred-felicitation)
nameCharacter1.SetSprite(smile_office -spd 0.75)
nameCharacter1.Highlight()
Alfred.Unhighlight()
PlayVoice(42-nc1-avecplaisr)
<nameCharacter1> "Avec plaisir, monsieur. {wa 0.5} Bonne chance pour votre conférence !"
StopVoice(42-nc1-avecplaisr)
playsfx(pas)
Alfred.Hide(-i true -spd 0.75)
StopSong()
wait(1)
PlayVoice(43-nc1-bonneaction)
<nameCharacter1> "(Et une bonne action de faite !)"
StopVoice(43-nc1-bonneaction)
nameCharacter1.Hide(-i true -spd 0.75)
ClearLayerMedia(background -m)
HideDB()
wait(2)

ShowGalleryImage(3-galerie-chap3-alfred)
wait(5)
HideGalleryImage(3-galerie-chap3-alfred)

Load(Chapitre_3_2)
