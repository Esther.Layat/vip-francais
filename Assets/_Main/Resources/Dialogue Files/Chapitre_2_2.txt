﻿Highlight(nameCharacter1 nameCharacter2 jauge mail)
ShowDB()
Pensées "Mercredi 7 août, 9h30"
nameCharacter1.Show() 
nameCharacter2.Show()
nameCharacter2.SetPosition(0.8 0.2)
wait(1)
PlaySong(MusMus-BGM-055 -v 0.05)
nameCharacter1.Unhighlight()
PlayVoice(13-nc2-vous-aussi)
<nameCharacter2> "Vous aussi, vous avez reçu un e-mail du Boss ?"
StopVoice(13-nc2-vous-aussi)
nameCharacter2.Unhighlight()
nameCharacter1.Highlight()
nameCharacter1.SetSprite(surprised_office2 -spd 0.75)
PlayVoice(14-nc1-monde-parallele)
<nameCharacter1> "Oui, nous sommes dans un monde parallèle…! {wa 1}Nous devons rester professionnels pour rentrer chez nous."
StopVoice(14-nc1-monde-parallele)
nameCharacter1.Unhighlight()
nameCharacter2.Highlight()
PlayVoice(15-nc2-vous-avez-raison)
<nameCharacter2> "Oui, vous avez raison."
StopVoice(15-nc2-vous-avez-raison)
StopSong()
ClearLayerMedia(background -m)
HideDB()
nameCharacter1.Hide()
nameCharacter2.Hide()
wait(2)
SetLayerMedia(background bureau-proche)
wait(2)
nameCharacter2.Show()
wait(1)
playsfx(e-mail -v 0.6)
ShowDB()
PlayVoice(16_nc2_je_dois_repondre)
<nameCharacter2> "En parlant d’être professionnel, {wa 0.5}je dois répondre à mes e-mails."
StopVoice(16_nc2_je_dois_repondre)
PlaySong(MusMus-BGM-099 -v 0.05)

choice "Que faire ?"
{
	-Consulter la boîte mail
}
wait(1.5)
mail.Show()
nameCharacter1.Hide()
nameCharacter2.Hide()
mail.setsprite(5-mail-lionel)
PlayVoice(19-nc2-lecture-mail)
<nameCharacter2> "*lecture du mail* {wa 2}"
StopVoice(19-nc2-lecture-mail)
PlayVoice(19-20-nc2-drole-de-nom)
<nameCharacter2> "(Lionel Pepsi ? {wa 1}Drôle de nom !)"
StopVoice(19-20-nc2-drole-de-nom)

choice "Que faire avec cet e-mail ?" 
{
	-Répondre au mail
}

wait(1.5)
mail.setsprite(6-reponse-lionel)
playsfx(clavier-ordi)
PlayVoice(22_nc2_bonjour)
<nameCharacter2> "Bonjour,"
StopVoice(22_nc2_bonjour)
stopsfx(clavier-ordi)

choice "Comment commencer l’e-mail ?" 
{
	-Veuillez m’excuser pour cet oubli
		mail.setsprite(7-reponse-lionel-choice1)
		PlayVoice(24-nc2-choice-veuillez-mexcuser)
		$VN.score += 1
		Jauge.SetSprite($VN.score -spd 0.4)
		playsfx(+jauge -v 0.8)
		
		playsfx(clavier-ordi)	
		<nameCharacter2> "Veuillez m’excuser pour cet oubli."
		StopVoice(24-nc2-choice-veuillez-mexcuser)
		stopsfx(clavier-ordi)

		choice "Comment continuer l’e-mail ?"
		{
			-Le fichier que vous cherchez est dans cet e-mail
				mail.setsprite(8-reponse-lionel-choice1-faux)
				PlayVoice(27-nc2-choice-le fichier-est-la)
				$VN.score -= 1
				Jauge.SetSprite($VN.score -spd 0.4)
				playsfx(moinsjauge -v 0.3)
				
				playsfx(clavier-ordi)	
				<nameCharacter2> "Le fichier que vous cherchez est dans cet e-mail."
				StopVoice(27-nc2-choice-le fichier-est-la)
				stopsfx(clavier-ordi)

			-Vous trouverez le formulaire joint à cet e-mail
				mail.setsprite(9-reponse-lionel-choice1-correct)
				PlayVoice(28-nc2-choice-joint-a-ce-mail)
				$VN.score += 1
				Jauge.SetSprite($VN.score -spd 0.4)
				playsfx(+jauge -v 0.8)
				
				playsfx(clavier-ordi)	
				<nameCharacter2> "Vous trouverez le formulaire en question joint à cet e-mail."
				StopVoice(28-nc2-choice-joint-a-ce-mail)
				stopsfx(clavier-ordi)
		}

	-Oui c’est vrai, je vous envoie ça
		mail.setsprite(10-reponse-lionel-choice2)
		PlayVoice(25-nc2-choice2-oui-cest-vrai)
		$VN.score -= 1
		Jauge.SetSprite($VN.score -spd 0.4)
		playsfx(moinsjauge -v 0.3)
		
		playsfx(clavier-ordi)	
		<nameCharacter2> "Oui c’est vrai, je vous envoie ça."
		StopVoice(25-nc2-choice2-oui-cest-vrai)
		stopsfx(clavier-ordi)

		choice "Comment continuer l’e-mail ?"
		{
			-Le fichier que vous cherchez est dans cet e-mail
				mail.setsprite(11-reponse-lionel-choice2-faux)
				PlayVoice(27-nc2-choice-le fichier-est-la)
				$VN.score -= 1
				Jauge.SetSprite($VN.score -spd 0.4)
				playsfx(moinsjauge -v 0.3)
				
				playsfx(clavier-ordi)	
				<nameCharacter2> "Le fichier que vous cherchez est dans cet e-mail."
				StopVoice(27-nc2-choice-le fichier-est-la)
				stopsfx(clavier-ordi)

			-Vous trouverez le formulaire joint à cet e-mail
				mail.setsprite(12-reponse-lionel-choice2-correct)
				PlayVoice(28-nc2-choice-joint-a-ce-mail)
				$VN.score += 1
				Jauge.SetSprite($VN.score -spd 0.4)
				playsfx(+jauge -v 0.8)
				
				playsfx(clavier-ordi)	
				<nameCharacter2> "Vous trouverez le formulaire en question joint à cet e-mail."
				StopVoice(28-nc2-choice-joint-a-ce-mail)
				stopsfx(clavier-ordi)
		}
}

wait(0.75)
playsfx(mail-envoi -v 0.6)
mail.Hide()
nameCharacter1.Show()
nameCharacter2.Show()
PlayVoice(29-nc2-cest-cool)
<nameCharacter2> "C’est cool, il y a des gens qui s’intéressent à notre fête !"
StopVoice(29-nc2-cest-cool)
HideDB()
wait(2)
StopSong()
SetLayerMedia(background -m bureau-fin-journee)
wait(0.5)
Lionel.SetPosition(0.8)
nameCharacter1.SetPosition(0.2)
nameCharacter1.FaceRight()
nameCharacter1.SetSprite(normal_office)
Lionel.FaceLeft()
Lionel.SetSprite(lionel_normal)
Load(Chapitre_2_3)