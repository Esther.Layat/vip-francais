﻿Pensées "Mercredi 7 août, {wa 1} 8h30"

RH.SetPosition(0.8)

playsfx(telephone-sonne -loop true -v 0.5)
SetLayerMedia(background bureau-jour -i true -spd 1.2 -blend)
nameCharacter1.FaceRight()
nameCharacter1.Show(-i true)
nameCharacter1.Move(0.2)
nameCharacter1.SetSprite(normal_office -spd 0.75)
RH.Show()
RH.Animate(Shiver -s true)
PlayVoice(chap1_tel_sonne)
<nameCharacter1> "Oh, {wa 0.4} le téléphone sonne." 
StopVoice(chap1_tel_sonne)
PlayVoice(chap1_appel_des_ressources_humaines)
<nameCharacter1> "Ça vient des <b>ressources humaines</b>. {wa 0.5}Il faut que je réponde."
RH.Animate(Shiver -s false)
StopVoice(chap1_appel_des_ressources_humaines)
stopsfx(telephone-sonne)

RH.Unhighlight()
choice "Comment saluer au téléphone ?" 
{
        -Bonjour, il y a un problème ?
                PlayVoice(24-nc1-bonjour-ya-un-problem)
                <nameCharacter1> "Bonjour, il y a un problème ?"
                StopVoice(24-nc1-bonjour-ya-un-problem)

        -"Bonjour, c'est bien la réception."
                PlayVoice(25-nc1-cest-la-reception)
                <nameCharacter1> "Bonjour, <nameCharacter1> de la réception."
                StopVoice(25-nc1-cest-la-reception)
	        playsfx(+jauge -v 0.8)
                $VN.score += 1
                Jauge.SetSprite($VN.score -spd 0.4)
                

        -Allô, c'est moi qui parle.
                PlayVoice(26-nc1-cest-moi)
                <nameCharacter1> "Allô, c’est moi qui parle."
                StopVoice(26-nc1-cest-moi)
	        playsfx(moinsjauge -v 0.3)
                $VN.score -= 1
                Jauge.SetSprite($VN.score -spd 0.4)
                
}

nameCharacter1.Unhighlight()
PlayVoice(chap1_reponse_des_RH)
RH "Oui, bonjour. {wa 0.5}C’est à propos du nouvel employé qui va rejoindre votre équipe. {wa 0.5}Je vous informe qu’il arrive aujourd’hui à 10h00. {wa 0.5}Son nom est <nameCharacter2> et ce sera son premier jour."
StopVoice(chap1_reponse_des_RH)

RH.Unhighlight()
PlayVoice(chap1_cest_note)
<nameCharacter1> "C’est noté, je vous remercie pour votre message."
StopVoice(chap1_cest_note)
playsfx(telephone-arret -v 0.3)
RH.Hide()
PlayVoice(chap1_jespere_il_est_sympa)
<nameCharacter1> "Un coéquipier ? {wa 0.5}J’espère qu’il sera sympa."
StopVoice(chap1_jespere_il_est_sympa)
nameCharacter1.Hide(-i true -spd 1.3)
ClearLayerMedia(background -m)
HideDB()
wait(3)
Load(Chapitre_1_3)