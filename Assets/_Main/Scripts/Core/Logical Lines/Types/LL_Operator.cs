using System;
using System.Collections;

using UnityEngine;
using System.Text.RegularExpressions;


using static DIALOGUE.LogicalLines.LogicalLineUtils.Expressions;


namespace DIALOGUE.LogicalLines
{
    public class LL_Operator : ILogicalLine
    {
        public string keyword => throw new System.NotImplementedException();

        // Méthode pour exécuter la ligne de dialogue
        public IEnumerator Execute(DIALOGUE_LINE line)
        {
            string trimmedLine = line.rawData.Trim();
            string[] parts = Regex.Split(trimmedLine, REGEX_ARITHMATIC);

            if(parts.Length < 3)
            {
                Debug.LogError($"Invalid command: {trimmedLine}");
                yield break;
            }

            string variable = parts[0].Trim().TrimStart(VariableStore.VARIABLE_ID);
            string op = parts[1].Trim();
            string[] remainingParts = new string[parts.Length - 2];
            Array.Copy(parts, 2, remainingParts, 0, parts.Length - 2);

            object value = CalculateValue(remainingParts);

            if(value == null)
                yield break;
            
            ProcessOperator(variable, op, value);
        }

        // Méthode pour traiter l'opérateur
        private void ProcessOperator(string variable, string op, object value)
        {
            if(VariableStore.TryGetValue(variable, out object currentvalue))
            {
                ProcessOperatorOnVariable(variable, op, value, currentvalue);
            }
            else if(op == "=")
            {
                VariableStore.CreateVariable(variable, value);
            }
        }

        // Méthode pour appliquer l'opérateur sur la variable existante
        private void ProcessOperatorOnVariable(string variable, string op, object value, object currentvalue)
        {
            switch(op)
            {
                case "=":
                    VariableStore.TrySetValue(variable,value);
                    break;
                case "+=":
                    VariableStore.TrySetValue(variable, ConcatenateOrAdd(value, currentvalue));
                    break;
                case "-=":
                    VariableStore.TrySetValue(variable, Convert.ToDouble(currentvalue)-Convert.ToDouble(value));
                    break;
                case "*=":
                    VariableStore.TrySetValue(variable, Convert.ToDouble(currentvalue)*Convert.ToDouble(value));
                    break;
                case "/=":
                    VariableStore.TrySetValue(variable, Convert.ToDouble(currentvalue)/Convert.ToDouble(value));
                    break;
                default : 
                    Debug.LogError($"Invalid operator: {op}");
                    break;
            }
        }

        // Méthode pour concaténer des chaînes de caractères ou additionner des valeurs numériques
        private object ConcatenateOrAdd(object value, object currentvalue)
        {
            if(value is string)
                return currentvalue.ToString() + value;
            
            return Convert.ToDouble(currentvalue) + Convert.ToDouble(value);
        }

        // Méthode pour vérifier si une ligne de dialogue correspond à ce type de ligne logique
        public bool Matches(DIALOGUE_LINE line)
        {
            Match match = Regex.Match(line.rawData.Trim(), REGEX_OPERATOR_LINE);

            return match.Success;
        }
    }
}

