using System.Collections;

namespace DIALOGUE.LogicalLines
{
    public class LL_Input : ILogicalLine
    {
        public string keyword => "input"; 
        // Méthode pour exécuter la logique associée à cette ligne
        public IEnumerator Execute(DIALOGUE_LINE line)
        {
            string title = line.dialogueData.rawData;
            InputPanel panel = InputPanel.instance;
            panel.Show(title);

            while(panel.isWaitingOnUserInput)
                yield return null;
        }

        // Méthode pour vérifier si cette ligne logique correspond à la ligne de dialogue
        public bool Matches(DIALOGUE_LINE line)
        {
            return (line.hasSpeaker && line.speakerData.name.ToLower()== keyword);
        }
    }
}

