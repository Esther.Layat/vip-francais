using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class VariableStore 
{
    private const string DEFAULT_DATABASE_NAME = "Default";
    public const char DATABASE_VARIABLE_RELATIONAL_ID = '.';
    public static readonly string REGEX_VARIABLE_IDS = @"[!]?\$[a-zA-Z0-9_.]+";
    public const char VARIABLE_ID = '$';
    public class Database
    {
        public Database(string name)
        {
            this.name = name;
            variables = new Dictionary<string, Variable>();
        }

        public string name;
        public Dictionary<string, Variable> variables = new Dictionary<string, Variable>();
    }

    public abstract class Variable
    {
        public abstract object Get();
        public abstract void Set(object value);
    }

    public class Variable<T> : Variable
    {
        private T value;

        private Func<T> getter;
        private Action<T> setter;

        public Variable(T defaultValue = default, Func<T> getter = null, Action<T> setter = null)
        {
            value = defaultValue;

            if(getter == null)
                this.getter = () => value;
            else 
                this.getter = getter;

            if(setter == null)
                this.setter = newValue => value = newValue;
            else 
                this.setter = setter;
        }

        // Implémentation de la méthode pour obtenir la valeur
        public override object Get() => getter();

        // Implémentation de la méthode pour définir la valeur
        public override void Set(object newValue) => setter((T)newValue);
    }

    // Dictionnaire contenant toutes les bases de données, avec une base de données par défaut
    public static Dictionary<string, Database> databases = new Dictionary<string,Database>() { { DEFAULT_DATABASE_NAME, new Database(DEFAULT_DATABASE_NAME)} };
    private static Database defaultDataBase => databases[DEFAULT_DATABASE_NAME];

    // Méthode pour créer une nouvelle base de données
    public static bool CreateDataBase(string name)
    {
        if(!databases.ContainsKey(name))
        {
            databases[name] = new Database(name);
            return true;
        }

        return false;
    }

    // Méthode pour obtenir une base de données par son nom
    public static Database GetDatabase(string name)
    {
        if(name == string.Empty)
            return defaultDataBase;
        
        if(!databases.ContainsKey(name))
            CreateDataBase(name);

        return databases[name];
    }

    // Méthode pour créer une nouvelle variable dans une base de données
    public static bool CreateVariable<T>(string name, T defaultValue, Func<T> getter = null, Action<T> setter = null)
    {
        (string[] parts, Database db, string variableName) = ExtractInfo(name);

        if(db.variables.ContainsKey(variableName))
            return false;
        
        db.variables[variableName] = new Variable<T>(defaultValue, getter, setter);

        return true;
    }

    // Méthode pour essayer d'obtenir la valeur d'une variable
    public static bool TryGetValue(string name, out object variable)
    {
        (string[] parts, Database db, string variableName) = ExtractInfo(name);
        
        if(!db.variables.ContainsKey(variableName))
        {
            variable = null;
            return false;
        }

        variable = db.variables[variableName].Get();
        return true;
    }

    // Méthode pour essayer de définir la valeur d'une variable
    public static bool TrySetValue<T>(string name, T value)
    {
        (string[] parts, Database db, string variableName) = ExtractInfo(name);

        if(!db.variables.ContainsKey(variableName))
            return false;
        
        db.variables[variableName].Set(value);
        return true;
    }

    // Méthode pour extraire les informations d'une variable (nom de la base de données et nom de la variable)
    private static (string[], Database, string) ExtractInfo(string name)
    {
        string[] parts = name.Split(DATABASE_VARIABLE_RELATIONAL_ID);
        Database db = parts.Length > 1 ? GetDatabase(parts[0]) : defaultDataBase;
        string variableName = parts.Length > 1 ? parts[1] : parts[0];

        return (parts, db, variableName);
    }

    // Méthode pour vérifier si une variable existe
    public static bool HasVariable(string name)
    {
        string[] parts = name.Split(DATABASE_VARIABLE_RELATIONAL_ID);
        Database db = parts.Length > 1 ? GetDatabase(parts[0]) : defaultDataBase;
        string variableName = parts.Length > 1 ? parts[1] : parts[0];

        return db.variables.ContainsKey(variableName);
    }

    // Méthode pour supprimer une variable
    public static void RemoveVariable(string name)
    {
        (string[] parts, Database db, string variableName) = ExtractInfo(name);

        if(db.variables.ContainsKey(variableName))
            db.variables.Remove(variableName);
        
    }

    // Méthode pour supprimer toutes les variables
    public static void RemoveAllVariables()
    {
        databases.Clear();
        databases[DEFAULT_DATABASE_NAME] = new Database(DEFAULT_DATABASE_NAME);
    }

    // Méthode pour imprimer tous les noms de bases de données
    public static void PrintAllDatabases()
    {
        foreach(KeyValuePair<string, Database> dbEntry in databases)
        {
            Debug.Log($"Database: '<color=#FFB145>{dbEntry.Key}</color>'");
        }
    }

    // Méthode pour imprimer toutes les variables dans toutes les bases de données ou dans une base de données spécifique
    public static void PrintAllVariables(Database database = null)
    {
        if(database != null)
        {
            PrintAllDatabasesVariables(database);
            return;
        }

        foreach(var dbEntry in databases)
        {
            PrintAllDatabasesVariables(dbEntry.Value);
        }
    }

    // Méthode pour imprimer toutes les variables dans une base de données spécifique
    private static void PrintAllDatabasesVariables(Database database)
    {
        StringBuilder sb = new StringBuilder();
        
        sb.AppendLine($"Database: <color=#F38544>{database.name}</color>");

        foreach(KeyValuePair<string, Variable> variablePair in database.variables)
        {
            string variableName = variablePair.Key;
            object variableValue = variablePair.Value.Get();
            sb.AppendLine($"\t<color=#FFB145>Variable [{variableName}]</color> = <color=#FFD22D>{variableValue}</color>");
        }
        Debug.Log(sb.ToString());
    }

}
