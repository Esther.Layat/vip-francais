
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System.Linq;
using System;

namespace DIALOGUE.LogicalLines
{
    public class LogicalLineManager
    {
        // Référence au système de dialogue singleton
        private DialogueSystem dialogueSystem => DialogueSystem.instance;
        // Liste des lignes logiques disponibles
        private List<ILogicalLine> logicalLines = new List<ILogicalLine>();
        // Constructeur qui charge les lignes logiques
        public LogicalLineManager() => LoadLogicalLines();

        // Méthode pour charger toutes les lignes logiques implémentées dans le projet
        private void LoadLogicalLines()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Type[] lineTypes = assembly.GetTypes()
                                        .Where(t => typeof(ILogicalLine).IsAssignableFrom(t) && !t.IsInterface)
                                        .ToArray();
            foreach(Type lineType in lineTypes)
            {
                ILogicalLine line = (ILogicalLine)Activator.CreateInstance(lineType);
                logicalLines.Add(line);
            }
        }

        // Essaie de trouver et d'exécuter la logique correspondante pour une ligne de dialogue donnée
        public bool TryGetLogic(DIALOGUE_LINE line, out Coroutine logic)
        {
            foreach(var logicalLine in logicalLines)
            {
                if(logicalLine.Matches(line))
                {
                    logic = dialogueSystem.StartCoroutine(logicalLine.Execute(line));
                    return true;
                }
            }
            
            logic = null;
            return false;
        }
    }
}

