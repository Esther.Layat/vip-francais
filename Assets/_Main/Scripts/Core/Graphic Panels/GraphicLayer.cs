
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class GraphicLayer
{
    public const string LAYER_OBJECT_NAME_FORMAT = "Layer: {0}";
    public int layerDepth = 0;
    public Transform panel;


    public GraphicObject currentGraphic = null;
    public List<GraphicObject> oldGraphics = new List<GraphicObject>();
    
    // Méthode pour définir la texture en chargeant depuis un fichier
    public Coroutine SetTexture(string filePath, float transitionSpeed = 1f, Texture blendingTexture = null, bool immediate = false){
        Texture tex = Resources.Load<Texture>(filePath);

        if(tex == null){
            Debug.LogError($"Could not load graphic texture from path '{filePath}.' Please ensure it exists within Resources!");
            return null;
        }

        return SetTexture(tex, transitionSpeed, blendingTexture, filePath, immediate);
    }

    // Méthode pour définir la texture en utilisant une texture directement
    public Coroutine SetTexture(Texture tex, float transitionSpeed = 1f, Texture blendingTexture = null, string filePath = "", bool immediate = false){
        return CreateGraphic(tex, transitionSpeed, filePath, blendingTexture : blendingTexture, immediate : immediate);
    }

    // Méthode pour définir la video en chargeant depuis un fichier
    public Coroutine SetVideo(string filePath, float transitionSpeed = 1f, bool useAudio = true, Texture blendingTexture = null, bool immediate = false){
        VideoClip clip = Resources.Load<VideoClip>(filePath);

        if(clip == null){
            Debug.LogError($"Could not load graphic texture from path '{filePath}.' Please ensure it exists within Resources!");
            return null;
        }

        return SetVideo(clip, transitionSpeed, useAudio, blendingTexture, filePath);
    }

    // Méthode pour définir la texture en utilisant une video directement
    public Coroutine SetVideo(VideoClip clip, float transitionSpeed = 1f, bool useAudio = true, Texture blendingTexture = null, string filePath = "", bool immediate = false){
        return CreateGraphic(clip, transitionSpeed, filePath, useAudio, blendingTexture, immediate);
    }

    // Méthode privée pour créer un nouvel objet graphique
    private Coroutine CreateGraphic<T>(T graphicData, float transitionSpeed, string filePath, bool useAudioForVideo = true, Texture blendingTexture = null, bool immediate = false){
        GraphicObject newGraphic = null; 

        if(graphicData is Texture){
            newGraphic = new GraphicObject(this, filePath, graphicData as Texture, immediate);
        }
        else if (graphicData is VideoClip)
        {
            newGraphic = new GraphicObject(this, filePath, graphicData as VideoClip, useAudioForVideo, immediate);
        }

        if(currentGraphic != null && !oldGraphics.Contains(currentGraphic))
            oldGraphics.Add(currentGraphic);

        currentGraphic = newGraphic;
        if(!immediate)
            // Déclenche un fondu entrant pour afficher le nouvel objet graphique
            return currentGraphic.FadeIn(transitionSpeed, blendingTexture);
        
        //Sinon, il s’agit d’un effet immédiat
        DestroyOldGraphics();
        return null;
    }

    // Méthode pour détruire les anciens objets graphiques
    public void DestroyOldGraphics(){
        foreach(var g in oldGraphics)
            Object.Destroy(g.renderer.gameObject);
        oldGraphics.Clear();
    }

    // Méthode pour effacer la couche graphique
    public void Clear(float transitionSpeed = 1, Texture blendTexture = null, bool immediate=false)
    {
        if(currentGraphic != null){
            if(!immediate)
                currentGraphic.FadeOut(transitionSpeed, blendTexture);
            else
                currentGraphic.Destroy();
        }

        foreach(var g in oldGraphics)
        {
            if(!immediate)
                g.FadeOut(transitionSpeed, blendTexture);
            else
                g.Destroy();
        }
    }

}