
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class GraphicPanel
{
    public string panelName;
    public GameObject rootPanel;
    public List<GraphicLayer> layers {get; private set; }= new List<GraphicLayer>();

    public bool isClear => layers == null || layers.Count == 0 || layers.All(layer => layer.currentGraphic == null);

    // Méthode pour obtenir une couche graphique par sa profondeur
    public GraphicLayer GetLayer(int layerDepth, bool createIfDoesNotExist = false)
    {
        for (int i = 0; i < layers.Count; i++)
        {
            if (layers[i].layerDepth == layerDepth)
            {
                return layers[i];
            }
        }

        if(createIfDoesNotExist){
            return CreateLayer(layerDepth);
        }
        return null;
    }

    // Méthode privée pour créer une nouvelle couche graphique
    private GraphicLayer CreateLayer(int layerDepth)
    {
        GraphicLayer layer = new GraphicLayer();
        GameObject panel = new GameObject(string.Format(GraphicLayer.LAYER_OBJECT_NAME_FORMAT, layerDepth));
        RectTransform rect = panel.AddComponent<RectTransform>();
        panel.AddComponent<CanvasGroup>();
        panel.transform.SetParent(rootPanel.transform, false);

        rect.anchorMin = Vector2.zero;
        rect.anchorMax = Vector2.one;
        rect.offsetMin = Vector2.zero;
        rect.offsetMax = Vector2.one;

        // Configure la couche graphique avec le GameObject et la profondeur spécifiés
        layer.panel = panel.transform;
        layer.layerDepth = layerDepth;

        int index = layers.FindIndex(l => l.layerDepth > layerDepth);
        if (index == -1)
        {
            layers.Add(layer);
        }
        else
            layers.Insert(index, layer);

        // Met à jour l'ordre des couches graphiques dans le panneau
        for (int i = 0; i < layers.Count; i++)
        {
            layers[i].panel.SetSiblingIndex(layers[i].layerDepth);
        }

        return layer;
    }

    // Méthode pour effacer toutes les couches graphiques dans ce panneau
    public void Clear(float transitionSpeed = 1, Texture blendTexture = null, bool immediate = false){
        foreach(var layer in layers)
            layer.Clear(transitionSpeed, blendTexture, immediate);
    }
}
