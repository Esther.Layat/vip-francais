using System;

using System.Collections.Generic;
using History;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DIALOGUE
{
    public class PlayerInputManager : MonoBehaviour
    {
        private PlayerInput input;
        private List<(InputAction action, Action<InputAction.CallbackContext> command)> actions = new List<(InputAction action, Action<InputAction.CallbackContext> command)> ();

        private void Awake() 
        {
            input = GetComponent<PlayerInput> ();
            InitializeActions();
        }

        private void InitializeActions()
        {
            actions.Add((input.actions["Next"], OnNext));
            actions.Add((input.actions["HistoryBack"], OnHistoryBack));
            actions.Add((input.actions["HistoryForward"], OnHistoryForward));
            actions.Add((input.actions["HistoryLogs"], OnHistoryToggleLog));
        }

        private void OnEnable() 
        {
            foreach(var inputAction in actions){
                inputAction.action.performed += inputAction.command;
            }
        }

        private void OnDisable() 
        {
            foreach(var inputAction in actions){
                inputAction.action.performed -= inputAction.command;
            }
        }

        // Fonction appelée pour faire avancer le dialogue
        public void OnNext(InputAction.CallbackContext c)
        {
            DialogueSystem.instance.OnUserPrompt_Next();
        }

        // Méthode appelée lors de l'appui sur la touche correspondante à "HistoryBack"
        public void OnHistoryBack(InputAction.CallbackContext c)
        {
            HistoryManager.instance.GoBack();
        }

        // Méthode appelée lors de l'appui sur la touche correspondante à "HistoryForward"
        public void OnHistoryForward(InputAction.CallbackContext c)
        {
            HistoryManager.instance.GoForward();
        }

        // Méthode appelée lors de l'appui sur la touche correspondante à "HistoryLogs"
        public void OnHistoryToggleLog(InputAction.CallbackContext c)
        {
            var logs = HistoryManager.instance.logManager;
            
            if(!logs.isOpen)
                logs.Open();
            else    
                logs.Close();
        }
    }
}

