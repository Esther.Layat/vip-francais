
using System;
using UnityEngine;

namespace VISUALNOVEL
{

    public class VNDatabaseLinkSetup : MonoBehaviour
    {
        // Méthode pour configurer les liens externes
        public void SetupExternalLinks()
        {
            VariableStore.CreateVariable("VN.mainCharName", "", () => VNGameSave.activeFile.playerName, value => VNGameSave.activeFile.playerName = value);
            VariableStore.CreateVariable("VN.mainCharName2", "", () => VNGameSave.activeFile.playerName2, value2 => VNGameSave.activeFile.playerName2 = value2);
            VariableStore.CreateVariable("VN.score",(double)0);
        }
    }
}

