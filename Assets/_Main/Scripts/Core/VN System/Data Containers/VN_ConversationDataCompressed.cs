

namespace VISUALNOVEL
{
    [System.Serializable]
    public class VN_ConversationDataCompressed
    {
        public string fileName;
        public int startIndex, endIndex;
        public int progress;
    }
}
