
using UnityEngine;

[System.Serializable]
public class VN_Configuration
{
    public static VN_Configuration activeConfig;

    public static string filePath => $"{FilePaths.root}vnconfig.cfg";

    public const bool ENCRYPT = false;

    //Réglages générals
    public bool display_fullscreen = true;
    public string display_resolution = "1920x1080";
    public bool continueSkippingAfterChoice = false;
    public float dialogueTextSpeed = 1f;
    public float dialogueAutoReadSpeed = 1f;

    //Réglages audio
    public float musicVolume = 1f;
    public float sfxVolume = 1f;
    public float voicesVolume = 1f;
    public bool musicMute = false;
    public bool sfxMute = false;
    public bool voicesMute = false;

    //Autres réglages
    public float historyLogScale = 1f;

    // Charge la configuration et l'applique aux éléments de l'interface utilisateur
    public void Load()
    {
        var ui = ConfigMenu.instance.ui;

        //Application des réglages générales
        ConfigMenu.instance.SetDisplayToFullScreen(display_fullscreen);
        ui.SetButtonColors(ui.fullscreen, ui.windowed, display_fullscreen);

        //Définir la résolution de l'écran
        int res_index = 0;
        for(int i = 0; i < ui.resolutions.options.Count; i++)
        {
            string resolution = ui.resolutions.options[i].text;
            if(resolution == display_resolution)
            {
                res_index = i;
                break;
            }
        }
        ui.resolutions.value = res_index;

        //Définir continuer après avoir sauté l’option
        ui.SetButtonColors(ui.skippingContinue, ui.skippingStop, continueSkippingAfterChoice);

        //Définir la valeur de l’achitect et la vitesse du lecteur automatique
        ui.architectSpeed.value = dialogueTextSpeed;
        ui.autoReaderSpeed.value = dialogueAutoReadSpeed;

        //Définir les volumes des audio mixer
        ui.musicVolume.value = musicVolume;
        ui.sfxVolume.value = sfxVolume;
        ui.voicesVolume.value = voicesVolume;
        ui.musicMute.sprite = musicMute ? ui.mutedSymbol : ui.unmutedSymbole;
        ui.sfxMute.sprite = sfxMute ? ui.mutedSymbol : ui.unmutedSymbole;
        ui.voicesMute.sprite = voicesMute ? ui.mutedSymbol : ui.unmutedSymbole;

    }

    // Sauvegarde la configuration dans un fichier
    public void Save()
    {
        FileManager.Save(filePath, JsonUtility.ToJson(this), encrypt : ENCRYPT);
    }

}
