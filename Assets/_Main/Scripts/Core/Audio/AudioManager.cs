using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public const string MUSIC_VOLUME_PARAMETER_NAME = "MusicVolume";
    public const string SFX_VOLUME_PARAMETER_NAME = "SFXVolume";
    public const string VOICES_VOLUME_PARAMETER_NAME = "VoicesVolume";
    public const float MUTED_VOLUME_LEVEL = -80F;

    private const string SFX_PARENT_NAME = "SFX";
    public static readonly char[] SFX_NAME_FORMAT_CONTAINERS = new char[] {'[', ']'};
    private static string SFX_NAME_FORMAT = $"SFX - {SFX_NAME_FORMAT_CONTAINERS[0]}"+"{0}"+$"{SFX_NAME_FORMAT_CONTAINERS[1]}";
    
    public const float TRACK_TRANSITION_SPEED = 1f;

    public static AudioManager instance { get; private set;}

    public Dictionary<int, AudioChannel> channels = new Dictionary<int,AudioChannel>();

    // Groupes de mixage pour la musique, les effets sonores et les voix
    public AudioMixerGroup musicMixer;
    public AudioMixerGroup sfxMixer;
    public AudioMixerGroup voicesMixer;

    public AnimationCurve audioFalloffCurve;

    // Racine des effets sonores dans la hiérarchie
    private Transform sfxRoot;

    public AudioSource[] allSFX => sfxRoot.GetComponentsInChildren<AudioSource>();

    private void Awake(){
        if(instance == null){
            transform.SetParent(null);
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else{
            DestroyImmediate(gameObject);
            return;
        }

        // Crée un GameObject pour regrouper tous les effets sonores
        sfxRoot = new GameObject(SFX_PARENT_NAME).transform;
        sfxRoot.SetParent(transform);
    }

    // Joue un effet sonore à partir d'un fichier audio (par chemin)
    public AudioSource PlaySoundEffect(string filePath, AudioMixerGroup mixer = null, float  volume = 1, float pitch = 1, bool loop = false){
        AudioClip clip = Resources.Load<AudioClip>(filePath);

        if(clip == null){
            Debug.LogError($"Could not load graphic texture from path '{filePath}.' Please ensure it exists within Resources!");
            return null;
        }

        return PlaySoundEffect(clip, mixer, volume, pitch, loop, filePath);
    }

    // Joue un effet sonore à partir d'un AudioClip
    public AudioSource PlaySoundEffect(AudioClip clip, AudioMixerGroup mixer = null, float  volume = 1, float pitch = 1, bool loop = false, string filePath = "")
    {
        string fileName = clip.name;
        if(filePath != string.Empty)
            fileName = filePath;

        AudioSource effectSource = new GameObject(string.Format(SFX_NAME_FORMAT, fileName)).AddComponent<AudioSource>();
        effectSource.transform.SetParent(sfxRoot);
        effectSource.transform.position = sfxRoot.position;

        effectSource.clip = clip;

        if(mixer == null){
            mixer = sfxMixer;
        }

        // Configure le groupe de mixage, le volume, le blend spatial, le pitch et la boucle de l'AudioSource
        effectSource.outputAudioMixerGroup = mixer;
        effectSource.volume = volume;
        effectSource.spatialBlend = 0;
        effectSource.pitch = pitch;
        effectSource.loop = loop;

        effectSource.Play();

        // Si l'effet sonore ne doit pas être en boucle, détruit l'AudioSource après la durée du clip plus une seconde
        if(!loop)
            Destroy(effectSource.gameObject, (clip.length / pitch) + 1);

        return effectSource;
    }

    // Joue une voix à partir d'un fichier audio (par chemin)
    public AudioSource PlayVoice(string filePath, float  volume = 1, float pitch = 1, bool loop = false){
        return PlaySoundEffect(filePath, voicesMixer, volume, pitch, loop);
    }

    // Joue une voix à partir d'un AudioClip
    public AudioSource PlayVoice(AudioClip clip, float volume = 1, float pitch = 1, bool loop = false){
        return PlaySoundEffect(clip, voicesMixer, volume, pitch, loop);
    }

    // Arrête la lecture d'un effet sonore en spécifiant son AudioClip
    public void StopSoundEffect(AudioClip clip) => StopSoundEffect(clip.name);

    // Arrête la lecture d'un effet sonore en spécifiant son nom
    public void StopSoundEffect(string soundName){
        soundName = soundName.ToLower();
        AudioSource[] sources = sfxRoot.GetComponentsInChildren<AudioSource>();
        foreach (var source in sources){
            if(source.clip.name.ToLower() == soundName){
                Destroy(source.gameObject);
                return;
            }
        }
    }

    // Vérifie si un effet sonore est en cours de lecture en spécifiant son nom
    public bool IsPlayingSoundEffect(string soundName)
    {
        soundName = soundName.ToLower();
        AudioSource[] sources = sfxRoot.GetComponentsInChildren<AudioSource>();
        foreach (var source in sources){
            if(source.clip.name.ToLower() == soundName)
                return true;
        }
        return false;
    }

    // Joue une piste audio à partir d'un fichier audio (par chemin)
    public AudioTrack PlayTrack(string filePath, int channel = 0, bool loop = true, float startingVolume = 0f, float volumeCap = 1f, float pitch = 1f){
        AudioClip clip = Resources.Load<AudioClip>(filePath) ;
        if (clip == null){
            Debug.LogError($"Could not load audio file: '{filePath}'. Please make sure this exists in the Resources directory!");
            return null;
        }
        return PlayTrack(clip, channel, loop, startingVolume, volumeCap, pitch, filePath);
    }

    // Joue une piste audio à partir d'un AudioClip
    public AudioTrack PlayTrack(AudioClip clip, int channel = 0, bool loop = true, float startingVolume = 0f, float volumeCap = 1f, float pitch = 1f, string filePath = ""){
        AudioChannel audioChannel= TryGetChannel(channel, createIfDoesNotExist : true);
        AudioTrack track = audioChannel.PlayTrack(clip, loop, startingVolume, volumeCap, pitch, filePath);
        return track;
    }

    // Arrête la lecture de la piste audio sur un canal spécifique
    public void StopTrack(int channel){
        AudioChannel c = TryGetChannel(channel, createIfDoesNotExist : false);

        if(c == null)
            return;
        c.StopTrack();
    }

    // Arrête la lecture de la piste audio avec un nom spécifique
    public void StopTrack(string trackName){
        trackName = trackName.ToLower();
        foreach (var channel in channels.Values){
            if (channel.activeTrack != null && channel.activeTrack.name.ToLower() == trackName)
            {
                channel.StopTrack();
                return;
            }
        }

    }

    // Arrête la lecture de des pistes audio
    public void StopAllTracks()
    {
        foreach (AudioChannel channel in channels.Values)
        {
            channel.StopTrack();
        }
    }

    // Arrête la lecture des effets sonores
    public void StopAllSoundEffects()
    {
        AudioSource[] sources = sfxRoot.GetComponentsInChildren<AudioSource>();
        foreach (var source in sources){
            Destroy(source.gameObject);
        }
    }

    // Tente de récupérer un canal audio en fonction de son numéro
    public AudioChannel TryGetChannel(int channelNumber, bool createIfDoesNotExist = false){
        AudioChannel channel = null;

        if(channels.TryGetValue(channelNumber, out channel)){
            return channel;
        }
        else if(createIfDoesNotExist){
            channel = new AudioChannel(channelNumber);
            channels.Add(channelNumber, channel);
            return channel;
        }
        return null;
    }

    // Définit le volume de la musique
    public void SetMusicVolume(float volume, bool muted)
    {
        volume = muted ? MUTED_VOLUME_LEVEL: audioFalloffCurve.Evaluate(volume);
        musicMixer.audioMixer.SetFloat(MUSIC_VOLUME_PARAMETER_NAME, volume);
    }

    // Définit le volume des effets sonores
    public void SetSFXVolume(float volume, bool muted)
    {
        volume = muted ? MUTED_VOLUME_LEVEL: audioFalloffCurve.Evaluate(volume);
        sfxMixer.audioMixer.SetFloat(SFX_VOLUME_PARAMETER_NAME, volume);
    }

    // Définit le volume des voix
    public void SetVoicesVolume(float volume, bool muted)
    {
        volume = muted ? MUTED_VOLUME_LEVEL: audioFalloffCurve.Evaluate(volume);
        voicesMixer.audioMixer.SetFloat(VOICES_VOLUME_PARAMETER_NAME, volume);
    }
        
}
