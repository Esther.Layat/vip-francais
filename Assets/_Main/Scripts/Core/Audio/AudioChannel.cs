using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioChannel 
{
    private const string TRACK_CONTAINER_NAME_FORMAT = "Channel - [{0}]";
    public int channelIndex { get; private set; }

    public Transform trackContainer { get; private set; } = null;

    public AudioTrack activeTrack { get; private set; } = null;

    private List<AudioTrack> tracks = new List<AudioTrack>();
    
    bool isLevelingVolume => co_volumeLeveling != null;
    Coroutine co_volumeLeveling = null;


    public AudioChannel(int channel){
        channelIndex = channel;

        trackContainer = new GameObject(string.Format(TRACK_CONTAINER_NAME_FORMAT, channel)).transform;
        trackContainer.SetParent(AudioManager.instance.transform);
    }

    // Joue une piste audio
    public AudioTrack PlayTrack(AudioClip clip, bool loop, float startingVolume, float volumeCap, float pitch, string filePath){
        if(TryGetTrack(clip.name, out AudioTrack existingTrack)){
            if(!existingTrack.isPlaying)
                existingTrack.Play();

            SetAsActiveTrack(existingTrack);

            return existingTrack;
        }

        AudioTrack track = new AudioTrack(clip, loop, startingVolume, volumeCap, pitch, this, AudioManager.instance.musicMixer, filePath);
        track.Play();

        SetAsActiveTrack(track);

        return track;
    }

    // Tente de récupérer une piste audio en fonction de son nom
    public bool TryGetTrack(string trackName, out AudioTrack value){
        trackName = trackName.ToLower();
        
        foreach (var track in tracks){
            if(track.name.ToLower() == trackName){
                value = track;
                return true;
            }
        }

        value = null;
        return false;
    }

    // Définit une piste audio comme active sur ce canal
    private void SetAsActiveTrack(AudioTrack track){
        if(!tracks.Contains(track))
            tracks.Add(track);

        activeTrack = track;

        TryStartVolumeLeveling();
    }
    
    // Essaye de démarrer le processus d'ajustement du volume
    private void TryStartVolumeLeveling(){
        if(!isLevelingVolume)
            co_volumeLeveling = AudioManager.instance.StartCoroutine(VolumeLeveling());

    }

    // Coroutine pour ajuster le niveau de volume
    private IEnumerator VolumeLeveling(){
        while((activeTrack != null && (tracks.Count > 1 || activeTrack.volume != activeTrack.volumeCap)) || (activeTrack == null && tracks.Count > 0)){
            for(int i = tracks.Count - 1;  i >= 0; i--){
                AudioTrack track = tracks[i];

                float tragetVol = activeTrack == track ? track.volumeCap : 0;

                if(track == activeTrack && track.volume == tragetVol)
                    continue;
                    
                track.volume = Mathf.MoveTowards(track.volume, tragetVol, AudioManager.TRACK_TRANSITION_SPEED*Time.deltaTime);

                if(track != activeTrack && track.volume == 0)
                {
                    DestroyTrack(track);
                }


            }
            yield return null;
        }
        co_volumeLeveling = null;
    }
    
    // Coroutine pour ajuster le niveau de volume
    private void DestroyTrack(AudioTrack track){
        if(tracks.Contains(track))
            tracks.Remove(track);

        Object.Destroy(track.root);
    }

    // Arrête la lecture de la piste audio active sur ce canal
    public void StopTrack(bool immediate = false){
        if(activeTrack == null){
            return;
        }

        if(immediate)
        {
            DestroyTrack(activeTrack);
            activeTrack = null;
        }
        else
        {
            activeTrack = null;
            TryStartVolumeLeveling();
        }
    }

}
