using System;
using System.Collections.Generic;
using UnityEngine;

namespace COMMANDS
{
    // Classe qui gère une base de données de commandes avec des délégués
    public class CommandDataBase
    {
        // Dictionnaire qui stocke les commandes avec leur nom correspondant
        private Dictionary<string, Delegate> database = new Dictionary<string, Delegate>();

        // Vérifie si la base de données contient des commandes avec le nom spécifié
        public bool HasCommands(string commandName) => database.ContainsKey(commandName.ToLower());

        // Ajoute une commande à la base de données
        public void AddCommand(string commandName, Delegate command)
        {
            commandName = commandName.ToLower();

            if (!database.ContainsKey(commandName))
            {
                database.Add(commandName, command);
            }
            else
                Debug.LogError($"Command already exists in the database '{commandName}'");
        }

        // Récupère une commande de la base de données en fonction de son nom
        public Delegate GetCommand(string commandName)
        {
            commandName = commandName.ToLower();
            
            if (!database.ContainsKey(commandName))
            {
                Debug.LogError($"Command '{commandName}' does not exist in the database!");
                return null;
            }

            return database[commandName];

        }
    }
}