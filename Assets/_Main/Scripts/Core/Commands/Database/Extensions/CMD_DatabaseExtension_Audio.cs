using System;
using UnityEngine;

namespace COMMANDS
{
    public class CMD_DatabaseExtension_Audio : CMD_DatabaseExtension
    {
        private static string[] PARAM_SFX = new string[] { "-s", "-sfx" };
        private static string[] PARAM_VOLUME = new string[] { "-v", "-vol", "-volume" };
        private static string[] PARAM_PITCH = new string[] { "-p", "-pitch" };
        private static string[] PARAM_LOOP = new string[] { "-l", "-loop" };

        private static string[] PARAM_CHANNEL = new string[] {"-c","-channel"};
        private static string[] PARAM_IMMEDIATE = new string[] {"-i","-immediate" };
        private static string[] PARAM_START_VOLUME = new string[] {"-sv","-startvolume"};
        private static string[] PARAM_SONG = new string[] {"-s", "-song" };
        private static string[] PARAM_AMBIENCE = new string[] {"-a","-ambience"};

        new public static void Extend(CommandDataBase database)
        {
            database.AddCommand("playsfx", new Action<string[]>(PlaySFX));
            database.AddCommand("stopsfx", new Action<string>(StopSFX));

            database.AddCommand("playvoice", new Action<string[]>(PlayVoice));
            database.AddCommand("stopvoice", new Action<string>(StopSFX));

            database .AddCommand("playsong", new Action<string[]>(PlaySong));
            database.AddCommand("playambience", new Action<string[]>(PlayAmbience));

            database.AddCommand("stopsong", new Action<string>(StopSong));
            database.AddCommand("stopambience", new Action<string>(StopAmbience));
        }

        // Méthode pour jouer un effet sonore
        private static void PlaySFX(string[] data)
        {
            string filepath;
            float volume, pitch;
            bool loop;

            var parameters = ConvertDataToParameters(data);

            //Essayez d’obtenir le nom ou le chemin vers l’effet sonore
            parameters.TryGetValue(PARAM_SFX, out filepath);
            //Essayez d’obtenir le volume du son
            parameters.TryGetValue(PARAM_VOLUME, out volume, defaultValue: 1f);
            //Essayez d’obtenir la hauteur du son
            parameters.TryGetValue(PARAM_PITCH, out pitch, defaultValue: 1f);
            //Essayez d’obtenir si ce son boucle
            parameters.TryGetValue(PARAM_LOOP, out loop, defaultValue: false);

            //Exécuter le son de la logique AudioClip   
            string resourcesPath = FilePaths.GetPathToResource(FilePaths.resources_sfx, filepath);
            AudioClip sound = Resources.Load<AudioClip>(resourcesPath);

            if (sound == null)
                return;

            AudioManager.instance.PlaySoundEffect(sound, volume: volume, pitch: pitch, loop: loop, filePath: resourcesPath);

        }

        // Méthode pour arrêter un effet sonore
        private static void StopSFX(string data)
        {
            AudioManager.instance.StopSoundEffect(data);
        }

        // Méthode pour jouer une voix
        private static void PlayVoice(string[] data)
        {
            string filepath;
            float volume, pitch;
            bool loop;

            var parameters = ConvertDataToParameters(data);

            //Essayez d’obtenir le nom ou le chemin vers l’effet sonore            
            parameters.TryGetValue(PARAM_SFX, out filepath);
            //Essayez d’obtenir le volume du son
            parameters.TryGetValue(PARAM_VOLUME, out volume, defaultValue: 1f);
            //Essayez d’obtenir la hauteur du son
            parameters.TryGetValue(PARAM_PITCH, out pitch, defaultValue: 1f);
            //Essayez d’obtenir si cette boucle sonore
            parameters.TryGetValue(PARAM_LOOP, out loop, defaultValue: false);
            
            //Exécuter la logique
            AudioClip sound = Resources.Load<AudioClip>(FilePaths.GetPathToResource(FilePaths.resources_voices, filepath));
            if (sound == null){
                Debug.Log($"was not able to load voice '{filepath}'"); 
                return;
            }
                
            AudioManager.instance.PlayVoice(sound, volume: volume, pitch: pitch, loop: loop) ;
        }

        // Méthode pour jouer une chanson
        private static void PlaySong(string[] data){
            string filepath;
            int channel;

            var parameters = ConvertDataToParameters(data);
            //Essayez d’obtenir le nom ou le chemin vers la piste
            parameters.TryGetValue(PARAM_SONG, out filepath) ;
            filepath = FilePaths.GetPathToResource(FilePaths.resources_music, filepath);
            
            //Essayez d’obtenir le canal pour cette piste
            parameters.TryGetValue (PARAM_CHANNEL, out channel, defaultValue: 1);
        
            PlayTrack(filepath, channel, parameters);
        }
        
        // Méthode pour jouer une ambiance
        private static void PlayAmbience(string[] data){
            string filepath;
            int channel;
            var parameters = ConvertDataToParameters(data);
            
            //Essayez d’obtenir le nom ou le chemin vers la piste 
            parameters.TryGetValue(PARAM_AMBIENCE, out filepath);
            filepath = FilePaths.GetPathToResource(FilePaths.resources_ambience, filepath);
            
            //Essayez d’obtenir le canal pour cette piste
            parameters.TryGetValue(PARAM_CHANNEL, out channel, defaultValue: 0) ;
            
            PlayTrack(filepath, channel, parameters) ;
        }

        // Méthode générique pour jouer une piste audio
        private static void PlayTrack(string filepath, int channel, CommandParameters parameters){
            bool loop;
            float volumeCap; 
            float startVolume;
            float pitch;
            
            //Essayez d’obtenir le volume maximum de la piste
            parameters. TryGetValue(PARAM_VOLUME, out volumeCap, defaultValue: 1f);
            //Essayez d’obtenir le volume de départ de la piste
            parameters. TryGetValue(PARAM_START_VOLUME, out startVolume, defaultValue: 0f);
            //Essayez d’obtenir le pitch de la piste
            parameters.TryGetValue(PARAM_PITCH, out pitch, defaultValue: 1f);
            //Essayez d’obtenir si cette piste boucle
            parameters.TryGetValue(PARAM_LOOP, out loop, defaultValue: true);

            //Exécuter la logique
            AudioClip sound = Resources.Load<AudioClip>(filepath);

            if (sound == null){
                Debug.Log($"Was not able to load voice '{filepath}'");
                return;
            }

            AudioManager.instance.PlayTrack(sound, channel, loop, startVolume, volumeCap, pitch, filepath) ;
        }

        // Méthode pour arrêter une chanson
        private static void StopSong(string data){
            if(data == string.Empty){
                StopTrack("1");
            }else{
                StopTrack(data);
            }
        }

        // Méthode pour arrêter une ambiance
        private static void StopAmbience(string data){
            if(data == string.Empty){
                StopTrack("0");
            }else{
                StopTrack(data);
            }
        }

        // Méthode générique pour arrêter une piste audio
        private static void StopTrack(string data){
            if (int.TryParse(data, out int channel))
                AudioManager.instance.StopTrack(channel);
            else
                AudioManager.instance.StopTrack(data);
        }

        
    }
}

