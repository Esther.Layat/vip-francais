using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Video;

namespace COMMANDS
{
    public class CMD_DatabaseExtension_GraphicPanels : CMD_DatabaseExtension
    {
        private static string[] PARAM_PANEL = new string[] { "-p", "-panel"};
        private static string[] PARAM_LAYER = new string[] {"-l", "-layer" };
        private static string[] PARAM_MEDIA = new string[] {"-m", "-media" };
        private static string[] PARAM_GRAPHIC = new string[] {"-g", "-graphic" };
        private static string[] PARAM_SPEED = new string[] { "-spd", "-speed" };
        private static string[] PARAM_IMMEDIATE = new string[] {"-i", "-immediate"};
        private static string[] PARAM_BLENDTEX = new string[] {"-b", "-blend"};
        private static string[] PARAM_USEVIDEOAUDIO = new string[] {"-aud", "-audio"};

        private const string HOMEDIRECTORY_SYMBOL = "~/";

        new public static void Extend(CommandDataBase dataBase){
            dataBase.AddCommand("setlayermedia", new Func<string[], IEnumerator>(SetLayerMedia));
            dataBase.AddCommand("clearlayermedia", new Func<string[], IEnumerator>(ClearLayerMedia));
        }

        private static IEnumerator SetLayerMedia(string[] data){
            //Paramètres disponibles pour la chaîne de fonction
            string panelName = "";
            int layer = 0;
            string mediaName = "";
            float transitionSpeed = 0;
            bool immediate = false;
            string blendTexName = "";
            bool useAudio = false;

            string pathToGraphic = "";
            UnityEngine.Object graphic = null;
            Texture blendTex = null;

            //Maintenant obtenir les paramètres
            var parameters = ConvertDataToParameters(data);

            //Essayez d’obtenir le panneau auquel ce média est appliqué
            parameters.TryGetValue(PARAM_PANEL, out panelName);
            GraphicPanel panel = GraphicPanelManager.instance.GetPanel(panelName);
            if(panel == null){
                Debug. LogError($"Unable to grab panel '{panelName} because it is not a valid panel. Please check the panel name and adjust the command.");
                yield break;
            }

            //Essayez d’appliquer ce graphique au calque
            parameters.TryGetValue(PARAM_LAYER, out layer, defaultValue : 0);

            //Essayez d’obtenir les paramètres graphiques.
            parameters.TryGetValue(PARAM_MEDIA, out mediaName, defaultValue : "");

            //Essayez d’obtenir si c’est un effet immédiat ou non
            parameters. TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

            //Essayez d’obtenir la vitesse de la transition si ce n’est pas un effet immédiat            
            if (!immediate)
                parameters.TryGetValue(PARAM_SPEED, out transitionSpeed, defaultValue : 1);

            //Essayez d’obtenir la texture de mélange pour le média si nous en utilisons un 
            parameters.TryGetValue(PARAM_BLENDTEX, out blendTexName);

            //S’il s’agit d’une vidéo, essayez de savoir si nous utilisons l’audio de la vidéo ou non 
            parameters.TryGetValue(PARAM_USEVIDEOAUDIO, out useAudio, defaultValue: false);

            //Maintenant : exécuter la logique
            pathToGraphic = GetPathToGraphic(FilePaths.resources_backgroundImages, mediaName);
            graphic = Resources.Load<Texture>(pathToGraphic);

            if(graphic == null){
                pathToGraphic = GetPathToGraphic(FilePaths.resources_backgroundVideos, mediaName);
                graphic = Resources.Load<VideoClip>(pathToGraphic);
            }

            if(graphic == null){
                Debug.LogError($"Could not find media file called '{mediaName}' in the Resources directories. Please specify the full path within resources and make sure that the file exists!");
                yield break;
            }

            if(!immediate && blendTexName != string.Empty)
                blendTex = Resources.Load<Texture>(FilePaths.resources_blendTextures + blendTexName);

            //Permet d’essayer d’obtenir le calque pour appliquer le support 
            GraphicLayer graphicLayer = panel.GetLayer(layer, createIfDoesNotExist : true);

            if(graphic is Texture)
            {
                if(!immediate)
                    CommandManager.instance.AddTerminationActionToCurrentProcess(() => {graphicLayer?.SetTexture(graphic as Texture, filePath: pathToGraphic, immediate: true); });
                yield return graphicLayer.SetTexture(graphic as Texture, transitionSpeed, blendTex, pathToGraphic, immediate);
            }
            else 
            {
                if(!immediate)
                    CommandManager.instance.AddTerminationActionToCurrentProcess(() => {graphicLayer?.SetVideo(graphic as VideoClip, filePath: pathToGraphic, immediate: true); });
                yield return graphicLayer.SetVideo(graphic as VideoClip, transitionSpeed, useAudio, blendTex, pathToGraphic, immediate);
            }

        }

        private static IEnumerator ClearLayerMedia(string[] data){
            //Paramètres disponibles pour la chaîne de fonction
            string panelName = "";
            int layer = 0;
            float transitionSpeed = 0;
            bool immediate = false;
            string blendTexName = "";

            Texture blendTex = null;

            //Maintenant obtenir les paramètres
            var parameters = ConvertDataToParameters(data);

            //Essayez d’obtenir le panneau auquel ce média est appliqué
            parameters.TryGetValue(PARAM_PANEL, out panelName);
            GraphicPanel panel = GraphicPanelManager.instance.GetPanel(panelName);
            if(panel == null){
                Debug.LogError($"Unable to grab panel '{panelName} because it is not a valid panel. Please check the panel name and adjust the command.");
                yield break;
            }

            //Essayez d’appliquer ce graphique au calque
            parameters.TryGetValue(PARAM_LAYER, out layer, defaultValue : -1);

            //Essayez d’obtenir si c’est un effet immédiat ou non
            parameters. TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

            //Essayez d’obtenir la vitesse de la transition si ce n’est pas un effet immédiat            
            if (!immediate)
                parameters.TryGetValue(PARAM_SPEED, out transitionSpeed, defaultValue : 1);

            //Essayez d’obtenir la texture de mélange pour le média si nous en utilisons un 
            parameters.TryGetValue(PARAM_BLENDTEX, out blendTexName);

            if(!immediate && blendTexName != string.Empty)
                blendTex = Resources.Load<Texture>(FilePaths.resources_blendTextures + blendTexName);

            if(layer == -1)
                panel.Clear(transitionSpeed, blendTex, immediate);
            else
            {
                GraphicLayer graphicLayer = panel.GetLayer(layer);
                if(graphicLayer == null){
                    Debug.LogError($"Could not clear layer [{layer}] on panel '{panel.panelName}'");
                    yield break;
                }

                graphicLayer.Clear(transitionSpeed, blendTex, immediate);
            }

        }
        
        private static string GetPathToGraphic(string defaultPath, string graphicName){
            if(graphicName.StartsWith(HOMEDIRECTORY_SYMBOL))
                return graphicName.Substring(HOMEDIRECTORY_SYMBOL.Length);
                
            return defaultPath + graphicName;
        }
    }
}

