using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using CHARACTERS;


namespace COMMANDS
{
    public class CMD_DatabaseExtension_Characters : CMD_DatabaseExtension
    {
        private static string[] PARAM_ENABLE => new string[] { "-e", "-enable" };
        private static string[] PARAM_IMMEDIATE => new string[] { "-i", "-immediate" };
        private static string[] PARAM_SPEED => new string[] { "-spd", "-speed" };
        private static string[] PARAM_SMOOTH => new string[] { "-sm", "-smooth" };
        private static string[] PARAM_STATE => new string[] { "-s", "-state" };

        

        private static string PARAM_XPOS => "-x";
        private static string PARAM_YPOS => "-y";

        // Méthode pour étendre la base de données de commandes avec des commandes liées aux personnages.
        new public static void Extend(CommandDataBase dataBase)
        {
            dataBase.AddCommand("createcharacter", new Action<string[]>(CreateCharacter));
            dataBase.AddCommand("movecharacter", new Func<string[], IEnumerator>(MoveCharacter));
            dataBase.AddCommand("show", new Func<string[], IEnumerator>(ShowAll));
            dataBase.AddCommand("hide", new Func<string[], IEnumerator>(HideAll));
            dataBase.AddCommand("sort", new Action<string[]>(Sort));
            dataBase.AddCommand("highlight", new Func<string[], IEnumerator>(HighlightAll));
            dataBase.AddCommand("unhighlight", new Func<string[], IEnumerator>(UnhighlightAll));

            // Ajouter des commandes aux caractères
            CommandDataBase baseCommands = CommandManager.instance.CreateSubDatabase(CommandManager.DATABASE_CHARACTERS_BASE);
            baseCommands.AddCommand("move", new Func<string[], IEnumerator>(MoveCharacter));
            baseCommands.AddCommand("show", new Func<string[], IEnumerator>(Show));
            baseCommands.AddCommand("hide", new Func<string[], IEnumerator>(Hide));
            baseCommands.AddCommand("setpriority", new Action<string[]>(SetPriority));
            baseCommands.AddCommand("setposition", new Action<string[]>(SetPosition));
            baseCommands.AddCommand("faceleft", new Action<string[]>(FaceLeft));
            baseCommands.AddCommand("faceright", new Action<string[]>(FaceRight));
            baseCommands.AddCommand("flip", new Action<string[]>(Flip));
            baseCommands.AddCommand("animate", new Action<string[]>(Animate));
            baseCommands.AddCommand("setcolor", new Func<string[], IEnumerator>(SetColor));
            baseCommands.AddCommand("highlight", new Func<string[], IEnumerator>(Highlight));
            baseCommands.AddCommand("unhighlight", new Func<string[], IEnumerator>(Unhighlight));

            //Add character specific databases
            CommandDataBase spriteCommands = CommandManager.instance.CreateSubDatabase(CommandManager.DATABASE_CHARACTERS_SPRITE);
            spriteCommands.AddCommand("setsprite", new Func<string[], IEnumerator>(SetSprite));
        }

        // Méthode pour créer un personnage.
        public static void CreateCharacter(string[] data)
        {
            string characterName = data[0];
            bool enable = false;
            bool immediate = false;

            var parameters = ConvertDataToParameters(data);

            parameters.TryGetValue(PARAM_ENABLE, out enable, defaultValue: false);
            parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

            Character character = CharacterManager.instance.CreateCharacter(characterName);
            if (!enable)
                return;

            if (immediate)
                character.isVisible = true;
            else
                character.Show();
        }

        // Méthode pour trier les personnages.
        private static void Sort(string[] data)
        {
            CharacterManager.instance.SortCharacters(data);
        }

        // Méthode pour déplacer un personnage.
        private static IEnumerator MoveCharacter(string[] data)
        {
            string characterName = data[0];
            Character character = CharacterManager.instance.GetCharacter(characterName);

            if (character == null)
            {
                yield break;
            }

            float x = 0, y = 0;
            float speed = 1;
            bool smooth = false;
            bool immediate = false;

            var parameters = ConvertDataToParameters(data);

            //Essayer d’obtenir la position de l’axe des x
            parameters.TryGetValue(PARAM_XPOS, out x);

            //Essayer d’obtenir la position de l’axe des y
            parameters.TryGetValue(PARAM_YPOS, out y);

            //Essayer d’obtenir la vitesse
            parameters.TryGetValue(PARAM_SPEED, out speed, defaultValue: 1);

            //Essayez d’obtenir le lissage
            parameters.TryGetValue(PARAM_SMOOTH, out smooth, defaultValue: false);

            //Essayez d’obtenir la position immediate
            parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

            Vector2 position = new Vector2(x, y);

            if (immediate)
            {
                character.SetPosition(position);
            }
            else
            {
                CommandManager.instance.AddTerminationActionToCurrentProcess(() => { character?.SetPosition(position); });
                yield return character.MoveToPosition(position, speed, smooth);
            }
        }


        // Méthode pour révéler tous les personnages spécifiés.
        public static IEnumerator ShowAll(string[] data)
        {
            List<Character> characters = new List<Character>();
            bool immediate = false;
            float speed = 1f;

            foreach (string s in data)
            {
                Character character = CharacterManager.instance.GetCharacter(s, createIfDoesNotExist: false);
                if (character != null)
                    characters.Add(character);
            }

            if (characters.Count == 0)
            {
                yield break;
            }

            //Convertir le tableau de données dans un conteneur de paramètres
            var parameters = ConvertDataToParameters(data);

            parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);
            parameters.TryGetValue(PARAM_SPEED, out speed, defaultValue: 1f);

            //Appeler la logique sur tous les caractères
            foreach (Character character in characters)
            {
                if (immediate)
                    character.isVisible = true;
                else
                    character.Show(speed);
            }

            if (!immediate)
            {
                CommandManager.instance.AddTerminationActionToCurrentProcess(() =>
                {
                    foreach (Character character in characters)
                        character.isVisible = true;
                });

                while (characters.Any(c => c.isRevealing))
                    yield return null;
            }

        }

        // Méthode pour cacher tous les personnages spécifiés.
        public static IEnumerator HideAll(string[] data)
        {
            List<Character> characters = new List<Character>();
            bool immediate = false;
            float speed = 1f;

            foreach (string s in data)
            {
                Character character = CharacterManager.instance.GetCharacter(s, createIfDoesNotExist: false);
                if (character != null)
                    characters.Add(character);
            }

            if (characters.Count == 0)
            {
                yield break;
            }

            //Convertir le tableau de données dans un conteneur de paramètres
            var parameters = ConvertDataToParameters(data);

            parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);
            parameters.TryGetValue(PARAM_SPEED, out speed, defaultValue: 1f);

            //Appeler la logique sur tous les caractères
            foreach (Character character in characters)
            {
                if (immediate)
                    character.isVisible = false;
                else
                    character.Hide(speed);
            }

            if (!immediate)
            {
                CommandManager.instance.AddTerminationActionToCurrentProcess(() =>
                {
                    foreach (Character character in characters)
                        character.isVisible = false;
                });
                while (characters.Any(c => c.isHidding))
                    yield return null;
            }
        }

        // Méthode pour mettre en surbrilance tous les personnages spécifiés.
        public static IEnumerator HighlightAll(string[] data)
        {
            List<Character> characters = new List<Character>();
            bool immediate = false;
            bool handleUnspecifiedCharacters = true;
            List<Character> unspecifiedCharacters = new List<Character>();

            //Ajouter les caractères à mettre en surbrillance.
            for (int i = 0; i < data.Length; i++)
            {
                Character character = CharacterManager.instance.GetCharacter(data[i], createIfDoesNotExist: false);
                if (character != null)
                    characters.Add(character);
            }
            if (characters.Count == 0)
                yield break;

            //Saisir les paramètres supplémentaires
            var parameters = ConvertDataToParameters(data, startingIndex: 1);
            parameters.TryGetValue(new string[] { "-i", "-immediate" }, out immediate, defaultValue: false);
            parameters.TryGetValue(new string[] { "-o", "-only" }, out handleUnspecifiedCharacters, defaultValue: true);

            //Faire exécuter la logique par tous les caractères
            foreach (Character character in characters)
                character.Highlight(immediate: immediate);

            //Si nous forçons des caractères non spécifiés à utiliser le statut en surbrillance opposé
            if (handleUnspecifiedCharacters)
            {
                foreach (Character character in CharacterManager.instance.allCharacters)
                {
                    if (characters.Contains(character))
                        continue;
                    unspecifiedCharacters.Add(character);
                    character.UnHighlight(immediate: immediate);
                }
            }

            //Attendre que tous les caractères terminent la mise en surbrillance
            if (!immediate)
            {
                CommandManager.instance.AddTerminationActionToCurrentProcess(() =>
                {
                    foreach (var character in characters)
                        character.Highlight(immediate: true);

                    if (!handleUnspecifiedCharacters) return;

                    foreach (var character in unspecifiedCharacters)
                        character.UnHighlight(immediate: true);
                });
                while (characters.Any(c => c.isHighlighting) || (handleUnspecifiedCharacters && unspecifiedCharacters.Any(uc => uc.isUnHighlighting)))
                    yield return null;
            }

        }

        // Méthode pour mettre en mode sombre tous les personnages spécifiés.
        public static IEnumerator UnhighlightAll(string[] data)
        {
            List<Character> characters = new List<Character>();
            bool immediate = false;
            bool handleUnspecifiedCharacters = true;
            List<Character> unspecifiedCharacters = new List<Character>();

            //Ajouter les caractères spécifiés pour être mis en sombre.
            for (int i = 0; i < data.Length; i++)
            {
                Character character = CharacterManager.instance.GetCharacter(data[i], createIfDoesNotExist: false);
                if (character != null)
                    characters.Add(character);
            }
            if (characters.Count == 0)
                yield break;

            //Saisir les paramètres supplémentaires
            var parameters = ConvertDataToParameters(data, startingIndex: 1);
            parameters.TryGetValue(new string[] { "-i", "-immediate" }, out immediate, defaultValue: false);
            parameters.TryGetValue(new string[] { "-o", "-only" }, out handleUnspecifiedCharacters, defaultValue: true);

            //Faire exécuter la logique par tous les caractères
            foreach (Character character in characters)
                character.UnHighlight(immediate: immediate);

            //Si nous forçons des caractères non spécifiés à utiliser le statut non affiché opposé
            if (handleUnspecifiedCharacters)
            {
                foreach (Character character in CharacterManager.instance.allCharacters)
                {
                    if (characters.Contains(character))
                        continue;
                    unspecifiedCharacters.Add(character);
                    character.UnHighlight(immediate: immediate);
                }
            }

            //Attendre que tous les caractères aient fini de s’afficher
            if (!immediate)
            {
                CommandManager.instance.AddTerminationActionToCurrentProcess(() =>
                {
                    foreach (var character in characters)
                        character.UnHighlight(immediate: true);

                    if (!handleUnspecifiedCharacters) return;

                    foreach (var character in unspecifiedCharacters)
                        character.Highlight(immediate: true);
                });
                while (characters.Any(c => c.isUnHighlighting) || (handleUnspecifiedCharacters && unspecifiedCharacters.Any(uc => uc.isHighlighting)))
                    yield return null;
            }

        }

        #region BASE CHARACTER COMMANDS

        // Méthode pour montrer le personnage
        private static IEnumerator Show(string[] data)
        {
            Character character = CharacterManager.instance.GetCharacter(data[0]);

            if (character == null)
                yield break;

            bool immediate = false;
            var parameters = ConvertDataToParameters(data);

            parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

            if (immediate)
                character.isVisible = true;
            else
            {
                //Un processus long devrait avoir une action d’arrêt pour annuler la coroutine et exécuter la logique qui devrait terminer cette commande 
                CommandManager.instance.AddTerminationActionToCurrentProcess(() => { if (character != null) character.isVisible = true; });
                yield return character.Show();
            }
        }

        // Méthode pour cacher le personnage
        private static IEnumerator Hide(string[] data)
        {
            Character character = CharacterManager.instance.GetCharacter(data[0]);

            if (character == null)
                yield break;

            bool immediate = false;
            var parameters = ConvertDataToParameters(data);

            parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

            if (immediate)
                character.isVisible = false;
            else
            {
                //Un processus long devrait avoir une action d’arrêt pour annuler la coroutine et exécuter la logique qui devrait terminer cette commande                
                CommandManager.instance.AddTerminationActionToCurrentProcess(() => { if (character != null) character.isVisible = false; });
                yield return character.Hide();
            }
        }

        // Méthode pour donner une priorité au personnage
        public static void SetPriority(string[] data)
        {
            Character character = CharacterManager.instance.GetCharacter(data[0], createIfDoesNotExist: false);
            int priority;

            if (character == null || data.Length < 2)
                return;

            if (!int.TryParse(data[1], out priority))
                priority = 0;

            character.SetPriority(priority);
        }

        // Méthode pour donner une couleur au personnage
        public static IEnumerator SetColor(string[] data)
        {
            Character character = CharacterManager.instance.GetCharacter(data[0], createIfDoesNotExist: false);
            string colorName;
            float speed;
            bool immediate;

            if (character == null || data.Length < 2)
                yield break;

            //Saisir les paramètres supplémentaires
            var parameters = ConvertDataToParameters(data, startingIndex: 1);

            //Essayez d’obtenir le nom de la couleur
            parameters.TryGetValue(new string[] { "-c", "-color" }, out colorName);

            //Essayez d’obtenir la vitesse de la transition
            bool specifiedSpeed = parameters.TryGetValue(new string[] { "-spd", "-speed" }, out speed, defaultValue: 1f);

            //Essayez d’obtenir la valeur instantanée
            if (!specifiedSpeed)
                parameters.TryGetValue(new string[] { "-i", "-immediate" }, out immediate, defaultValue: true);
            else
                immediate = false;

            //Récupère la valeur de couleur du nom
            Color color = Color.white;
            color = color.GetColorFromName(colorName);

            if (immediate)
                character.SetColor(color);
            else
            {
                CommandManager.instance.AddTerminationActionToCurrentProcess(() => { character?.SetColor(color); });
                character.TransitionColor(color, speed);
            }
            yield break;
        }

        // Méthode pour donner en surbrillance le personnage
        public static IEnumerator Highlight(string[] data)
        {
            //format: SetSprite(character sprite)
            Character character = CharacterManager.instance.GetCharacter(data[0], createIfDoesNotExist: false) as Character;

            if (character == null)
                yield break;
            bool immediate = false;

            //Saisir les paramètres supplémentaires
            var parameters = ConvertDataToParameters(data, startingIndex: 1);

            parameters.TryGetValue(new string[] { "-i", "-immediate" }, out immediate, defaultValue: false);

            if (immediate)
                character.Highlight(immediate: true);
            else
            {
                CommandManager.instance.AddTerminationActionToCurrentProcess(() => { character?.Highlight(immediate: true); });
                yield return character.Highlight();
            }
        }

        // Méthode pour mettre en mode sombre le personnage
        public static IEnumerator Unhighlight(string[] data)
        {
            //format: SetSprite (character sprite)
            Character character = CharacterManager.instance.GetCharacter(data[0], createIfDoesNotExist: false) as Character;

            if (character == null)
                yield break;

            bool immediate = false;

            //Saisir les paramètres supplémentaires
            var parameters = ConvertDataToParameters(data, startingIndex: 1);
            parameters.TryGetValue(new string[] { "-i", "-immediate" }, out immediate, defaultValue: false);

            if (immediate)
                character.UnHighlight(immediate: true);
            else
            {
                CommandManager.instance.AddTerminationActionToCurrentProcess(() => { character?.Highlight(immediate: true); });
                yield return character.UnHighlight();
            }

        }

        // Méthode pour déplacer le personnage
        public static void SetPosition(string[] data)
        {
            Character character = CharacterManager.instance.GetCharacter(data[0], createIfDoesNotExist: false);
            float x = 0, y = 0;

            if (character == null || data.Length < 2)
                return;

            var parameters = ConvertDataToParameters(data, 1);
            parameters.TryGetValue(PARAM_XPOS, out x, defaultValue: 0);
            parameters.TryGetValue(PARAM_YPOS, out y, defaultValue: 0);
            character.SetPosition(new Vector2(x, y));

        }

        public static void FaceRight(string[] data)
        {
            Character character = CharacterManager.instance.GetCharacter(data[0], createIfDoesNotExist: false);
            character.FaceRight();

        }

        public static void FaceLeft(string[] data)
        {
            Character character = CharacterManager.instance.GetCharacter(data[0], createIfDoesNotExist: false);
            character.FaceLeft();

        }

        public static void Flip(string[] data)
        {
            Character character = CharacterManager.instance.GetCharacter(data[0], createIfDoesNotExist: false);
            character.Flip();
        }

        public static void Animate(string[] data)
        {
            Character character = CharacterManager.instance.GetCharacter(data[0], createIfDoesNotExist: false);
            var parameters = ConvertDataToParameters(data, 1);
            bool state;
            if (parameters.TryGetValue(PARAM_STATE, out state))
                character.Animate(data[1], state);
            else 
                character.Animate(data[1]);
            
        }
        
        #endregion

        #region SPRITE CHARACTER COMMANDS

        //Méthode qui modifie un sprite pour un personnage, ex : change l'humeur
        public static IEnumerator SetSprite(string[] data)
        {
            //format: SetSprite(character sprite)
            Character_Sprite character = CharacterManager.instance.GetCharacter(data[0], createIfDoesNotExist: false) as Character_Sprite;
            int layer = 0;
            string spriteName;
            bool immediate = false;
            float speed;

            if (character == null || data.Length < 2)
                yield break;

            //Saisir les paramètres supplémentaires
            var parameters = ConvertDataToParameters(data, startingIndex: 1);

            //Essayez d’obtenir le nom du sprite
            parameters.TryGetValue(new string[] { "-s", "-sprite" }, out spriteName);

            //Essayez d’obtenir le calque
            parameters.TryGetValue(new string[] { "-l", "-layer" }, out layer, defaultValue: 0);

            //Essayez d’obtenir la vitesse de transition
            bool specifiedSpeed = parameters.TryGetValue(PARAM_SPEED, out speed, defaultValue: 0.1f);

            //Essayez de savoir s’il s’agit d’une transition immédiate ou non
            if (!specifiedSpeed)
                parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: true);

            //Exécuter la logique
            Sprite sprite = character.GetSprite(spriteName);

            Debug.Log(character.name + spriteName);

            if (sprite == null)
                yield break;

            if (immediate)
                character.SetSprite(sprite, layer);

            else {
                CommandManager.instance.AddTerminationActionToCurrentProcess (() => { character?.SetSprite(sprite, layer); });
                yield return character. TransitionSprite(sprite, layer, speed);
            }
        }

        #endregion
    }
}

