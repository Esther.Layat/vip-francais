using System;
using System.Collections;
using UnityEngine;

namespace COMMANDS
{
    public class CMD_DatabaseExtension_Gallery : CMD_DatabaseExtension
    {
        private static string[] PARAM_MEDIA = new string[] {"-m", "-media" };
        private static string[] PARAM_SPEED = new string[] { "-spd", "-speed" };
        private static string[] PARAM_IMMEDIATE = new string[] {"-i", "-immediate"};
        private static string[] PARAM_BLENDTEX = new string[] {"-b", "-blend"};

        // Étendre la base de données de commandes
        new public static void Extend(CommandDataBase database)
        {
            database.AddCommand("showgalleryimage", new Func<string[], IEnumerator>(ShowGalleryImage));
            database.AddCommand ("hidegalleryimage", new Func<string[], IEnumerator>(HideGalleryImage));
        }

        // Commande pour cacher l'image de la galerie
        public static IEnumerator HideGalleryImage(string[] data)
        {
            GraphicLayer graphicLayer = GraphicPanelManager.instance.GetPanel("Cinematic").GetLayer(0, createIfDoesNotExist: true);

            if(graphicLayer.currentGraphic == null)
                yield break;

            float transitionSpeed = 0;
            bool immediate = false;
            string blendTexName = "";
            Texture blendTex = null;

            //Maintenant obtenir les paramètres
            var parameters = ConvertDataToParameters(data);

            //Essayez d’obtenir si c’est un effet immédiat ou non
            parameters. TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

            //Essayez d’obtenir la vitesse de la transition si ce n’est pas un effet immédiat            
            if (!immediate)
                parameters.TryGetValue(PARAM_SPEED, out transitionSpeed, defaultValue : 1);

            //Essayez d’obtenir la texture de mélange pour le média si nous en utilisons un 
            parameters.TryGetValue(PARAM_BLENDTEX, out blendTexName);

            if(!immediate && blendTexName != string.Empty)
                blendTex = Resources.Load<Texture>(FilePaths.resources_blendTextures + blendTexName);

            if(!immediate)
                CommandManager.instance.AddTerminationActionToCurrentProcess ( () => {Debug.Log("CLEAR"); graphicLayer.Clear(immediate: true);});

            graphicLayer.Clear(transitionSpeed, blendTex, immediate);

            if(graphicLayer.currentGraphic != null)
            {
                var graphicObject = graphicLayer.currentGraphic;
                yield return new WaitUntil(() => graphicObject == null);
            }
                
        }

        // Commande pour afficher l'image de la galerie
        public static IEnumerator ShowGalleryImage(string[] data)
        {
            string mediaName = "";
            float transitionSpeed = 0;
            bool immediate = false;
            string blendTexName = "";

            Texture blendTex = null;

            //Maintenant obtenir les paramètres
            var parameters = ConvertDataToParameters(data);

            //Essayez d’obtenir les paramètres graphiques.
            parameters.TryGetValue(PARAM_MEDIA, out mediaName, defaultValue : "");

            //Essayez d’obtenir si c’est un effet immédiat ou non
            parameters. TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

            //Essayez d’obtenir la vitesse de la transition si ce n’est pas un effet immédiat            
            if (!immediate)
                parameters.TryGetValue(PARAM_SPEED, out transitionSpeed, defaultValue : 1);

            //Essayez d’obtenir la texture de mélange pour le média si nous en utilisons un 
            parameters.TryGetValue(PARAM_BLENDTEX, out blendTexName);

            string pathToGraphic = FilePaths.resources_gallery + mediaName;

            Texture graphic = Resources.Load<Texture>(pathToGraphic);

            if(graphic == null)
            {
                Debug.LogError($"Could not find gallery image called '{mediaName}'• in the Resources '{FilePaths.resources_gallery}' directory.");
                yield break;
            }

            if (! immediate && blendTexName != string.Empty)
                blendTex = Resources.Load<Texture>(FilePaths.resources_blendTextures + blendTexName);

            //Permet d’essayer d’obtenir la couche pour appliquer le support à
            GraphicLayer graphicLayer = GraphicPanelManager.instance.GetPanel("Cinematic").GetLayer(0, createIfDoesNotExist: true);

            if(!immediate)
                CommandManager.instance.AddTerminationActionToCurrentProcess(() => {graphicLayer?.SetTexture(graphic, filePath: pathToGraphic, immediate : true);});

            GalleryConfig.UnlockImage(mediaName);

            yield return graphicLayer.SetTexture(graphic, transitionSpeed, blendTex, pathToGraphic, immediate);
        }
    }
}

