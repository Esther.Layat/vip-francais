using System;

namespace COMMANDS
{
    public class CMD_DatabaseExtension_VisualNovel : CMD_DatabaseExtension
    {
        new public static void Extend(CommandDataBase dataBase)
        {
            //Variable assigment
            dataBase.AddCommand("setplayername", new Action<string>(SetPlayerNameVariable));
            dataBase.AddCommand("setplayername2", new Action<string>(SetPlayerNameVariable2));
        }

        // Méthode pour définir la variable playerName dans le fichier de sauvegarde actif
        private static void SetPlayerNameVariable(string data)
        {
            VISUALNOVEL.VNGameSave.activeFile.playerName = data;
        }

        private static void SetPlayerNameVariable2(string data)
        {
            VISUALNOVEL.VNGameSave.activeFile.playerName2 = data;
        }
    }
}

