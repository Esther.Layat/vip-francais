using System;
using System.Collections;
using UnityEngine;
using COMMANDS;

namespace TESTING
{
    public class CMD_DatabaseExtension_Examples : CMD_DatabaseExtension
    {
        // Méthode statique pour étendre la base de données de commandes
        new public static void Extend(CommandDataBase database)
        {
            // Ajouter une commande sans paramètres
            database.AddCommand("print", new Action(PrintDefaultMessage));
            // Ajouter une commande avec un paramètre
            database.AddCommand("print_1p", new Action<string>(PrintUsermessage));
            // Ajouter une commande avec plusieurs paramètres
            database.AddCommand("print_mp", new Action<string[]>(PrintLines));

            // Ajouter lambda sans paramètres
            database.AddCommand("lambda", new Action(() => { Debug.Log("Printing a default message to console from lambda command."); }));
            // Ajouter une lambda avec un paramètre
            database.AddCommand("lambda_1p", new Action<string>((arg) => { Debug.Log($"Log User Lambda Message: '{arg}'"); }));
            // Ajouter une lambda avec plusieurs paramètres
            database.AddCommand("lambda_mp", new Action<string[]>((args) => { Debug.Log(string.Join(", ", args)); }));

            // Add couroutine sans parameters
            database.AddCommand("process", new Func<IEnumerator>(SimpleProcess));
            // Ajouter une coroutine avec un paramètre
            database.AddCommand("process_1p", new Func<string, IEnumerator>(LineProcess));
            // Ajouter une coroutine avec plusieurs paramètres
            database.AddCommand("process_mp", new Func<string[], IEnumerator>(MultiLineProcess));

            // Exemple spécial
            database.AddCommand("moveCharDemo", new Func<string, IEnumerator>(MoveCharacter));

        }

        // Méthode pour imprimer un message par défaut
        private static void PrintDefaultMessage()
        {
            Debug.Log("Printing a default message to console.");
        }

        // Méthode pour imprimer un message utilisateur
        private static void PrintUsermessage(string message)
        {
            Debug.Log($"User Message: '{message}'");
        }

        // Méthode pour imprimer plusieurs lignes
        private static void PrintLines(string[] lines)
        {
            int i = 1;
            foreach (string line in lines)
            {
                Debug.Log($"{i++}. '{line}'");
            }
        }

        // Coroutine pour un processus simple
        private static IEnumerator SimpleProcess()
        {
            for (int i = 0; i <= 5; i++)
            {
                Debug.Log($"Process Running... [{i}]");
                yield return new WaitForSeconds(1);
            }
        }

        // Coroutine pour un processus avec un paramètre
        private static IEnumerator LineProcess(string data)
        {
            if (int.TryParse(data, out int num))
            {
                for (int i = 0; i <= num; i++)
                {
                    Debug.Log($"Process Running... [{i}]");
                    yield return new WaitForSeconds(1);
                }
            }
        }

        // Coroutine pour un processus avec plusieurs paramètres
        private static IEnumerator MultiLineProcess(string[] data)
        {
            foreach (string line in data)
            {
                Debug.Log($"Process Message: '{line}'");
                yield return new WaitForSeconds(0.5f);
            }
        }

        // Coroutine pour déplacer un personnage
        private static IEnumerator MoveCharacter(string direction)
        {
            bool left = direction.ToLower() == "left";

            // Obtenir les variables dont on a besoin. Cela serait défini ailleurs.
            Transform character = GameObject.Find("Image").transform;
            float moveSpeed = 15;

            // Calculer la position cible de l’image
            float targetX = left ? -8 : 8;

            // Calculer la position actuelle de l’image
            float currentX = character.position.x;

            // Déplacer l’image progressivement vers la position cible 
            while (Mathf.Abs(targetX - currentX) > 0.1f)
            {
                currentX = Mathf.MoveTowards(currentX, targetX, moveSpeed * Time.deltaTime);
                character.position = new Vector3(currentX, character.position.y, character.position.z);
                yield return null;
            }
        }
    }
}
