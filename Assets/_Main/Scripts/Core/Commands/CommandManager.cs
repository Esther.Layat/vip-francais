using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System.Linq;
using System;
using UnityEngine.Events;
using CHARACTERS;

namespace COMMANDS
{
    // Classe qui gère l'exécution des commandes
    public class CommandManager : MonoBehaviour
    {
        private const char SUB_COMMAND_IDENTIFIER = '.';
        public const string DATABASE_CHARACTERS_BASE = "characters";
        public const string DATABASE_CHARACTERS_SPRITE = "characters_sprite";
        public const string DATABASE_CHARACTERS_LIVE2D = "characters_live2D";
        public const string DATABASE_CHARACTERS_MODEL3D = "characters_model3D";

        // Instance statique du gestionnaire de commandes
        public static CommandManager instance { get; private set; }

        // Coroutine de processus en cours
        private static Coroutine process = null;

        // Vérifie si un processus est en cours d'exécution
        private static bool isRunningProcess => process != null;
        private CommandDataBase database;
        private Dictionary<string, CommandDataBase> subDatabases = new Dictionary<string, CommandDataBase>();

        private List<CommandProcess> activeProcesses = new List<CommandProcess>();
        private CommandProcess topProcess => activeProcesses.Last();

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                database = new CommandDataBase();
                Assembly assembly = Assembly.GetExecutingAssembly();
                Type[] extensionTypes = assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(CMD_DatabaseExtension))).ToArray();

                foreach (Type extension in extensionTypes)
                {
                    MethodInfo extendMethod = extension.GetMethod("Extend");
                    extendMethod.Invoke(null, new object[] { database });
                }
            }
            else
                DestroyImmediate(gameObject);
        }

        // Méthode pour exécuter une commande avec des arguments
        public CoroutineWrapper Execute(string commandName, params string[] args)
        {
            if(commandName.Contains(SUB_COMMAND_IDENTIFIER))
                return ExecuteSubCommand(commandName, args);
            Delegate command = database.GetCommand(commandName);

            if (command == null)
                return null;

            return StartProcess(commandName, command, args);
        }

        // Méthode qui gère l'exécution de sous-commandes
        private CoroutineWrapper ExecuteSubCommand(string commandName, string[] args){
            string[] parts = commandName.Split(SUB_COMMAND_IDENTIFIER);
            string databaseName = string.Join(SUB_COMMAND_IDENTIFIER, parts.Take(parts.Length - 1));
            string subCommandName = parts.Last();

            if(subDatabases.ContainsKey(databaseName)){
                Delegate command = subDatabases[databaseName].GetCommand(subCommandName);
                if(command != null){
                    return StartProcess(commandName, command, args);
                }
                else {
                    Debug.LogError($"No command called '{subCommandName}' was found in sub database '{databaseName}'");
                    return null;
                }
            }

            string characterName = databaseName;
            //Si nous l’avons fait ici alors nous devrions essayer d’exécuter comme une commande de caractère
            if(CharacterManager.instance.HasCharacter(databaseName)){
                List<string> newArgs = new List<string>(args);
                newArgs.Insert(0, characterName);
                args = newArgs.ToArray();

                return ExecuteCharacterCommand(subCommandName, args);
            } 

            Debug.LogError($"No sub database called '{databaseName}' exists! Commans '{subCommandName}' could not be run.");
            return null;
        }
        
        // Méthode qui gère l'exécution de sous-commandes spécifique pour un personnage
        private CoroutineWrapper ExecuteCharacterCommand(string commandName, params string[] args){
            Delegate command = null;

            CommandDataBase db = subDatabases[DATABASE_CHARACTERS_BASE];
            if(db.HasCommands(commandName)){
                command = db.GetCommand(commandName);
                return StartProcess(commandName, command, args);
            }

            CharacterConfigData characterConfigData = CharacterManager.instance.GetCharacterConfig(args[0]);
            switch(characterConfigData.characterType){
                case Character.CharacterType.Sprite:
                case Character.CharacterType.SpriteSheet:
                    db = subDatabases[DATABASE_CHARACTERS_SPRITE];
                    break;
                case Character.CharacterType.Live2D:
                    db = subDatabases[DATABASE_CHARACTERS_LIVE2D];
                    break;
                case Character.CharacterType.Model3D:
                    db = subDatabases[DATABASE_CHARACTERS_MODEL3D];
                    break;
            }
            command = db.GetCommand(commandName);

            if(command != null){
                return StartProcess(commandName, command, args);
            }
            
            Debug.LogError($"Command Manger was unable to execute command '{commandName}' on character '{args[0]}'. The character name or command may be invalid.");
            return null;
        }

        // Méthode pour démarrer un processus de commande
        private CoroutineWrapper StartProcess(string commandName, Delegate command, string[] args)
        {
            System.Guid processID = System.Guid.NewGuid();
            CommandProcess cmd = new CommandProcess(processID, commandName, command, null, args, null);
            activeProcesses.Add(cmd);

            Coroutine co = StartCoroutine(RunningProcess(cmd));
            cmd.runningProcess = new CoroutineWrapper(this, co);
            return cmd.runningProcess;
        }

        // Méthode pour arrêter le processus en cours
        public void StopCurrentProcess()
        {
            if (topProcess != null)
                KillProcess(topProcess);

        }

        // Méthode qui arrête tous les processus en cours
        public void StopAllProcesses(){
            foreach(var c  in activeProcesses){
                if(c.runningProcess != null && !c.runningProcess.IsDone)
                    c.runningProcess.Stop();
                c.onTerminateAction?.Invoke();
            }

            activeProcesses.Clear();
        }

        // Méthode qui exécute le processus de commande
        private IEnumerator RunningProcess(CommandProcess process)
        {
            yield return WaitingForProcessToComplete(process.command, process.args);

            KillProcess(process);
        }

        // Méthode qui arrête un processus spécifique et effectue des actions de nettoyage associées
        public void KillProcess(CommandProcess cmd){
            activeProcesses.Remove(cmd);

            if(cmd.runningProcess != null && !cmd.runningProcess.IsDone)
                cmd.runningProcess.Stop();
            
            cmd.onTerminateAction?.Invoke();
        }

        // Méthode qui attend la fin du processus de commande
        private IEnumerator WaitingForProcessToComplete(Delegate command, string[] args)
        {
            if (command is Action)
                command.DynamicInvoke();

            else if (command is Action<string>)
                command.DynamicInvoke(args.Length == 0 ? string.Empty : args[0]);

            else if (command is Action<string[]>)
                command.DynamicInvoke((object)args);

            else if (command is Func<IEnumerator>)
                yield return ((Func<IEnumerator>)command)();

            else if (command is Func<string, IEnumerator>)
                yield return ((Func<string, IEnumerator>)command)(args.Length == 0 ? string.Empty : args[0]);

            else if (command is Func<string[], IEnumerator>)
                yield return ((Func<string[], IEnumerator>)command)(args);
        }

        // Méthode permet de spécifier une action à exécuter une fois que le processus en cours est terminé
        public void AddTerminationActionToCurrentProcess(UnityAction action){
            CommandProcess process = topProcess;
            
            if(topProcess == null){
                return;
            }

            topProcess.onTerminateAction = new UnityEvent();
            topProcess.onTerminateAction.AddListener(action);
        }

        // Méthode qui crée une nouvelle base de données de commandes pour stocker 
        // des commandes spécifiques à un certain contexte ou à une certaine catégorie
        public CommandDataBase CreateSubDatabase(string name){
            name = name.ToLower();

            if(subDatabases.TryGetValue(name, out CommandDataBase db)){
                Debug.LogWarning($"Adatabase by the name of '{name} already exists!");
                return db;
            }

            CommandDataBase newDatabase = new CommandDataBase();
            subDatabases.Add(name, newDatabase);

            return newDatabase;
        }
    }
}
