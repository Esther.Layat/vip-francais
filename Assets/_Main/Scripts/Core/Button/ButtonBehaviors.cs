using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonBehaviors : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private static ButtonBehaviors selectedButton = null;
    public Animator anim;

    // Méthode appelée lorsque le pointeur quitte le bouton
    public void OnPointerExit(PointerEventData eventData)
    {
        anim.Play("Exit");
    }

    // Méthode appelée lorsque le pointeur entre dans le bouton
    public void OnPointerEnter(PointerEventData eventData)
    {
        if(selectedButton != null && selectedButton != this)
        {
            selectedButton.OnPointerExit(null);
        }

        anim.Play("Enter");
        selectedButton = this;
    }

    
}
