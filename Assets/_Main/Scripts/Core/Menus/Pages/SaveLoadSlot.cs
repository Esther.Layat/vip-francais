
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using VISUALNOVEL;
using History;

public class SaveLoadSlot : MonoBehaviour
{
    public GameObject root;
    public RawImage previewImage;
    public TextMeshProUGUI titleText;
    public Button deleteButton;
    public Button saveButton;
    public Button loadButton;

    [HideInInspector] public int fileNumber = 0;
    [HideInInspector] public string filePath = "";

    private UIConfirmationMenu uiChoiceMenu => UIConfirmationMenu.instance;

    // Méthode pour peupler les détails du slot de sauvegarde en fonction de la fonction du menu (sauvegarder ou charger)
    public void PopulateDetails(SaveAndLoadMenu.MenuFunction function)
    {
        if (File.Exists(filePath))
        {
            VNGameSave file = VNGameSave.Load(filePath);
            PopulateDetailsFromFile(function, file);
        }
        else
        {
            PopulateDetailsFromFile(function, null);
        }
    }

    // Méthode privée pour peupler les détails à partir d'un fichier de sauvegarde
    private void PopulateDetailsFromFile(SaveAndLoadMenu.MenuFunction function, VNGameSave file)
    {
        if (file == null)
        {
            titleText.text = $"{fileNumber}. Fichier vide";
            deleteButton.gameObject.SetActive(false);
            loadButton.gameObject.SetActive(false);
            saveButton.gameObject.SetActive(function == SaveAndLoadMenu.MenuFunction.save);
            previewImage.texture = SaveAndLoadMenu.Instance.emptyFileImage;
        }
        else
        {
            titleText.text = $"{fileNumber}. {file.timestamp}";

            deleteButton.gameObject.SetActive(true);
            loadButton.gameObject.SetActive(function == SaveAndLoadMenu.MenuFunction.load);
            saveButton.gameObject.SetActive(function == SaveAndLoadMenu.MenuFunction.save);

            byte[] data = File.ReadAllBytes(file.screenshotPath);
            Texture2D screenshotPreview = new Texture2D(1, 1);
            ImageConversion.LoadImage(screenshotPreview, data);
            previewImage.texture = screenshotPreview;
        }
    }

    // Méthode pour supprimer un fichier de sauvegarde
    public void Delete()
    {
        uiChoiceMenu.Show(
            // Titre
            "Effacer ce fichier ? (<i>Attention se sera définitif!</i>)", 
            //Choix 1
            new UIConfirmationMenu.ConfirmationButton("Oui", () =>
                {
                    uiChoiceMenu.Show(
                        "Êtes vous sûr ?",
                        new UIConfirmationMenu.ConfirmationButton("Je suis sûre", OnConfirmDelete),
                        new UIConfirmationMenu.ConfirmationButton("J'ai changé d'avis", null)
                    );
                }, 
                autoCloseOnClick : false
            ), 
            //Choix 2
            new UIConfirmationMenu.ConfirmationButton("Non", null));
    }

    // Méthode pour confirmer pour supprimer un fichier de sauvegarde
    private void OnConfirmDelete()
    {
        File.Delete(filePath);
        PopulateDetails(SaveAndLoadMenu.Instance.menuFunction);
    }

    // Méthode pour charger un fichier de sauvegarde
    public void Load()
    {
        VNGameSave file = VNGameSave.Load(filePath, false);
        SaveAndLoadMenu.Instance.Close(closeAllMenus: true);
        
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == MainMenu.MAIN_MENU_SCENE)
        {
            MainMenu.instance.LoadGame(file);
        }
        else
        {
            file.Activate();

            VariableStore.TryGetValue("VN.mainCharName", out object test);
            InputPanel.Name = test.ToString();
            VariableStore.TryGetValue("VN.mainCharName2", out object test2);
            InputPanel.Name2 = test2.ToString();
        }
    }

    // Méthode pour sauvegarder l'état actuel dans le fichier de sauvegarde
    public void Save()
    {
        VariableStore.TrySetValue("VN.mainCharName", InputPanel.Name);
        VariableStore.TrySetValue("VN.mainCharName2", InputPanel.Name2);
        VariableStore.PrintAllVariables();

        if(HistoryManager.instance.isViewingHistory)
        {
            UIConfirmationMenu.instance.Show("Vous ne pouvez pas enregistrer pendant l’affichage de l’historique.", new UIConfirmationMenu.ConfirmationButton("Okay", null));
            return;
        }

        var activeSave = VNGameSave.activeFile;
        activeSave.slotNumber = fileNumber;

        activeSave.Save();

        PopulateDetailsFromFile(SaveAndLoadMenu.Instance.menuFunction, activeSave);
    }

}
