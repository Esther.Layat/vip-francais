
using UnityEngine;

public class MenuPage : MonoBehaviour
{
    public enum PageType { SaveAndLoad, Config, Help}
    public PageType pageType;

    public const string OPEN = "Open";
    public const string CLOSE = "Close";
    public Animator anim;

    // Méthode virtuelle pour ouvrir la page de menu
    public virtual void Open()
    {
        anim.SetTrigger(OPEN);
    }

    // Méthode virtuelle pour fermer la page de menu
    public virtual void Close(bool closeAllMenus = false)
    {
        anim.SetTrigger(CLOSE);

        if(closeAllMenus)
            VNMenuManager.instance.CloseRoot();
    }

}
