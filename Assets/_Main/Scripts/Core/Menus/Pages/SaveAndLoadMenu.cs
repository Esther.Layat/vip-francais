
using UnityEngine;
using VISUALNOVEL;

public class SaveAndLoadMenu : MenuPage
{
    public static SaveAndLoadMenu Instance { get; private set;}
    public const int MAX_FILES = 30;

    private int currentPage = 1;
    private bool loadedFilesForFirstTime = false;

    public enum MenuFunction {save, load}
    public MenuFunction menuFunction = MenuFunction.save;

    public SaveLoadSlot[] saveSlots;
    public int slotsPerPage => saveSlots.Length;
    public Texture emptyFileImage;

    private void Awake() 
    {
        Instance = this;
    }

    // Méthode appelée pour ouvrir la page de menu
    public override void Open()
    {
        base.Open();

        if(!loadedFilesForFirstTime)
            PopulateSaveSlotsForPage(currentPage);
    }

    // Méthode pour peupler les slots de sauvegarde/chargement pour une page donnée
    public void PopulateSaveSlotsForPage(int pageNumber)
    {
        currentPage = pageNumber;
        int startingFile = ((currentPage - 1) * slotsPerPage) + 1;
        int endingFile = startingFile + slotsPerPage -1;

        for (int i = 0; i < slotsPerPage; i++)
        {
            int fileNum = startingFile + i;
            SaveLoadSlot slot = saveSlots[i];

            if(fileNum <= MAX_FILES)
            {
                slot.root.SetActive(true);
                string filePath = $"{FilePaths.gameSaves}{fileNum}{VNGameSave.FILE_TYPE}";
                slot.fileNumber = fileNum;
                slot.filePath = filePath;
                slot.PopulateDetails(menuFunction);
            }
            else
            {
                slot.root.SetActive(false);
            }
        }
    }
}
