using System.Collections.Generic;
using System.IO;
using System.Linq;
using DIALOGUE;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ConfigMenu : MenuPage
{
    public static ConfigMenu instance { get; private set; }
    [SerializeField] private GameObject[] panels;
    private GameObject activePanel;

    public UI_ITEMS ui;

    private VN_Configuration config => VN_Configuration.activeConfig;

    private void Awake() 
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < panels.Length; i++)
        {
            panels[i].SetActive(i == 0);
        }

        activePanel = panels[0];

        SetAvailableResolution();

        LoadConfig();
    }

    // Charge la configuration depuis un fichier
    private void LoadConfig()
    {
        if(File.Exists(VN_Configuration.filePath))
            VN_Configuration.activeConfig = FileManager.Load<VN_Configuration>(VN_Configuration.filePath, encrypt : VN_Configuration.ENCRYPT);
        else 
            VN_Configuration.activeConfig = new VN_Configuration();
        
        VN_Configuration.activeConfig.Load();
    }

    // Sauvegarde la configuration lors de la fermeture de l'application
    private void OnApplicationQuit()
    {
        VN_Configuration.activeConfig.Save();
        VN_Configuration.activeConfig = null;
    }

    // Ouvre un panneau spécifique dans le menu de configuration
    public void OpenPanel(string panelName)
    {
        GameObject panel = panels.First(p => p.name.ToLower() == panelName.ToLower());

        if(panel == null)
        {
            Debug.LogWarning($"Did not find panel called '{panelName}' in config menu.");
            return;
        }

        if(activePanel != null && activePanel != panel)
            activePanel.SetActive(false);

        panel.SetActive(true);
        activePanel = panel;
    }

    // Initialise les résolutions d'affichage disponibles
    private void SetAvailableResolution()
    {
        Resolution[] resolutions = Screen.resolutions;
        Debug.Log($"Res: {Screen.currentResolution.width} x {Screen.currentResolution.height}");
        
        List<string> options = new List<string>();

        for(int i = resolutions.Length - 1; i >= 0; i--)
        {
            options.Add($"{resolutions[i].width}x{resolutions[i].height}");        
        }

        ui.resolutions.ClearOptions();
        ui.resolutions.AddOptions(options);
    }

    [System.Serializable]
    public class UI_ITEMS
    {
        private static Color button_selectedColor = new Color(1, 0.35f, 0, 1);
        private static Color button_unselectedColor = new Color(1, 1, 1, 1);
        private static Color text_selectedColor = new Color(1, 1, 0, 1);
        private static Color text_unselectedColor = new Color(0.25f, 0.25f, 0.25f, 1);
        public static Color musicOnColor = new Color(1, 0.65f, 0, 1);
        public static Color musicOffColor = new Color(0.5f, 0.5f, 0.5f, 1);


        [Header("General")]
        public Button fullscreen;
        public Button windowed;
        public TMP_Dropdown resolutions;
        public Button skippingContinue, skippingStop;
        public Slider architectSpeed, autoReaderSpeed;


        [Header("Audio")]
        public Slider musicVolume;
        public Image musicFill;
        public Slider sfxVolume;
        public Image sfxFill;
        public Slider voicesVolume;
        public Image voicesFill;
        public Sprite mutedSymbol;
        public Sprite unmutedSymbole;
        public Image musicMute;
        public Image sfxMute;
        public Image voicesMute;

        // Définit les couleurs des boutons sélectionnés et non sélectionnés
        public void SetButtonColors(Button A, Button B, bool selectedA)
        {
            A.GetComponent<Image>().color = selectedA ? button_selectedColor : button_unselectedColor;
            B.GetComponent<Image>().color = !selectedA ? button_selectedColor : button_unselectedColor;

            A.GetComponentInChildren<TextMeshProUGUI>().color = selectedA ? text_selectedColor : text_unselectedColor;
            B.GetComponentInChildren<TextMeshProUGUI>().color = !selectedA ? text_selectedColor : text_unselectedColor;
        }

    }

    // Méthodes appelables par l'interface utilisateur

    // Définit le mode d'affichage plein écran ou fenêtré
    public void SetDisplayToFullScreen(bool fullscreen)
    {
        Screen.fullScreen = fullscreen;
        ui.SetButtonColors(ui.fullscreen, ui.windowed, fullscreen);
    }

    // Définit la résolution d'affichage
    public void SetDisplayResolution()
    {
        string resolution = ui.resolutions.captionText.text;
        string[] values = resolution.Split('x');

        if(int.TryParse(values[0], out int width) && int.TryParse(values[1], out int height))
        {
            Screen.SetResolution(width, height, Screen.fullScreen);
            config.display_resolution = resolution;
        }
        else 
            Debug. LogError($"Parsing error for screen resolution! [{resolution}] could not be parsed into WIDTHxHEIGHT");
    }

    // Définit si la lecture continue après un choix
    public void SetContinueSkippingAfterChoice(bool continueSkipping)
    {
        config.continueSkippingAfterChoice = continueSkipping;
        ui.SetButtonColors(ui.skippingContinue, ui.skippingStop, continueSkipping);
    }

    // Définit la vitesse de texte pour l'architecte
    public void SetArchitectSpeed()
    {
        config.dialogueTextSpeed = ui.architectSpeed.value;

        if(DialogueSystem.instance != null)
            DialogueSystem.instance.conversationManager.architect.speed = config.dialogueTextSpeed;
    }

    // Définit la vitesse de lecture automatique
    public void SetAutoReaderSpeed()
    {
        config.dialogueAutoReadSpeed = ui.autoReaderSpeed.value;

        if(DialogueSystem.instance == null)
            return;

        AutoReader autoReader = DialogueSystem.instance.autoReader;
        if(autoReader != null)
            autoReader.speed = config.dialogueAutoReadSpeed;
    }

    // Définit le volume de la musique
    public void SetMusicVolume()
    {
        config.musicVolume = ui.musicVolume.value;
        AudioManager.instance.SetMusicVolume(config.musicVolume, config.musicMute);

        ui.musicFill.color = config.musicMute ? UI_ITEMS.musicOffColor : UI_ITEMS.musicOnColor;
    }

    // Définit le volume des effets sonores
    public void SetSFXVolume()
    {
        config.sfxVolume = ui.sfxVolume.value;
        AudioManager.instance.SetSFXVolume(config.sfxVolume, config.sfxMute);

        ui.sfxFill.color = config.sfxMute ? UI_ITEMS.musicOffColor : UI_ITEMS.musicOnColor;
    }

    // Définit le volume des voix
    public void SetVoicesVolume()
    {
        config.voicesVolume = ui.voicesVolume.value;
        AudioManager.instance.SetVoicesVolume(config.voicesVolume, config.voicesMute);

        ui.voicesFill.color = config.voicesMute ? UI_ITEMS.musicOffColor : UI_ITEMS.musicOnColor;
    }

    // Active ou désactive le mute de la musique
    public void SetMusicMute()
    {
        config.musicMute = !config.musicMute;
        ui.musicVolume.fillRect.GetComponent<Image>().color = config.musicMute ? UI_ITEMS.musicOffColor : UI_ITEMS.musicOnColor;
        ui.musicMute.sprite = config.musicMute ? ui.mutedSymbol : ui.unmutedSymbole;

        AudioManager.instance.SetMusicVolume(config.musicVolume, config.musicMute);
    }

    // Active ou désactive le mute des effets sonores
    public void SetSFXMute()
    {
        config.sfxMute = !config.sfxMute;
        ui.sfxVolume.fillRect.GetComponent<Image>().color = config.sfxMute ? UI_ITEMS.musicOffColor : UI_ITEMS.musicOnColor;
        ui.sfxMute.sprite = config.sfxMute ? ui.mutedSymbol : ui.unmutedSymbole;

        AudioManager.instance.SetSFXVolume(config.sfxVolume, config.sfxMute);
    }

    // Active ou désactive le mute des voix
    public void SetVoicesMute()
    {
        config.voicesMute = !config.voicesMute;
        ui.voicesVolume.fillRect.GetComponent<Image>().color = config.voicesMute ? UI_ITEMS.musicOffColor : UI_ITEMS.musicOnColor;
        ui.voicesMute.sprite = config.voicesMute ? ui.mutedSymbol : ui.unmutedSymbole;

        AudioManager.instance.SetVoicesVolume(config.voicesVolume, config.voicesMute);
    }
}
