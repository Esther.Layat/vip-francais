
using System.Linq;
using UnityEngine;

public class VNMenuManager : MonoBehaviour
{
    public static VNMenuManager instance;

    private MenuPage activePage = null;
    private bool isOpen = false;

    [SerializeField] private CanvasGroup root;
    [SerializeField] private MenuPage[] pages;

    private CanvasGroupController rootGC;

    private UIConfirmationMenu uiChoiceMenu => UIConfirmationMenu.instance;

    private void Awake() 
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        rootGC = new CanvasGroupController(this, root);
    }

    // Méthode pour obtenir une page de menu par son type
    private MenuPage GetPage(MenuPage.PageType pageType)
    {
        return pages.FirstOrDefault(page => page.pageType == pageType);
    }

    // Méthode pour ouvrir la page de sauvegarde
    public void OpenSavePage()
    {
        var page = GetPage(MenuPage.PageType.SaveAndLoad);
        var slm = page.anim.GetComponentInParent<SaveAndLoadMenu>();
        slm.menuFunction = SaveAndLoadMenu.MenuFunction.save;
        OpenPage(page);
    }

    // Méthode pour ouvrir la page de sauvegarde
    public void OpenLoadPage()
    {
        var page = GetPage(MenuPage.PageType.SaveAndLoad);
        var slm = page.anim.GetComponentInParent<SaveAndLoadMenu>();
        slm.menuFunction = SaveAndLoadMenu.MenuFunction.load;
        OpenPage(page);
    }

    // Méthode pour ouvrir la page de configuration
    public void OpenConfigPage()
    {
        var page = GetPage(MenuPage.PageType.Config);
        OpenPage(page);
    }

    // Méthode pour ouvrir la page d'aide
    public void OpenHelpPage()
    {
        var page = GetPage(MenuPage.PageType.Help);
        OpenPage(page);
    }

    // Méthode pour ouvrir une page spécifique
    private void OpenPage (MenuPage page)
    {
        if(page == null)
            return;
        
        if(activePage != null && activePage != page)
            activePage.Close();
        
        page.Open();
        activePage = page;

        if(!isOpen)
            OpenRoot();

    }

    // Méthode pour ouvrir le CanvasGroup racine
    public void OpenRoot()
    {
        rootGC.Show();
        rootGC.SetInteractableState(true);
        isOpen = true;
    }

    // Méthode pour fermer le CanvasGroup racine
    public void CloseRoot()
    {
        rootGC.Hide();
        rootGC.SetInteractableState(false);
        isOpen = false;
    }

    // Méthode appelée lors du clic sur le bouton "Home"
    public void Click_Home()
    {
        uiChoiceMenu.Show(
            // Titre
            "Revenir au menu principal ?", 
            //Choix 1
            new UIConfirmationMenu.ConfirmationButton("Oui", Home), 
            //Choix 2
            new UIConfirmationMenu.ConfirmationButton("Non", null));
    }

    public void Home()
    {
        VN_Configuration.activeConfig.Save();

        UnityEngine.SceneManagement.SceneManager.LoadScene(MainMenu.MAIN_MENU_SCENE);
    }

    // Méthode appelée lors du clic sur le bouton "Quitter"
    public void Click_Quit()
    {
        uiChoiceMenu.Show("Voulez vous quitter le jeu ?", new UIConfirmationMenu.ConfirmationButton("Oui", () => Application.Quit()), new UIConfirmationMenu.ConfirmationButton("Non", null));
    }

}
