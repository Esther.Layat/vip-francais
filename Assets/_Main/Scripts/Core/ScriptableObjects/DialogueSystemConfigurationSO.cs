using CHARACTERS;
using TMPro;
using UnityEngine;

namespace DIALOGUE
{
    // Crée un nouvel élément de menu dans l'éditeur Unity pour créer une configuration du système de dialogue
    [CreateAssetMenu(fileName ="Dialogue System Configuration", menuName ="Dialogue System/Dialogue Configuration Asset")]
    public class DialogueSystemConfigurationSO : ScriptableObject
    {
        public const float DEFAULT_FONTSIZE_DIALOGUE = 40;
        public const float DEFAULT_FONTSIZE_NAME = 45;
        // Asset de configuration des personnages utilisé par le système de dialogue
        public CharacterConfigSO characterConfigurationAsset;

        // Couleur de texte par défaut pour le dialogue
        public Color defaultTextColor = Color.white;
        
        // Police de caractères par défaut pour le dialogue
        public TMP_FontAsset defaultFont;
        public float dialogueFontScale = 1f;
        public float defaultDialogueFontSize = DEFAULT_FONTSIZE_DIALOGUE;
        public float defaultNameFontSize = DEFAULT_FONTSIZE_NAME;
    }
}
