
using UnityEngine;

namespace CHARACTERS
{
    // Crée un nouvel élément de menu dans l'éditeur Unity pour créer des configurations de personnages
    [CreateAssetMenu(fileName = "Character Configuration Asset",menuName = "Dialogue System/Character Configuration Asset")]
    public class CharacterConfigSO : ScriptableObject
    {
        // Tableau de données de configuration pour les personnages
        public CharacterConfigData[] characters;

        // Méthode pour obtenir la configuration d'un personnage par son nom
        public CharacterConfigData GetConfig(string characterName, bool safe = true){
            characterName = characterName.ToLower();
            for (int i = 0; i < characters.Length; i++)
            {
                CharacterConfigData data = characters[i];

                if(string.Equals(characterName, data.name.ToLower()) || string.Equals(characterName, data.alias.ToLower())){
                    return safe ? data.Copy() : data;
                }
            }

            return CharacterConfigData.Default;
        }
    }
}