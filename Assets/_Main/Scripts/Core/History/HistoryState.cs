
using System.Collections.Generic;


namespace History
{
    [System.Serializable]
    public class HistoryState
    {
        public DialogueData dialogue;
        public List<CharacterData> characters;
        public List<AudioTrackData> audio;
        public List<AudioSFXData> sfx;
        public List<GraphicData> graphics;

        // Méthode statique pour capturer l'état actuel du jeu
        public static HistoryState Capture()
        {
            HistoryState state = new HistoryState();
            state.dialogue =  DialogueData.Capture();
            state.characters = CharacterData.Capture();
            state.audio =  AudioTrackData.Capture();
            state.sfx = AudioSFXData.Capture();
            state.graphics = GraphicData.Capture();

            return state;
        }

        // Méthode pour charger l'état sauvegardé
        public void Load()
        {
            DialogueData.Apply(dialogue);
            CharacterData.Apply(characters);
            AudioTrackData.Apply(audio);
            AudioSFXData.Apply(sfx);
            GraphicData.Apply(graphics);
        }
    }
}

