
using System.Collections.Generic;
using DIALOGUE;

using UnityEngine;

namespace History
{
    // RequireComponent garantit qu'un composant HistoryNavigation est également attaché au même GameObject
    [RequireComponent(typeof(HistoryLogManager))]
    [RequireComponent(typeof(HistoryNavigation))]
    public class HistoryManager : MonoBehaviour
    {
        public const int HISTORY_CACHE_LIMIT = 100;
        public static HistoryManager instance {get; private set;}
        public List<HistoryState> history = new List<HistoryState>();

        
        private HistoryNavigation navigation;
        public bool isViewingHistory => navigation.isViewingHistory;
        public HistoryLogManager logManager {get; private set;}

        private void Awake() 
        {
            instance = this;
            navigation = GetComponent<HistoryNavigation>();
            logManager  = GetComponent<HistoryLogManager>();
        }
        // Start is called before the first frame update
        void Start()
        {
            DialogueSystem.instance.onClear += LogCurrentState;
        }

        // Enregistrer l'état actuel du système et maintenir la limite du cache de l'historique
        public void LogCurrentState()
        {
            HistoryState state = HistoryState.Capture();
            history.Add(state);
            logManager.AddLog(state);

            if(history.Count > HISTORY_CACHE_LIMIT)
                history.RemoveAt(0);
        }

        // Charger un état spécifique
        public void LoadState(HistoryState state)
        {
            state.Load();
            
        }

        // Naviguer vers l'avant dans l'historique
        public void GoForward() => navigation.GoForward();
        
        // Naviguer vers l'arrière dans l'historique
        public void GoBack() => navigation.GoBack();

    }
}

