using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class FilePaths
{
    private const string HOME_DIRECTORY_SYMBOL = "~/";

    // Définit un chemin racine pour les fichiers de données du jeu
    public static readonly string root = $"{runtimePath}/gameData/";

    // Runtime Paths
    public static readonly string gameSaves = $"{runtimePath}Save Files/";

    // Ressources Paths
    public static readonly string resources_font = "Fonts/";
    public static readonly string resources_graphics = "Graphics/";
    public static readonly string resources_backgroundImages = $"{resources_graphics}BG Images/";
    public static readonly string resources_backgroundVideos = $"{resources_graphics}BG Videos/";
    public static readonly string resources_blendTextures = $"{resources_graphics}Transition Effects/";
    public static readonly string resources_gallery = $"{resources_graphics}Gallery/";

    public static readonly string resources_audio = "Audio/";
    public static readonly string resources_sfx = $"{resources_audio}SFX/";
    public static readonly string resources_voices = $"{resources_audio}Voices/";
    public static readonly string resources_music = $"{resources_audio}Music/";
    public static readonly string resources_ambience = $"{resources_audio}Ambience/";

    public static readonly string resources_dialogueFiles = "Dialogue Files/";

    /// <summary>
    /// Retourne le chemin d’accès à la ressource en utilisant le chemin par défaut ou la racine du dossier de ressources si    /// </summary>
    /// ‹param name-"defaultPath"></param>
    /// ‹param name="resourceName"></param>
    /// <returns></returns>
    
    public static string GetPathToResource(string defaultPath, string resourceName)
    {
        if (resourceName.StartsWith(HOME_DIRECTORY_SYMBOL))
            return resourceName.Substring(HOME_DIRECTORY_SYMBOL.Length);

        return defaultPath + resourceName;
    }

    // Chemin d'accès au dossier d'exécution du jeu
    public static string runtimePath
    {
        get
        {
            #if UNITY_EDITOR
                return "Assets/appdata/";
            #else 
                return Application.persistentDataPath + "/appdata/";
            #endif

        }
    }
}
