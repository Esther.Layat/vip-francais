
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class FileManager
{
    private const string KEY = "SECRETKEY";

    // Méthode pour lire un fichier texte à partir du système de fichiers
    public static List<string> ReadTextFile(string filePath, bool includeBlankLines = true)
    {
        if (!filePath.StartsWith('/'))
            filePath = FilePaths.root + filePath;

        List<string> lines = new List<string>();
        try{
            using (StreamReader sr = new StreamReader(filePath)){
                while(!sr.EndOfStream){
                    string line = sr.ReadLine();
                    if (includeBlankLines || !string.IsNullOrWhiteSpace(line))
                        lines.Add(line);
                }
            }
        }
        catch (FileNotFoundException ex){
            Debug.LogError($"File not found: '{ex.FileName}'");
        }

        return lines;
    }

    // Méthode pour lire un fichier texte à partir des ressources de l'application
    public static List<string> ReadTextAsset(string filePath, bool includeBlankLines = true)
    {
        TextAsset asset = Resources.Load<TextAsset>(filePath);
        if(asset == null){
            Debug.LogError($"Asset not found: '{filePath}'");
            return null;
        }

        return ReadTextAsset(asset, includeBlankLines);
    } 

    // Méthode pour lire un TextAsset et retourner son contenu sous forme de liste de chaînes
    public static List<string> ReadTextAsset(TextAsset asset, bool includeBlankLines = true)
    {
        List<string> lines = new List<string>();
        using (StringReader sr = new StringReader(asset.text)){
            while(sr.Peek() > -1){
                string line = sr.ReadLine();
                if (includeBlankLines || !string.IsNullOrWhiteSpace(line))
                    lines.Add(line);
            }
        }

        return lines;
    } 

    // Méthode pour essayer de créer un répertoire à partir du chemin spécifié
    public static bool TryCreateDirectoryFromPath(string path)
    {
        if(Directory.Exists(path) || File.Exists(path))
            return true;

        if(path.Contains("."))
        {
            path = Path.GetDirectoryName(path);
            if(Directory.Exists(path))
                return true;
        }
            
        if(path == string.Empty)
            return false;
        
        try
        {
            Directory.CreateDirectory(path);
            return true;   
        }
        catch (System.Exception e)
        {
            Debug.LogError($"Could not create directory! {e}");
            return false;
        }
    }

    // Méthode pour enregistrer des données JSON dans un fichier
    public static void Save(string filePath, string JSONData, bool encrypt = false)
    {
        if(!TryCreateDirectoryFromPath(filePath))
        {
            Debug.LogError($"FAILED TO SAVE FILET '{filePath}' Please see the console for error details.");
            return;
        }

        if(encrypt)
        {
            byte[] dataBytes = Encoding.UTF8.GetBytes(JSONData);
            byte[] keyBytes = Encoding.UTF8.GetBytes(KEY);
            byte[] encryptedBytes = XOR(dataBytes, keyBytes);

            File.WriteAllBytes(filePath, encryptedBytes);
        }
        else 
        {
            StreamWriter sw = new StreamWriter(filePath);
            sw.Write(JSONData);
            sw.Close();
        }
        Debug.Log($"Saved data to file '{filePath}'");
    }

    // Méthode pour charger des données JSON à partir d'un fichier
    public static T Load<T>(string filePath, bool encrypt = false)
    {
        if(File.Exists(filePath))
        {
            if(encrypt)
            {
                byte[] encryptedBytes = File.ReadAllBytes(filePath);
                byte[] keyBytes = Encoding.UTF8.GetBytes(KEY);

                byte[] decryptedBytes = XOR(encryptedBytes, keyBytes);

                string decryptedString = Encoding.UTF8.GetString(decryptedBytes);

                return JsonUtility.FromJson<T>(decryptedString);
            }
            else
            {
                string JSONData = File.ReadAllLines(filePath)[0];
                return JsonUtility.FromJson<T>(JSONData);
            }
        }
        else
        {
            Debug.LogError($"Error - File does not exist! '{filePath}'");
            return default(T);
        }
    }

    // Méthode pour effectuer une opération XOR sur les données d'entrée et la clé
    private static byte[] XOR (byte[] input, byte[] key)
    {
        byte[] output = new byte[input.Length];

        for(int i = 0; i < input.Length; i++)
        {
            output[i] = (byte)(input[i] ^ key[i % key.Length]);
        }

        return output;
    }
}
