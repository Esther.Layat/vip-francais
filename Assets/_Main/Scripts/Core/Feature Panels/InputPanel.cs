
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using VISUALNOVEL;

public class InputPanel : MonoBehaviour
{
    // Singleton instance pour accéder à l'InputPanel globalement
    public static InputPanel instance { get; private set;} = null;

    // Références aux composants UI configurées depuis l'éditeur
    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private TMP_Text nameCharacter;
    [SerializeField] private Button acceptButton;
    [SerializeField] private TMP_InputField inputField;

    // Contrôleur pour gérer l'affichage et l'interaction avec le CanvasGroup
    private CanvasGroupController cg;

    // Propriétés pour obtenir la dernière entrée de l'utilisateur et pour vérifier si le panneau attend une entrée de l'utilisateur
    public string lastInput {get; private set;} = string.Empty;
    public static string Name;
    public static string Name2;
    private int index = 0;
    public bool isWaitingOnUserInput {get; private set;}


    private void Awake() {
        instance = this;
    }
 
    // Start is called before the first frame update
    void Start()
    {
        cg = new CanvasGroupController(this, canvasGroup);

        cg.alpha = 0;
        cg.SetInteractableState(active: false);
        acceptButton.gameObject.SetActive(false);

        // Ajouter des écouteurs d'événements pour les changements dans le champ de saisie et pour le clic sur le bouton d'acceptation
        inputField.onValueChanged.AddListener(OnInputChanged);
        acceptButton.onClick.AddListener(OnAcceptInput);
    }

    // Méthode pour afficher le panneau avec le nom du personnage spécifié
    public void Show(string name)
    {
        nameCharacter.text = name;
        inputField.text = string.Empty;
        cg.Show();
        cg.SetInteractableState(active: true);
        isWaitingOnUserInput = true;
    }

    // Méthode pour masquer le panneau
    public void Hide(){
        cg.Hide();
        cg.SetInteractableState(active: false);
        isWaitingOnUserInput = false;
    }

    // Méthode appelée lorsque l'utilisateur clique sur le bouton d'acceptation
    public void OnAcceptInput()
    {
        if(inputField.text == string.Empty)
            return;
        
        lastInput = inputField.text;

        if(index == 0)
        {
            VNGameSave.activeFile.playerName = lastInput;
            Name = lastInput;
        }
        else if(index == 1)
        {
            VNGameSave.activeFile.playerName2 = lastInput;
            Name2 = lastInput;
        }
        index++;
        Hide();
    }

    // Méthode appelée lorsqu'il y a un changement dans le champ de saisie
    public void OnInputChanged(string value){
        acceptButton.gameObject.SetActive(HasValidText());
    }

    // Méthode pour vérifier si le texte dans le champ de saisie est valide
    private bool HasValidText()
    {
        return inputField.text != string.Empty;
    }

}
