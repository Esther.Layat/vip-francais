using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChoicePanel : MonoBehaviour
{
    public static ChoicePanel instance { get; private set; }

    // Constantes pour les dimensions des boutons
    private const float BUTTON_MIN_WIDTH = 80;
    private const float BUTTON_MAX_WIDTH = 1000;
    private const float BUTTON_WIDTH_PADDING = 25;

    private const float BUTTON_HEIGHT_PER_LINE = 80f;
    private const float BUTTON_HEIGHT_PADDING = 20f;

    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private TextMeshProUGUI titleText;
    [SerializeField] private GameObject choiceButtonPrefab;
    [SerializeField] private VerticalLayoutGroup buttonLayoutGroup;

    private CanvasGroupController cg = null;
    private List<ChoiceButton> buttons = new List<ChoiceButton>();
    public ChoicePanelDecision lastDecision { get; private set; } = null;

    public bool isWaitingOnUserChoice { get; private set; } = false;

    private void Awake() {
        instance = this;
        cg = new CanvasGroupController(this, canvasGroup);

        cg.alpha = 0;
        cg.SetInteractableState(active : true);
    }

    // Start is called before the first frame update
    void Start()
    {
        cg = new CanvasGroupController(this, canvasGroup);

        if (isWaitingOnUserChoice == false)
        {
            cg.alpha = 0;
            cg.SetInteractableState(active : false);
        }
        
    }

    // Affiche le panneau de choix avec la question et les options fournies
    public void Show(string question, string[] choices)
    {
        lastDecision = new ChoicePanelDecision(question,choices);
        isWaitingOnUserChoice = true;

        titleText.text = question;
        StartCoroutine(GenerateChoice(choices));
        cg.Show();
        cg.SetInteractableState(active: true);

    }

    // Coroutine pour générer dynamiquement les boutons de choix
    private IEnumerator GenerateChoice(string[] choices)
    {
        float maxWidht = 0;

        for (int i = 0; i < choices.Length; i++)
        {
            ChoiceButton choiceButton;
            if(i< buttons.Count)
            {
                choiceButton = buttons[i];
            }
            else
            {
                GameObject newButtonObject = Instantiate(choiceButtonPrefab, buttonLayoutGroup.transform);
                newButtonObject.SetActive(true);

                Button newButton = newButtonObject.GetComponent<Button>();
                TextMeshProUGUI newTitle = newButton.GetComponentInChildren<TextMeshProUGUI>();
                LayoutElement newLayout = newButton.GetComponent<LayoutElement>();

                choiceButton = new ChoiceButton {button = newButton, layout = newLayout, title = newTitle};

                buttons.Add(choiceButton);
            }

            choiceButton.button.onClick.RemoveAllListeners();
            int buttonIndex = i;
            choiceButton.button.onClick.AddListener(() => AcceptAnswer(buttonIndex));
            choiceButton.title.text = choices[i];

            float buttonWidth = Mathf.Clamp(BUTTON_WIDTH_PADDING + choiceButton.title.preferredWidth, BUTTON_MIN_WIDTH, BUTTON_MAX_WIDTH);
            maxWidht = Mathf.Max(maxWidht, buttonWidth);
        }

        foreach (var button in buttons)
        {
            button.layout.preferredWidth = maxWidht;
        }

        for (int i = 0; i < buttons.Count; i++)
        {
            bool show = i < choices.Length ? true : false;
            buttons[i].button.gameObject.SetActive(show);
        }

        yield return new WaitForEndOfFrame();

        foreach(var button in buttons)
        {
            int lines = button.title.textInfo.lineCount;
            Debug.Log(lines);
            button.layout.preferredHeight = BUTTON_HEIGHT_PADDING + (BUTTON_HEIGHT_PER_LINE * lines);
        }
    }

    // Masque le panneau de choix
    public void Hide()
    {
        cg.Hide();
        cg.SetInteractableState(active : false);
    }

    // Accepte la réponse de l'utilisateur et met à jour la décision
    private void AcceptAnswer(int index)
    {
        if(index < 0 || index > lastDecision.choices.Length -1)
            return;
        
        lastDecision.answerIndex = index;
        isWaitingOnUserChoice = false;
        Hide();
    }

    // Classe pour stocker la décision de l'utilisateur
    public class ChoicePanelDecision
    {
        public string question = string.Empty;
        public int answerIndex = -1;
        public string[] choices = new string[0];

        public ChoicePanelDecision(string question, string[] choices)
        {
            this.question = question;
            this.choices = choices;
            answerIndex = -1;
        }
    }

    // Structure pour les informations des boutons de choix
    private struct ChoiceButton
    {
        public Button button;
        public TextMeshProUGUI title;
        public LayoutElement layout;
    }

}
