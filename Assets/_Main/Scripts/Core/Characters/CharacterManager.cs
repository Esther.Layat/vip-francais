using DIALOGUE;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CHARACTERS
{
    // Gestionnaire des personnages
    public class CharacterManager : MonoBehaviour
    {
        // Instance statique du gestionnaire de personnages
        public static CharacterManager instance { get; private set; }
        // Tableau de tous les personnages
        public Character[] allCharacters => characters.Values.ToArray();

        // Dictionnaire des personnages avec leur nom comme clé
        private Dictionary<string, Character> characters = new Dictionary<string, Character>();
        // Configuration des personnages
        private CharacterConfigSO config => DialogueSystem.instance.config.characterConfigurationAsset;

        // Identificateur utilisé pour séparer le nom du personnage et son alias
        public const string CHARACTER_CASTING_ID = " as ";
        // Identificateur utilisé pour formater les chemins des ressources des personnages
        private const string CHARACTER_NAME_ID = "<charname>";

        // Formats pour les chemins des ressources des personnages
        public string characterRootPathFormat => $"Characters/{CHARACTER_NAME_ID}";
        public string characterPrefabNameFormat => $"Character - [{CHARACTER_NAME_ID}]";
        public string characterPrefabPathFormat => $"{characterRootPathFormat}/{characterPrefabNameFormat}";

        // Panneaux pour afficher les personnages
        [SerializeField] private RectTransform _characterpanel = null;
        [SerializeField] private RectTransform _characterpanel_live2D = null;
        [SerializeField] private RectTransform _characterpanel_model3D = null;

        // Accesseurs pour les panneaux des personnages
        public RectTransform characterPanel => _characterpanel;
        public RectTransform characterPanelLive2D => _characterpanel_live2D;
        public RectTransform characterPanelModel3D => _characterpanel_model3D;


        public void Awake()
        {
            instance = this;
        }

        // Obtenir la configuration d'un personnage par son nom
        public CharacterConfigData GetCharacterConfig(string characterName, bool getOriginal = false)
        {
            if (!getOriginal)
            {
                Character character = GetCharacter(characterName);
                if (character != null)
                {
                    return character.config;
                }
            }
            return config.GetConfig(characterName);
        }

        // Obtenir un personnage par son nom, le créer s'il n'existe pas
        public Character GetCharacter(string characterName, bool createIfDoesNotExist = false)
        {
            if (characters.ContainsKey(characterName.ToLower()))
                return characters[characterName.ToLower()];
            else if (createIfDoesNotExist)
                return CreateCharacter(characterName);

            return null;
        }

        public bool HasCharacter(string characterName) => characters.ContainsKey(characterName.ToLower());

        // Créer un personnage par son nom
        public Character CreateCharacter(string characterName, bool revealAfterCreation = false)
        {
            if (characters.ContainsKey(characterName.ToLower()))
            {
                Debug.LogWarning($"A Character called '{characterName}' already exists. Did not create the character.");
                return null;
            }

            CHARACTER_INFO info = GetCharacterInfo(characterName);

            Character character = CreateCharacterFromInfo(info);

            if(info.castingName != info.name )
                character.castingName = info.castingName;

            if(!characters.ContainsKey(info.name.ToLower()))
                characters.Add(info.name.ToLower(), character);
            
            else
                character.Show();

            if (revealAfterCreation)
                character.Show();

            return character;
        }

        // Obtenir les informations d'un personnage par son nom
        private CHARACTER_INFO GetCharacterInfo(string characterName)
        {
            CHARACTER_INFO result = new CHARACTER_INFO();

            string[] nameData = characterName.Split(CHARACTER_CASTING_ID, System.StringSplitOptions.RemoveEmptyEntries);
            result.name = nameData[0];
            result.castingName = nameData.Length > 1 ? nameData[1] : result.name;

            result.config = config.GetConfig(result.castingName);

            result.prefab = GetPrefabForCharacter(result.castingName);

            result.rootCharacterFolder = FormatCharacterPath(characterRootPathFormat, result.castingName);

            return result;
        }

        // Obtenir le préfab d'un personnage par son nom
        private GameObject GetPrefabForCharacter(string characterName)
        {
            string prefabPath = FormatCharacterPath(characterPrefabPathFormat, characterName);
            return Resources.Load<GameObject>(prefabPath);
        }

        // Formater le chemin d'un personnage avec son nom
        public string FormatCharacterPath(string path, string characterName) => path.Replace(CHARACTER_NAME_ID, characterName);

        // Créer un personnage à partir d'informations
        private Character CreateCharacterFromInfo(CHARACTER_INFO info)
        {
            CharacterConfigData config = info.config;

            if (config.characterType == Character.CharacterType.Text)
                return new Character_Text(info.name, config);

            if (config.characterType == Character.CharacterType.Sprite || config.characterType == Character.CharacterType.SpriteSheet)
                return new Character_Sprite(info.name, config, info.prefab, info.rootCharacterFolder);

            if (config.characterType == Character.CharacterType.Live2D)
                return new Character_Live2D(info.name, config, info.prefab, info.rootCharacterFolder);

            if (config.characterType == Character.CharacterType.Model3D)
                return new Character_Model3D(info.name, config, info.prefab, info.rootCharacterFolder);

            return null;

        }

        // Trier les personnages actifs
        public void SortCharacters()
        {
            List<Character> activeCharacters = characters.Values.Where(c => c.root.gameObject.activeInHierarchy && c.isVisible).ToList();
            List<Character> inactiveCharacters = characters.Values.Except(activeCharacters).ToList();

            activeCharacters.Sort((a, b) => a.priority.CompareTo(b.priority));
            activeCharacters.Concat(inactiveCharacters);

            SortCharacters(activeCharacters);
        }

        // Trier les personnages par une liste de noms
        public void SortCharacters(string[] characterNames)
        {
            List<Character> sortedCharacters = new List<Character>();

            sortedCharacters = characterNames
                .Select(name => GetCharacter(name))
                .Where(character => character != null)
                .ToList();

            List<Character> remainingCharacters = characters.Values
                .Except(sortedCharacters)
                .OrderBy(character => character.priority)
                .ToList();

            sortedCharacters.Reverse();

            int startingPriority = remainingCharacters.Count > 0 ? remainingCharacters.Max(c => c.priority) : 0;
            for (int i = 0; i < sortedCharacters.Count; i++)
            {
                Character character = sortedCharacters[i];
                character.SetPriority(startingPriority + i + 1, autoSortCharactersOnUI: false);
            }

            List<Character> allCharacters = sortedCharacters.Concat(sortedCharacters).ToList();
            SortCharacters(allCharacters);
        }


        private void SortCharacters(List<Character> charactersSortingOrder)
        {
            int i = 0;
            foreach (Character character in charactersSortingOrder)
            {
                Debug.Log($"{character.name} priority is {character.priority}");
                character.root.SetSiblingIndex(i++);
                character.OnSort(i);
            }
        }

        // Classe pour stocker les informations d'un personnage
        private class CHARACTER_INFO
        {
            public string name = "";
            public string castingName = "";

            public string rootCharacterFolder = "";

            public CharacterConfigData config = null;

            public GameObject prefab = null;
        }

    }

}