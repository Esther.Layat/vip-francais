using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace CHARACTERS
{
    public class Character_Sprite : Character
    {
        // Constantes pour le nom du parent des renderers, le nom par défaut de la spritesheet et le délimiteur des noms de sprite dans la spritesheet
        private const string SPRITE_RENDERERS_PARENT_NAME = "Renderers";
        private const string SPRITESHEET_DEFAULT_SHEETNAME = "Default";
        private const char SPRITESHEET_TEXT_SPRITE_DELIMITTER = '-';

        // Propriété pour accéder au CanvasGroup racine
        private CanvasGroup rootCG => root.GetComponent<CanvasGroup>();

        // Liste des couches de sprites du personnage
        public List<CharacterSpriteLayer> layers = new List<CharacterSpriteLayer>();

        // Répertoire des assets graphiques
        private string artAssetsDirectory = "";

        // Propriété pour vérifier si le personnage est visible
        public override bool isVisible
        {
            get { return isRevealing || rootCG.alpha == 1;}
            set { rootCG.alpha = value ? 1 : 0;}
        }

        // Constructeur prenant des paramètres supplémentaires spécifiques aux sprites
        public Character_Sprite(string name, CharacterConfigData config, GameObject prefab, string rootAssetsFolder) : base(name, config, prefab)
        {
            rootCG.alpha = ENABLE_ON_START ? 1 : 0;
            artAssetsDirectory = rootAssetsFolder + "/Images";
            GetLayer();

            Debug.Log($"Created Sprite Character : '{name}'");
        }

        // Méthode pour récupérer les couches de sprites du personnage
        private void GetLayer()
        {
            // Recherche du parent des renderers
            Transform rendererRoot = animator.transform.Find(SPRITE_RENDERERS_PARENT_NAME);
            if (rendererRoot == null)
            {
                return;
            }

            // Parcours des enfants pour récupérer les images
            for (int i = 0; i < rendererRoot.transform.childCount; i++)
            {
                Transform chidl = rendererRoot.transform.GetChild(i);
                Image rendererImage = chidl.GetComponentInChildren<Image>();
                if (rendererImage != null)
                {
                    // Création d'une nouvelle couche de sprite et ajout à la liste
                    CharacterSpriteLayer layer = new CharacterSpriteLayer(rendererImage, i);
                    layers.Add(layer);
                    // Renommage de l'objet enfant pour la visualisation dans l'éditeur
                    chidl.name = $"Layers: {i}";
                }

            }
        }

        // Méthode pour définir le sprite d'une couche spécifique
        public void SetSprite(Sprite sprite, int layer = 0)
        {
            layers[layer].SetSprite(sprite);
        }

        // Méthode pour obtenir un sprite à partir de son nom
        public Sprite GetSprite(string spriteName)
        {
            if(config.Sprites.Count > 0)
            {
                if(config.Sprites.TryGetValue(spriteName, out Sprite sprite))
                    return sprite;
            }

            if (config.characterType == CharacterType.SpriteSheet)
            {
                string[] data = spriteName.Split(SPRITESHEET_TEXT_SPRITE_DELIMITTER);
                Sprite[] spriteArray = new Sprite[0];

                if (data.Length == 2)
                {
                    string texturename = data[0];
                    spriteName = data[1];
                    spriteArray = Resources.LoadAll<Sprite>($"{artAssetsDirectory}/{texturename}");
                }
                else
                {
                    Debug.Log($"{artAssetsDirectory}/{SPRITESHEET_DEFAULT_SHEETNAME}");
                    spriteArray = Resources.LoadAll<Sprite>($"{artAssetsDirectory}/{SPRITESHEET_DEFAULT_SHEETNAME}");
                }
                if (spriteArray.Length == 0)
                    {
                        Debug.LogWarning($"Character '{name}' does not have an art asset called '{SPRITESHEET_DEFAULT_SHEETNAME}'");
                    }
                    return Array.Find(spriteArray, sprite => sprite.name == spriteName);
            }
            else
            {
                Debug.Log($"{artAssetsDirectory}/{spriteName}");
                return Resources.Load<Sprite>($"{artAssetsDirectory}/{spriteName}");
            }
        }

        // Méthode pour effectuer la transition d'un sprite sur une couche spécifique
        public Coroutine TransitionSprite(Sprite sprite, int layer = 0, float speed = 1){
            CharacterSpriteLayer spriteLayer = layers[layer];

            return spriteLayer.TransitionSprite(sprite, speed);
        }

        // Méthode pour afficher ou cacher le personnage avec une transition
        public override IEnumerator ShowingOrHidding(bool show, float speedMultiplier = 1f)
        {
            float targetAlpha = show ? 1f : 0;
            CanvasGroup self = rootCG;

            while (self.alpha != targetAlpha)
            {
                self.alpha = Mathf.MoveTowards(self.alpha, targetAlpha, 3f * Time.deltaTime * speedMultiplier);
                yield return null;
            }

            co_revealing = null;
            co_hidding = null;
        }

        // Méthode pour définir la couleur du personnage
        public override void SetColor(Color color)
        {
            base.SetColor(color);
            color = displayColor;

            foreach (CharacterSpriteLayer layer in layers){
                layer.StopChangingColor();
                layer.SetColor(color);
            }
        }

        // Méthode pour changer la couleur du personnage avec une transition
        public override IEnumerator ChangingColor(Color color, float speed)
        {
            foreach(CharacterSpriteLayer layer in layers){
                layer.TransitionColor(color, speed);
            }

            yield return null;

            while(layers.Any(l => l.isChangingColor)){
                yield return null;
            }

            co_changingColor = null;
        }

        // Méthode pour mettre en surbrillance le personnage avec une transition
        public override IEnumerator Highlighting(bool highlight, float speedMultiplier,  bool immediate = false)
        {
            Color targetColor = displayColor;

            foreach(CharacterSpriteLayer layer in layers){
                if(immediate)
                    layer.SetColor(targetColor);
                else 
                    layer.TransitionColor(targetColor, speedMultiplier);
            }

            yield return null;

            while(layers.Any(l => l.isChangingColor)){
                yield return null;
            }

            co_highlighting = null;

        }

        // Méthode pour orienter le personnage vers une direction
        public override IEnumerator FaceDirection(bool faceLeft, float speedMultiplier, bool immediate)
        {
            foreach(CharacterSpriteLayer layer in layers){
                if(faceLeft)
                    layer.FaceLeft(speedMultiplier, immediate);
                else
                    layer.FaceRight(speedMultiplier,immediate);
            }
            yield return null;

            while(layers.Any(l => l.isFlipping))
                yield return null;
            
            co_flipping = null;
        }

        // Méthode appelée lorsqu'une expression de casting est reçue
        public override void OnReceiveCastingExpression(int layer, string expression){
            Sprite sprite = GetSprite(expression);
            if (sprite == null){ 
                Debug.LogWarning($"Sprite '{expression}' could not be found for character '{name}'"); 
                return;
            }

            TransitionSprite(sprite, layer); // Effectue la transition vers le nouveau sprite sur la couche spécifiée
        }
    }
}
