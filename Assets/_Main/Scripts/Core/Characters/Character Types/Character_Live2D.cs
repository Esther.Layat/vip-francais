using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Live2D.Cubism.Rendering;
using Live2D.Cubism.Framework.Expression;
using System.Linq;

namespace CHARACTERS
{
    public class Character_Live2D : Character
    {
        // Constantes pour la vitesse de transition par défaut et la taille de la profondeur de tri des personnages
        public const float DEFAULT_TRANSITION_SPEED = 3F;
        public const int CHARACTER_SORTING_DEPTH_SIZE = 250;

        // Contrôleur de rendu Live2D, contrôleur d'expression et animateur
        private CubismRenderController renderController;
        private CubismExpressionController expressionController;
        private Animator montionAnimator;

        // Liste des anciens contrôleurs de rendu
        private List<CubismRenderController> oldRenderers = new List<CubismRenderController>();

        // Echelle horizontale du personnage Live2D
        private float xScale;

        public string activeExpression {get; private set;}
        public string activeMotion {get; private set;}

        // Propriété pour vérifier si le personnage est visible
        public override bool isVisible 
        {   
            get => isRevealing || renderController.Opacity == 1; 
            set => renderController.Opacity = value ? 1 : 0; 
        }

        // Constructeur prenant des paramètres supplémentaires spécifiques à Live2D
        public Character_Live2D(string name, CharacterConfigData config, GameObject prefab, string rootAssetsFolder) : base(name, config, prefab){
            Debug.Log($"Created Live2D Character : '{name}'");

            // Récupération de l'animateur et des contrôleurs de rendu et d'expression
            montionAnimator = animator.transform.GetChild(0).GetComponentInChildren<Animator>();
            renderController = montionAnimator.GetComponent<CubismRenderController>();
            expressionController = montionAnimator.GetComponent<CubismExpressionController>();

            // Sauvegarde de l'échelle horizontale initiale du personnage
            xScale = renderController.transform.localScale.x;
        }

        // Méthode pour définir le mouvement du personnage
        public void SetMotion(string animationName){
            montionAnimator.Play(animationName);
            activeMotion = animationName;
        }

        // Méthode pour définir l'expression du personnage par son index
        public void SetExpression(int expressionIndex){
            expressionController.CurrentExpressionIndex = expressionIndex;
            activeExpression = expressionIndex.ToString();
        }

        // Méthode pour définir l'expression du personnage par son nom
        public void SetExpression(string expressionName){
            expressionController.CurrentExpressionIndex = GetExpressionIndexByNaame(expressionName);
            activeExpression = expressionName;
        }

        // Méthode pour obtenir l'index de l'expression en fonction de son nom
        private int GetExpressionIndexByNaame(string expressionName){
            expressionName = expressionName.ToLower();

            for(int i = 0; i < expressionController.ExpressionsList.CubismExpressionObjects.Length; i++){
                CubismExpressionData expr = expressionController.ExpressionsList.CubismExpressionObjects[i];
                if(expr.name.Split('.')[0].ToLower() == expressionName){
                    return i;
                }
            }
            return -1;
        }

        // Méthode pour montrer ou cacher le personnage avec une transition
        public override IEnumerator ShowingOrHidding(bool show, float speedMultiplier = 1f)
        {
            float targetAlpha = show ? 1f : 0f;
            while (renderController.Opacity != targetAlpha){
                renderController.Opacity = Mathf.MoveTowards(renderController.Opacity, targetAlpha, DEFAULT_TRANSITION_SPEED*Time.deltaTime*speedMultiplier);
                yield return null;
            }

            co_revealing = null;
            co_hidding = null;
        }

        // Méthode pour définir la couleur du personnage Live2D
        public override void SetColor(Color color)
        {
            base.SetColor(color);

            foreach(CubismRenderer renderer in renderController.Renderers){
                renderer.Color = color;
            }
        }

        // Méthode pour changer la couleur du personnage avec une transition
        public override IEnumerator ChangingColor(Color color, float speed)
        {
            yield return ChangingColorL2D(color, speed);
            co_changingColor = null;
        }

        // Méthode pour mettre en surbrillance le personnage avec une transition
        public override IEnumerator Highlighting(bool highlight, float speedMultiplier,  bool immediate = false)
        {
            Color targetColor = displayColor;

            if(!isChangingColor)
            {
                if(immediate){
                    foreach(var renderer in renderController.Renderers){
                        renderer.Color = displayColor;
                    }
                }
                else 
                    yield return ChangingColorL2D(targetColor, speedMultiplier);
            }
            co_highlighting = null;
        }

        // Méthode privée pour changer la couleur avec une transition
        private IEnumerator ChangingColorL2D(Color targetColor, float speed){
            CubismRenderer[] renderers = renderController.Renderers;
            Color startColor = renderers[0].Color;

            float colorPercent = 0;
            while(colorPercent != 1){
                colorPercent  = Mathf.Clamp01( colorPercent+(DEFAULT_TRANSITION_SPEED * speed * Time.deltaTime));
                Color currentColor = Color.Lerp(startColor, targetColor, colorPercent);

                foreach(CubismRenderer renderer in renderController.Renderers){
                renderer.Color = currentColor;
                }
                yield  return null;
            }
        }

        // Méthode pour orienter le personnage vers une direction
        public override IEnumerator FaceDirection(bool faceLeft, float speedMultiplier, bool immediate)
        {
            GameObject newLive2DCharacter = CreateNewCharacterController();
            newLive2DCharacter.transform.localScale = new Vector3(faceLeft ? xScale : -xScale, newLive2DCharacter.transform.localScale.y,newLive2DCharacter.transform.localScale.z);
            renderController.Opacity = 0;
            float transitionSpeed = DEFAULT_TRANSITION_SPEED * speedMultiplier * Time.deltaTime;

            while(renderController.Opacity < 1 || oldRenderers.Any(r => r.Opacity > 0)){
                renderController.Opacity = Mathf.MoveTowards(renderController.Opacity, 1, transitionSpeed);
                foreach(CubismRenderController oldRenderer in oldRenderers){
                    oldRenderer.Opacity = Mathf.MoveTowards(oldRenderer.Opacity, 0, transitionSpeed);
                }
                yield return null;
            }

            foreach(CubismRenderController r in oldRenderers){
                UnityEngine.Object.Destroy(r.gameObject);
            }

            oldRenderers.Clear();
            co_flipping = null;
        }

        // Méthode pour créer un nouveau contrôleur de personnage Live2D
        private GameObject CreateNewCharacterController(){
            oldRenderers.Add(renderController);

            GameObject newLive2DCharacter = UnityEngine.Object.Instantiate(renderController.gameObject, renderController.transform.parent);
            newLive2DCharacter.name = name;
            renderController = newLive2DCharacter.GetComponent<CubismRenderController>();
            expressionController = newLive2DCharacter.GetComponent<CubismExpressionController>();
            montionAnimator = newLive2DCharacter.GetComponent<Animator>();

            return newLive2DCharacter;
        }

        // Méthode appelée lors du tri des personnages
        public override void OnSort(int sortingIndex)
        {
            renderController.SortingOrder = sortingIndex * CHARACTER_SORTING_DEPTH_SIZE;
        }

        // Méthode appelée lorsqu'une expression de casting est reçue
        public override void OnReceiveCastingExpression(int layer, string expression){
            SetExpression(expression) ;
        }
    }
}
