using UnityEngine;

namespace CHARACTERS
{
    public class Character_Model3D : Character
    {
        public Transform model { get; private set; }
        public Character_Model3D(string name, CharacterConfigData config, GameObject prefab, string rootAssetsFolder) : base(name, config, prefab){
            Debug.Log($"Created Model3D Character : '{name}'");
        }
    }
}
