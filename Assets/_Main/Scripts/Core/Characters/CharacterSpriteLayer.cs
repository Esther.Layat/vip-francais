using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace CHARACTERS
{
    public class CharacterSpriteLayer
    {
        // Référence au CharacterManager pour les corroutines et autres fonctionnalités
        private CharacterManager characterManager => CharacterManager.instance;

        // Vitesse par défaut de la transition entre les sprites
        private const float DEFAULT_TRANSITION_SPEED = 3f;
        private float transitionSpeedMultiplier = 1;

        // Index de la couche d'affichage du sprite
        public int layer { get; private set; } = 0;
        // Renderer de l'image du sprite
        public Image renderer { get; private set; } = null;
        // CanvasGroup du renderer pour gérer l'opacité
        public CanvasGroup rendererCG => renderer.GetComponent<CanvasGroup>();

        // Anciens renderers utilisés lors de la transition entre les sprites
        private List<CanvasGroup> oldRenderers = new List<CanvasGroup>();

        // Coroutines en cours d'exécution
        private Coroutine co_transitioningLayer = null;
        private Coroutine co_levelingAlpha = null;
        private Coroutine co_changingColor = null;
        private Coroutine co_flipping = null;
        // Orientation actuelle du sprite
        private bool isFacingLeft = Character.DEFAULT_ORIENTATION_IS_FACING_LEFT;

        // Propriétés pour vérifier l'état des coroutines
        public bool isTransitioningLayer => co_transitioningLayer != null;
        public bool isLevelingAlpha => co_levelingAlpha != null;
        public bool isChangingColor => co_changingColor != null;
        public bool isFlipping => co_flipping != null;

        // Constructeur prenant le renderer de l'image par défaut et l'index de la couche
        public CharacterSpriteLayer(Image defaultRenderer, int layer = 0)
        {
            renderer = defaultRenderer;
            this.layer = layer;
        }

        // Méthode pour définir le sprite de la couche
        public void SetSprite(Sprite sprite)
        {
            renderer.sprite = sprite;
        }

        // Méthode pour lancer la transition entre les sprites avec une vitesse optionnelle
        public Coroutine TransitionSprite(Sprite sprite, float speed = 1)
        {
            if (sprite == renderer.sprite)
                return null;

            if (isTransitioningLayer)
                characterManager.StopCoroutine(co_transitioningLayer);

            co_transitioningLayer = characterManager.StartCoroutine(TransitioningSprite(sprite, speed));
            return co_transitioningLayer;
        }

        // Coroutine pour gérer la transition entre les sprites
        private IEnumerator TransitioningSprite(Sprite sprite, float speedMultiplier)
        {
            transitionSpeedMultiplier = speedMultiplier;
            Image newRenderer = CreateRenderer(renderer.transform.parent);
            newRenderer.sprite = sprite;

            yield return TryStartLevelingAlphas();

            characterManager.StopCoroutine(co_transitioningLayer);
        }

        // Méthode pour créer un nouveau renderer pour la transition
        private Image CreateRenderer(Transform parent)
        {
            Image newRenderer = Object.Instantiate(renderer, parent);
            oldRenderers.Add(rendererCG);

            newRenderer.name = renderer.name;
            renderer = newRenderer;
            renderer.gameObject.SetActive(true);
            rendererCG.alpha = 0;

            return newRenderer;
        }

        // Méthode pour lancer la transition de l'opacité
        private Coroutine TryStartLevelingAlphas()
        {
            if (isLevelingAlpha)
                characterManager.StopCoroutine(co_levelingAlpha);

            co_levelingAlpha = characterManager.StartCoroutine(RunAlphaLeveling());
            return co_levelingAlpha;
        }

        // Coroutine pour gérer la transition de l'opacité
        private IEnumerator RunAlphaLeveling()
        {
            while (rendererCG.alpha < 1 || oldRenderers.Any(oldCG => oldCG.alpha > 0))
            {
                float speed = DEFAULT_TRANSITION_SPEED * transitionSpeedMultiplier * Time.deltaTime;
                rendererCG.alpha = Mathf.MoveTowards(rendererCG.alpha, 1, speed);

                for (int i = oldRenderers.Count - 1; i >= 0; i--)
                {
                    CanvasGroup oldCG = oldRenderers[i];
                    oldCG.alpha = Mathf.MoveTowards(oldCG.alpha, 0, speed);

                    if (oldCG.alpha <= 0)
                    {
                        oldRenderers.RemoveAt(i);
                        Object.Destroy(oldCG.gameObject);
                    }
                }

                yield return null;
            }

            characterManager.StopCoroutine(co_levelingAlpha);
        }

        // Méthode pour définir la couleur du sprite
        public void SetColor(Color color)
        {
            renderer.color = color;

            foreach (CanvasGroup oldCG in oldRenderers)
            {
                oldCG.GetComponent<Image>().color = color;
            }
        }

        // Méthode pour lancer la transition de couleur du sprite
        public Coroutine TransitionColor(Color color, float speed)
        {
            if (isChangingColor)
                characterManager.StopCoroutine(co_changingColor);

            co_changingColor = characterManager.StartCoroutine(ChangingColor(color, speed));

            return co_changingColor;
        }

        // Méthode pour arrêter la transition de couleur
        public void StopChangingColor()
        {
            if (!isChangingColor)
                return;

            characterManager.StopCoroutine(co_changingColor);
        }

        // Coroutine pour gérer la transition de couleur
        private IEnumerator ChangingColor(Color color, float speedMultiplier)
        {
            Color oldColor = renderer.color;
            List<Image> oldImages = new List<Image>();

            foreach (var oldCG in oldRenderers)
            {
                oldImages.Add(oldCG.GetComponent<Image>());
            }

            float colorPercent = 0;
            while (colorPercent < 1)
            {
                colorPercent += DEFAULT_TRANSITION_SPEED * speedMultiplier * Time.deltaTime;
                renderer.color = Color.Lerp(oldColor, color, colorPercent);

                for(int i = oldImages.Count - 1; i >= 0; i--)
                {
                    Image image = oldImages[i];
                    if(image != null)
                       image.color = renderer.color;
                    else 
                        oldImages.RemoveAt(i);
                }

                yield return null;
            }

            characterManager.StopCoroutine(co_changingColor);
        }

        // Méthode pour retourner le sprite horizontalement
        public Coroutine Flip(float speed = 1, bool immediate = false){
            if (isFacingLeft)
                return FaceRight(speed, immediate);
            else
                return FaceLeft(speed, immediate);
        }

         // Méthode pour mettre la tête à gauche
        public Coroutine FaceLeft(float speed = 1, bool immediate = false)
        {
            if (isFlipping)
                characterManager.StopCoroutine(co_flipping);

            isFacingLeft = true;
            co_flipping = characterManager.StartCoroutine(FaceDirection(isFacingLeft, speed, immediate));
            return co_flipping;
        }

         // Méthode pour mettre la tête à droite
        public Coroutine FaceRight(float speed = 1, bool immediate = false)
        {
            if (isFlipping)
                characterManager.StopCoroutine(co_flipping);

            isFacingLeft = false;
            co_flipping = characterManager.StartCoroutine(FaceDirection(isFacingLeft, speed, immediate));
            return co_flipping;
        }

        // Coroutine pour gérer l'orientation du sprite
        private IEnumerator FaceDirection(bool faceLeft, float speedMultiplier, bool immediate)
        {
            float xScale = faceLeft ? 1 : -1;
            Vector3 newScale = new Vector3(xScale, 1, 1);

            if (!immediate)
            {
                Image newRenderer = CreateRenderer(renderer.transform.parent);
                newRenderer.transform.localScale = newScale;
                transitionSpeedMultiplier = speedMultiplier;
                TryStartLevelingAlphas();
                while (isLevelingAlpha)
                    yield return null;
            }
            else
            {
                renderer.transform.localScale = newScale;
            }

            co_flipping = null;
        }

    }
}

