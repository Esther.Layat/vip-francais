using DIALOGUE;
using TMPro;
using UnityEngine;
using AYellowpaper.SerializedCollections;

namespace CHARACTERS
{
    // Classe définissant les données de configuration d'un personnage
    [System.Serializable]
    public class CharacterConfigData
    {
        public string name;
        public string alias;
        public Character.CharacterType characterType;

        public Color nameColor;
        public Color dialogueColor;
        
        public TMP_FontAsset nameFont;
        public TMP_FontAsset dialogueFont;

        public float nameFontScale = 1f;
        public float dialogueFontScale = 1f;

       [SerializedDictionary("Path / ID", "Sprite")]
        public SerializedDictionary<string, Sprite> Sprites = new SerializedDictionary<string, Sprite>();

        // Méthode pour copier les données de configuration
        public CharacterConfigData Copy(){
            CharacterConfigData result = new CharacterConfigData();
            result.name = name;
            result.alias = alias;
            result.characterType = characterType;
            result.nameFont = nameFont;
            result.dialogueFont = dialogueFont;

            result.nameColor = new Color(nameColor.r, nameColor.g, nameColor.b, nameColor.a);
            result.dialogueColor = new Color(dialogueColor.r, dialogueColor.g, dialogueColor.b, dialogueColor.a);

            result.dialogueFontScale = dialogueFontScale;
            result.nameFontScale = nameFontScale;

            return result;
        }

        // Propriétés statiques pour obtenir les valeurs par défaut de la configuration
        private static Color defaultColor => DialogueSystem.instance.config.defaultTextColor;
        private static TMP_FontAsset defaultFont => DialogueSystem.instance.config.defaultFont;
        
        private static float defaultnameFontSize => DialogueSystem.instance.config.defaultNameFontSize;
        private static float defaultdialogueFontSize => DialogueSystem.instance.config.defaultDialogueFontSize;


        // Propriété statique pour obtenir une configuration par défaut
        public static CharacterConfigData Default {
            get {
                CharacterConfigData result = new CharacterConfigData();
                result.name = "";
                result.alias = "";
                result.characterType = Character.CharacterType.Text;

                result.nameFont = defaultFont;
                result.dialogueFont = defaultFont;
                result.nameColor = new Color(defaultColor.r, defaultColor.g, defaultColor.b, defaultColor.a);
                result.dialogueColor = new Color(defaultColor.r, defaultColor.g, defaultColor.b, defaultColor.a);

                result.dialogueFontScale = 1f;
                result.nameFontScale = 1f;

                return result;
            }
        }
    }
}