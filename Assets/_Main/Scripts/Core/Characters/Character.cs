using System.Collections;
using System.Collections.Generic;
using DIALOGUE;
using TMPro;
using UnityEngine;

namespace CHARACTERS
{
    public abstract class Character
    {
        // Constantes
        public const bool ENABLE_ON_START = false; // Indique si le personnage est activé au démarrage.
        private const float UNHIGHLIGHTED_DARKEN_STRENGHT = 0.65f; // Force de l'assombrissement lorsque le personnage n'est pas en surbrillance.
        public  const bool DEFAULT_ORIENTATION_IS_FACING_LEFT = true; // Orientation par défaut du personnage (vers la gauche).
        public const string ANIMATION_REFRESH_TRIGGER = "Refresh"; // Déclencheur utilisé pour rafraîchir les animations.

        public string name = ""; // Nom du personnage.
        public string displayName = ""; // Nom affiché du personnage.
        public string castingName = "";
        public RectTransform root = null; // Référence à la racine RectTransform du personnage.
        public CharacterConfigData config;
        public Animator animator;
        public Color color { get; protected set; } = Color.white;
        protected Color displayColor => highlighted ? highlightedColor : unhighlightedColor;
        protected Color highlightedColor => color;
        protected Color unhighlightedColor => new Color(color.r * UNHIGHLIGHTED_DARKEN_STRENGHT, color.g * UNHIGHLIGHTED_DARKEN_STRENGHT, color.b * UNHIGHLIGHTED_DARKEN_STRENGHT, color.a);
        public bool highlighted { get; protected set; } = true; // Indique si le personnage est en surbrillance.
        protected bool facingLeft = DEFAULT_ORIENTATION_IS_FACING_LEFT;
        public int priority { get; protected set; } // Priorité du personnage.
        public Vector2 targetPosition { get; private set; }

        protected CharacterManager characterManager => CharacterManager.instance; // Gestionnaire de personnages.
        public DialogueSystem dialogueSystem => DialogueSystem.instance; // Système de dialogue.

        // Coroutines
        protected Coroutine co_revealing, co_hidding;
        protected Coroutine co_moving;
        protected Coroutine co_changingColor;
        protected Coroutine co_highlighting;
        protected Coroutine co_flipping;
        public bool isRevealing => co_revealing != null;
        public bool isHidding => co_hidding != null;
        public bool isMoving => co_moving != null;
        public bool isChangingColor => co_changingColor != null;
        public bool isHighlighting => (highlighted && co_highlighting != null);
        public bool isUnHighlighting => (!highlighted && co_highlighting != null);

        public virtual bool isVisible { get; set; }
        public bool isFacingLeft => facingLeft;
        public bool isFacingRight => !facingLeft;
        public bool isFlipping => co_flipping != null;


        // Constructeur
        public Character(string name, CharacterConfigData config, GameObject prefab)
        {
            this.name = name;
            displayName = name;
            this.config = config;

            if (prefab != null)
            {
                RectTransform parentPanel = null;
                switch(config.characterType){
                    case CharacterType.Sprite:
                    case CharacterType.SpriteSheet:
                        parentPanel = characterManager.characterPanel;
                        break;
                    case CharacterType.Live2D : 
                        parentPanel = characterManager.characterPanelLive2D;
                        break;
                    case CharacterType.Model3D:
                        parentPanel = characterManager.characterPanelModel3D;
                        break;
                }

                GameObject ob = Object.Instantiate(prefab, parentPanel);
                ob.name = characterManager.FormatCharacterPath(characterManager.characterPrefabNameFormat, name);
                ob.SetActive(true);
                root = ob.GetComponent<RectTransform>();
                animator = root.GetComponentInChildren<Animator>();
            }
        }

        // Méthode pour faire parler le personnage avec un seul texte
        public Coroutine Say(string dialogue) => Say(new List<string> { dialogue });

        // Méthode pour faire parler le personnage avec une liste de textes
        public Coroutine Say(List<string> dialogue)
        {
            dialogueSystem.ShowSpeakerName(displayName);
            UpdateTextCustomizationsOnScreen();
            return dialogueSystem.Say(dialogue);
        }

        // Méthodes pour définir les polices et les couleurs du texte
        public void SetNameFont(TMP_FontAsset font) => config.nameFont = font;
        public void SetDialogueFont(TMP_FontAsset font) => config.dialogueFont = font;
        public void SetNameColor(Color color) => config.nameColor = color;
        public void SetDialogueColor(Color color) => config.dialogueColor = color;

        // Méthode pour réinitialiser les données de configuration
        public void ResetConfigurationData() => config = CharacterManager.instance.GetCharacterConfig(name, getOriginal : true);

        // Méthode pour appliquer les personnalisations de texte à l'écran
        public void UpdateTextCustomizationsOnScreen() => dialogueSystem.ApplySpeakerDataToDialogueContainer(config);

        // Méthode virtuelle pour afficher le personnage
        public virtual Coroutine Show(float speedMultiplier = 1f)
        {
            if(isRevealing)
                characterManager.StopCoroutine(co_revealing);
            if (isHidding)
                characterManager.StopCoroutine(co_hidding);

            co_revealing = characterManager.StartCoroutine(ShowingOrHidding(true, speedMultiplier));
            return co_revealing;
        }

        // Méthode virtuelle pour masquer le personnage
        public virtual Coroutine Hide(float speedMultiplier = 1f)
        {
            if (isHidding)
                characterManager.StopCoroutine(co_hidding);
            if (isRevealing)
                characterManager.StopCoroutine(co_revealing);

            co_hidding = characterManager.StartCoroutine(ShowingOrHidding(false, speedMultiplier));
            return co_hidding;

        }

        // Méthode virtuelle pour montrer ou cacher le personnage
        public virtual IEnumerator ShowingOrHidding(bool show, float speedMultiplier = 1f)
        {
            Debug.Log("Show/Hide cannot be called from a base character type.");
            yield return null;
        }
        
        // Méthode virtuelle pour définir la position du personnage
        public virtual void SetPosition(Vector2 position)
        {
            if (root == null)
                return;
            (Vector2 minAnchorTarget, Vector2 maxAnchorTarget) = ConvertUITargetPositionToBeRelativecharacterAnchorTargets(position);
            root.anchorMin = minAnchorTarget;
            root.anchorMax = maxAnchorTarget;

            targetPosition = position;
        }

        // Méthode virtuelle pour déplacer le personnage vers une position
        public virtual Coroutine MoveToPosition(Vector2 position, float speed = 2f, bool smooth = false)
        {
            if (root == null)
                return null;
            if (isMoving)
                characterManager.StopCoroutine(co_moving);
            co_moving = characterManager.StartCoroutine(MovingToPosition(position, speed, smooth));
            targetPosition = position;
            return co_moving;
        }

        // Méthode pour déplacer le personnage vers une position
        public IEnumerator MovingToPosition(Vector2 position, float speed, bool smooth)
        {
            (Vector2 minAnchorTarget, Vector2 maxAnchorTarget) = ConvertUITargetPositionToBeRelativecharacterAnchorTargets(position);
            Vector2 padding = root.anchorMax - root.anchorMin;

            while (root.anchorMin != minAnchorTarget || root.anchorMax != maxAnchorTarget)
            {
                root.anchorMin = smooth ?
                    Vector2.Lerp(root.anchorMin, minAnchorTarget, speed * Time.deltaTime)
                    : Vector2.MoveTowards(root.anchorMin, minAnchorTarget, speed * Time.deltaTime * 0.35f);

                root.anchorMax = root.anchorMin + padding;

                if (smooth && Vector2.Distance(root.anchorMin, minAnchorTarget) <= 0.001f)
                {
                    root.anchorMin = minAnchorTarget;
                    root.anchorMax = maxAnchorTarget;
                    break;
                }

                yield return null;
            }

            Debug.Log("Done moving");
            characterManager.StopCoroutine(co_moving);
        }

        protected (Vector2, Vector2) ConvertUITargetPositionToBeRelativecharacterAnchorTargets(Vector2 position)
        {
            Vector2 padding = root.anchorMax - root.anchorMin;

            float maxX = 1f - padding.x;
            float maxY = 1f - padding.y;

            Vector2 minAnchorTarget = new Vector2(maxX * position.x, maxY * position.y);
            Vector2 maxAnchorTarget = minAnchorTarget + padding;

            return (minAnchorTarget, maxAnchorTarget);

        }

        // Méthode virtuelle pour changer la couleur du personnage
        public virtual void SetColor(Color color)
        {
            this.color = color;
        }

        // Méthode pour transitionner la couleur du personnage
        public Coroutine TransitionColor(Color color, float speed = 1f)
        {
            this.color = color;

            if (isChangingColor)
                characterManager.StopCoroutine(co_changingColor);

            co_changingColor = characterManager.StartCoroutine(ChangingColor(displayColor, speed));
            return co_changingColor;
        }

        public virtual IEnumerator ChangingColor(Color color, float speed)
        {
            Debug.Log("Color changing is not applicable on this character type!");
            yield return null;
        }

        // Méthode pour mettre en surbrillance le personnage
        public Coroutine Highlight(float speed = 1f, bool immediate = false)
        {
            if (isUnHighlighting || isHighlighting)
                characterManager.StopCoroutine(co_highlighting);

            highlighted = true;
            co_highlighting = characterManager.StartCoroutine(Highlighting(highlighted, speed, immediate));
            return co_highlighting;
        }

        // Méthode pour retirer la surbrillance du personnage
        public Coroutine UnHighlight(float speed = 1f, bool immediate = false)
        {
            if (isUnHighlighting || isHighlighting)
                characterManager.StopCoroutine(co_highlighting);

            highlighted = false;
            co_highlighting = characterManager.StartCoroutine(Highlighting(highlighted, speed, immediate));
            return co_highlighting;
        }

        public virtual IEnumerator Highlighting(bool highlight, float speedMultiplier, bool immediate = false){
            Debug.Log("Highlighting is not available on this character type!");
            yield return null;
        }

        // Méthode pour retourner le personnage
        public Coroutine Flip(float speed = 1, bool immediate = false){
            if (isFacingLeft)
                return FaceRight(speed, immediate);
            else
                return FaceLeft(speed, immediate);
        }

        // Méthode pour orienter le personnage vers la gauche
        public Coroutine FaceLeft(float speed = 1, bool immediate = false){
            if(isFlipping)
                characterManager.StopCoroutine(co_flipping);
            facingLeft = true;
            co_flipping = characterManager.StartCoroutine(FaceDirection(facingLeft, speed, immediate));
            return co_flipping;
        }

        // Méthode pour orienter le personnage vers la droite
        public Coroutine FaceRight(float speed = 1, bool immediate = false){
            if(isFlipping)
                characterManager.StopCoroutine(co_flipping);
            facingLeft = false;
            co_flipping = characterManager.StartCoroutine(FaceDirection(facingLeft, speed, immediate));
            return co_flipping;
        }

        // Méthode virtuelle pour orienter le personnage vers une direction
        public virtual IEnumerator FaceDirection(bool faceLeft, float speedMultiplier, bool immediate){
            Debug.Log("Cannot flip a character of this type!"); 
            yield return null;
        }

        // Méthode pour définir la priorité du personnage
        public void SetPriority (int priority, bool autoSortCharactersOnUI = true)
        {
            this.priority = priority;

            if (autoSortCharactersOnUI)
                characterManager.SortCharacters();
        }

        // Méthode pour déclencher une animation sur le personnage
        public void Animate(string animation){
            animator.SetTrigger(animation);
        }

        // Méthode pour déclencher ou arrêter une animation sur le personnage
        public void Animate(string animation, bool state){
            animator.SetBool(animation, state);
            animator.SetTrigger(ANIMATION_REFRESH_TRIGGER);
        }

        // Méthode virtuelle appelée lors du tri des personnages
        public virtual void OnSort(int sortingIndex){
            return ;
        }

        // Méthode appelée lorsqu'une expression de casting est reçue
        public virtual void OnReceiveCastingExpression(int layer, string expression){
            return;
        }

        public enum CharacterType
        {
            Text,
            Sprite,
            SpriteSheet,
            Live2D,
            Model3D
        }
    }
}
