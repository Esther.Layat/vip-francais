using System.Collections.Generic;

namespace DIALOGUE
{
    public class ConversationQueue
    {
        private Queue<Conversation> conversationQueue = new Queue<Conversation>();
        public Conversation top => conversationQueue.Peek();

        // Méthode pour ajouter une conversation à la file d'attente
        public void Enqueue(Conversation conversation) => conversationQueue.Enqueue(conversation);

        // Méthode pour ajouter une conversation avec priorité en tête de file
        public void EnqueuePriority(Conversation conversation)
        {
            Queue<Conversation> queue = new Queue<Conversation>();
            queue.Enqueue(conversation);

            while(conversationQueue.Count>0)
                queue.Enqueue(conversationQueue.Dequeue());

            conversationQueue = queue;
        }

        // Méthode pour retirer la conversation en tête de file
        public void Dequeue()
        {
            if(conversationQueue.Count > 0)
                conversationQueue.Dequeue();
            

        }

        // Méthode pour vérifier si la file d'attente est vide
        public bool IsEmpty() => conversationQueue.Count == 0;

        // Méthode pour effacer la file d'attente
        public void Clear() => conversationQueue.Clear();

        // Méthode pour obtenir une copie en lecture seule de la file d'attente sous forme de tableau
        public Conversation[] GetReadOnly() => conversationQueue.ToArray();
    }
}

