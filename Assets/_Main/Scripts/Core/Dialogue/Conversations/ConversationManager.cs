using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using COMMANDS;
using CHARACTERS;
using DIALOGUE.LogicalLines;

namespace DIALOGUE
{
    public class ConversationManager
    {
        private DialogueSystem dialogueSystem => DialogueSystem.instance;
        private Coroutine process = null;
        public bool isRunning => process != null;
        public bool isOnLogicLine { get; private set; } = false;
        public TextArchitect architect = null;
        private bool userPrompt = false;

        private TagManager tagManager;
        private LogicalLineManager logicalLineManager;

        public Conversation conversation => (conversationQueue.IsEmpty() ? null : conversationQueue.top);
        public int conversationProgress => (conversationQueue.IsEmpty() ? -1 : conversationQueue.top.GetProgress());
        
        private ConversationQueue conversationQueue;

        public bool allowUserPrompts = true;

        // Constructeur prenant un architecte de texte en argument
        public ConversationManager(TextArchitect architect){
            this.architect = architect;
            dialogueSystem.onUserPrompt_Next += OnUserPrompt_Next;

            logicalLineManager = new LogicalLineManager();

            conversationQueue = new ConversationQueue();
        }

        public Conversation[] GetConversationsQueue() => conversationQueue.GetReadOnly();

        public void Enqueue(Conversation conversation) => conversationQueue.Enqueue(conversation);
        public void EnqueuePriority(Conversation conversation) => conversationQueue.EnqueuePriority(conversation);

        // Méthode appelée lorsque l'événement de la prochaine demande utilisateur est déclenché
        private void OnUserPrompt_Next(){
            if(allowUserPrompts)
                userPrompt = true;
        }

        // Méthode pour démarrer une conversation
        public Coroutine StartConversation(Conversation conversation){
            StopConversation();
            conversationQueue.Clear();

            Enqueue(conversation);

            process = dialogueSystem.StartCoroutine(RunnigConversation());

            return process;
        }

        // Méthode pour arrêter une conversation
        public void StopConversation(){
            if(!isRunning)
                return;
            
            dialogueSystem.StopCoroutine(process);
            process = null;
        }

        // Coroutine pour exécuter une conversation
        IEnumerator RunnigConversation()
        {
            while(!conversationQueue.IsEmpty())
            {
                Conversation currentConversation = conversation;
                
                if(currentConversation.HasReachedEnd())
                {
                    conversationQueue.Dequeue();
                    continue;
                }

                string rawLine = currentConversation.CurrentLine();
                //N'afficher pas de lignes vides ou essayer d’exécuter une logique sur eux.
                if(string.IsNullOrWhiteSpace(rawLine))
                {
                    TryAdvanceConversation(currentConversation);
                    continue;
                }

                DIALOGUE_LINE line = DialogueParser.Parse(rawLine);

                if(logicalLineManager.TryGetLogic(line, out Coroutine logic))
                {
                    isOnLogicLine = true;
                    yield return logic;
                }
                else
                {
                    // Montre le dialogue
                    if(line.hasDialogue)
                        yield return Line_RunDialogue(line);

                    // Run les commandes
                    if(line.hasCommands)
                        yield return Line_RunCommands(line);

                    // Attendre l’entrée de l’utilisateur si le dialogue était dans cette ligne
                    if(line.hasDialogue){
                        // Attendre la saisie de l’utilisateur
                        yield return WaitForUserInput();

                        CommandManager.instance.StopAllProcesses();

                        dialogueSystem.OnSystemPrompt_Clear();
                    }
                }

                TryAdvanceConversation(currentConversation);
                isOnLogicLine = false;
            }

            process = null;
        }

        private void TryAdvanceConversation(Conversation conversation)
        {
            conversation.IncrementProgress();

            if(conversation != conversationQueue.top)
                return;

            if(conversation.HasReachedEnd())
                conversationQueue.Dequeue();
        }

        // Coroutine pour exécuter une ligne de dialogue
        IEnumerator Line_RunDialogue(DIALOGUE_LINE line){

            // Afficher ou masquer le nom du locuteur s’il y en a un.
            if(line.hasSpeaker)
                HandleSpeakerLogic(line.speakerData);

            //Si la boîte de dialogue n’est pas visible - assurez-vous qu’elle devient visible automatiquement
            if (!dialogueSystem.dialogueContainer.isVisible) 
                dialogueSystem.dialogueContainer.Show();

            // Établir un dialogue
            yield return BuildLineSegments(line.dialogueData);
        }

        // Méthode pour gérer la logique du locuteur
        private void HandleSpeakerLogic(DL_SPEAKER_DATA speakerData){

            bool characterMustBeCreated = speakerData.makeCharacterEnter || speakerData.isCastingPosition || speakerData.isCastingExpression;

            Character character = CharacterManager.instance.GetCharacter(speakerData.name, createIfDoesNotExist:characterMustBeCreated);

            if(speakerData.makeCharacterEnter && (!character.isVisible && !character.isRevealing)){
                character.Show();
            }

            //Ajouter le charactère de nom à l’interface utilisateur
            dialogueSystem.ShowSpeakerName(TagManager.Inject(speakerData.displayname));

            DialogueSystem.instance.ApplySpeakerDataToDialogueContainer(speakerData.name);

            if(speakerData.isCastingPosition)
                character.MoveToPosition(speakerData.castPosition);

            //Cast Expression
            if(speakerData.isCastingExpression){
                foreach(var ce in speakerData.CastExpressions){
                    character.OnReceiveCastingExpression(ce.layer, ce.expression);
                }
            }
            
        }

        // Coroutine pour exécuter les commandes d'une ligne de dialogue
        IEnumerator Line_RunCommands(DIALOGUE_LINE line){
            List<DL_COMMAND_DATA.Command> commands = line.commandData.commands;

            foreach(DL_COMMAND_DATA.Command command in commands){
                if(command.waitForCompletion || command.name == "wait"){
                    CoroutineWrapper cw = CommandManager.instance.Execute(command.name, command.arguments);
                    while(!cw.IsDone){
                        if(userPrompt){
                            CommandManager.instance.StopCurrentProcess();
                            userPrompt = false;
                        }
                        yield return null;
                    }
                }
                else 
                    CommandManager.instance.Execute(command.name, command.arguments);
            }
            yield return null;
        }

        // Coroutine pour construire les segments de ligne de dialogue
        IEnumerator BuildLineSegments(DL_DIALOGUE_DATA line)
        {
            for(int i = 0; i < line.segments.Count; i++){
                DL_DIALOGUE_DATA.DIALOGUE_SEGMENT segment = line.segments[i];
                yield return WaitForDialogueSegmentSignalToBeTrigger(segment);
                yield return BuildDialogue(segment.dialogue, segment.appendText);
            }
        }
        
        public bool isWaitingOnAutoTimer{get; private set;} = false;

        // Coroutine pour attendre que le signal de segment de dialogue soit déclenché
        IEnumerator WaitForDialogueSegmentSignalToBeTrigger(DL_DIALOGUE_DATA.DIALOGUE_SEGMENT segment){
            switch(segment.startSignal){
                case DL_DIALOGUE_DATA.DIALOGUE_SEGMENT.StartSignal.C:
                    yield return WaitForUserInput();
                    dialogueSystem.OnSystemPrompt_Clear();
                    break;
                case DL_DIALOGUE_DATA.DIALOGUE_SEGMENT.StartSignal.A:
                    yield return WaitForUserInput();
                    break;
                case DL_DIALOGUE_DATA.DIALOGUE_SEGMENT.StartSignal.WC:
                    isWaitingOnAutoTimer = true;
                    yield return new WaitForSeconds(segment.signalDelay);
                    isWaitingOnAutoTimer = false;
                    dialogueSystem.OnSystemPrompt_Clear();
                    break;
                case DL_DIALOGUE_DATA.DIALOGUE_SEGMENT.StartSignal.WA:
                    isWaitingOnAutoTimer = true;
                    yield return new WaitForSeconds(segment.signalDelay);
                    isWaitingOnAutoTimer = false;
                    break;
                default:
                    break;

            }
        }

        // Coroutine pour construire le dialogue
        IEnumerator BuildDialogue(string dialogue, bool append = false){
            dialogue = TagManager.Inject(dialogue);

            // Établir le dialogue
            if(!append)
                architect.Build(dialogue);
            else 
                architect.Append(dialogue);

            // Attendre que le dialogue soit terminé.
            while(architect.isBuilding){
                if(userPrompt){
                    if(!architect.hurryUp)
                        architect.hurryUp = true;
                    else 
                        architect.ForceComplete();

                    userPrompt = false;
                }
                yield return null;
            }
        }

        // Coroutine pour attendre que l'utilisateur appuie sur une touche
        IEnumerator WaitForUserInput(){

            dialogueSystem.prompt.Show();

            while(!userPrompt){
                yield return null;
            }

            dialogueSystem.prompt.Hide();

            userPrompt = false ;
        }
    }
}
