using System.Collections.Generic;


namespace DIALOGUE
{
    public class Conversation
    {
        private List<string> lines = new List<string>();
        private int progress = 0;

        public string file { get; private set; }
        public int fileStartIndex { get; private set; }
        public int fileEndIndex { get; private set; }


        // Constructeur pour initialiser une conversation avec une liste de lignes et une progression optionnelle
        public Conversation(List<string> lines, int progress = 0, string file = "", int fileStartIndex = -1, int fileEndIndex = -1)
        {
            this.lines = lines;
            this.progress = progress;
            this.file = file;

            if(fileStartIndex == -1)
                fileStartIndex = 0;
            if(fileEndIndex == -1)
                fileEndIndex = lines.Count - 1;
            
            this.fileStartIndex = fileStartIndex;
            this.fileEndIndex = fileEndIndex;

        }

        // Méthode pour obtenir la progression actuelle
        public int GetProgress() => progress;

        // Méthode pour définir la progression actuelle
        public void SetProgress(int value) => progress = value;

        // Méthode pour incrémenter la progression de 1
        public void IncrementProgress() => progress++;

        // Propriété pour obtenir le nombre total de lignes dans la conversation
        public int Count => lines.Count;

        // Méthode pour obtenir la liste des lignes de dialogue
        public List<string> GetLines() => lines;

        // Méthode pour obtenir la ligne actuelle de dialogue en fonction de la progression
        public string CurrentLine()
        {
            if (progress >= 0 && progress < lines.Count)
            {
                return lines[progress];
            }
            else
            {
                // Retourne null ou gère le cas lorsque la progression est hors limites
                return null;
            }
        }

        // Méthode pour vérifier si la fin de la conversation est atteinte
        public bool HasReachedEnd() => progress >= lines.Count;

    }
}

