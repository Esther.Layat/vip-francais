
using System.Text.RegularExpressions;



namespace DIALOGUE{
    public class DialogueParser
    {

        // Modèle de regex pour identifier les commandes dans la ligne brute
        private const string commandRegexPattern = @"[\w\[\]]*[^\s]\(";

        // Méthode statique pour analyser une ligne brute de dialogue
        public static DIALOGUE_LINE Parse(string rawLine)
        {
            // Debug.Log($"Parsing Line - '{rawLine}'");
            (string speaker, string dialogue, string commands) = RipContent(rawLine);
            // Debug.Log($"Speaker = '{speaker}'\nDialogue = '{dialogue}'\nCommands = '{commands}'");

            //Nous devons injecter des balises et des variables dans le haut-parleur et dialoguer séparément car il y a des vérifications initiales qui doivent 
            //être effectuées.
            //Mais les commandes n’ont pas besoin de vérifications, nous pouvons donc y injecter les variables dès maintenant.
            commands = TagManager.Inject(commands);

            return new DIALOGUE_LINE(rawLine, speaker, dialogue, commands);
        }

        // Méthode interne pour extraire les informations pertinentes de la ligne brute
        private static (string, string, string) RipContent(string rawLine){
            string speaker = "", dialogue = "", commands = "";
            
            int dialogueStart = -1;
            int dialogueEnd = -1;
            bool isEscaped = false;

            for (int i = 0; i < rawLine.Length; i++)
            {
                char current = rawLine[i];
                if(current == '\\')
                    isEscaped = !isEscaped;
                else if (current == '"' && !isEscaped){
                    if(dialogueStart == -1){
                        dialogueStart = i;
                    }else if(dialogueEnd == -1){
                        dialogueEnd = i;
                    }
                }
                else 
                    isEscaped = false;
            }

            // Identifie le partern des commandes
            Regex commandRegex = new Regex(commandRegexPattern);
            MatchCollection matches = commandRegex.Matches(rawLine);
            int commandStart = -1;
            foreach(Match match in matches){
                if(match.Index < dialogueStart || match.Index > dialogueEnd){

                    commandStart = match.Index;
                    break;
                }
            }

            if(commandStart != -1 && (dialogueStart == -1 && dialogueEnd == -1))
                return("","",rawLine.Trim());

            // Si nous sommes ici, nous avons un dialogue ou un argument multi-mots dans une commande. 
            // Déterminez si c’est un dialogue.
            if(dialogueStart != -1 && dialogueEnd != -1 && (commandStart == -1 || commandStart > dialogueEnd)){
                //nous savons que nous avons un dialogue valable
                speaker = rawLine.Substring(0, dialogueStart).Trim();
                dialogue = rawLine.Substring(dialogueStart + 1, dialogueEnd - dialogueStart - 1).Replace("\\\"", "\"");
                if(commandStart != -1){
                    commands = rawLine.Substring(commandStart).Trim();
                }

            }
            else if (commandStart != -1 && dialogueStart > commandStart){
                commands= rawLine;
            } else 
                dialogue = rawLine;

            // Debug.Log($"DS={dialogueStart}, DE={dialogueEnd}, CS={commandStart}");
            // Debug.Log($"{commands}");
            return (speaker, dialogue, commands);
        }
    }
}

