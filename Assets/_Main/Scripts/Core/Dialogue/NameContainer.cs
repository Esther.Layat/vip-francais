
using TMPro;
using UnityEngine;

namespace DIALOGUE {
    //La zone qui contient le texte du nom à l’écran. Fait partie du conteneur de dialogue.
    [System.Serializable]
    public class NameContainer 
    {
        // Référence à la racine de l'objet UI contenant le nom
        [SerializeField] private GameObject root;

        // Référence au composant TextMeshProUGUI utilisé pour afficher le nom
        [field : SerializeField] public TextMeshProUGUI nameText {get; private set;}

        // Méthode pour afficher le nom avec une couleur spécifiée
        public void Show(string nameToShow = ""){
            root.SetActive(true);
            if(nameToShow != string.Empty)
                nameText.text = nameToShow;
        }

        // Méthode pour cacher le nom
        public void Hide(){
            root.SetActive(false);
        }

        // Méthode pour définir la couleur du texte du nom
        public void SetNameColor(Color color) => nameText.color = color;

        // Méthode pour définir la police du texte du nom
        public void SetNameFont(TMP_FontAsset font) => nameText.font = font;

        public void SetNameFontSize(float size) => nameText. fontSize = size;
    }
}

