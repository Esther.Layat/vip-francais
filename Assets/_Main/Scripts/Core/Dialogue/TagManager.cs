using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using VISUALNOVEL;

public class TagManager 
{
    private static readonly Dictionary<string, Func<string>> tags = new Dictionary<string,Func<string>>()
    {
        {"<mainChar>", () => "Avira"},
        {"<time>", () => DateTime.Now.ToString("hh:mm tt")},
        {"<playerLevel>", () => "15"},
        {"<nameCharacter1>", () => VNGameSave.activeFile.playerName},
        {"<nameCharacter2>", () => VNGameSave.activeFile.playerName2},
        {"<tempVal1>", () => "42"}
    };
    // Expression régulière pour rechercher des balises dans une chaîne de texte
    private static readonly Regex tagRegex = new Regex("<\\w+>");

    // Méthode pour injecter les valeurs des balises dans une chaîne de texte
    public static string Inject(string text, bool injectTags = true, bool injectVariables = true)
    {
        if(injectTags)
            text = InjectTags(text);

        if(injectVariables)
            text = InjectVariables(text);

        return text;
    }

    // Méthode privée pour remplacer les balises par leurs valeurs correspondantes
    private static string InjectTags(string value)
    {
        if(tagRegex.IsMatch(value)){
            foreach(Match match in  tagRegex.Matches(value)){
                if(tags.TryGetValue(match.Value, out var tagValueRequest)){
                    value = value.Replace(match.Value, tagValueRequest());
                }
            }
        }

        return value;
    }

    // Méthode privée pour remplacer les variables par leurs valeurs correspondantes
    private static string InjectVariables(string value)
    {
        var matches = Regex.Matches(value, VariableStore.REGEX_VARIABLE_IDS);
        var matchesList = matches.Cast<Match>().ToList();

        for(int i = matchesList.Count - 1; i >= 0; i--)
        {
            var match = matchesList[i];
            string variableName = match.Value.TrimStart(VariableStore.VARIABLE_ID, '!');
            bool negate = match.Value.StartsWith('!');

            bool endInIllegalCharacter = variableName.EndsWith(VariableStore.DATABASE_VARIABLE_RELATIONAL_ID);
            if(endInIllegalCharacter)
                variableName = variableName.Substring(0, variableName.Length - 1);
            UnityEngine.Debug.Log(variableName);

            if(!VariableStore.TryGetValue(variableName, out var variableValue))
            {
                UnityEngine.Debug.LogError($"Variable {variableName} not found in string assignment");
                continue;
            }

            if(negate && variableValue is bool)
                variableValue = !(bool)variableValue;

            int lenghtToBeRemoved = match.Index + match.Length > value.Length ? value.Length - match.Index : match.Length;
            if(endInIllegalCharacter)
                lenghtToBeRemoved -= 1;
                
            value = value.Remove(match.Index, lenghtToBeRemoved);
            value = value.Insert(match.Index, variableValue.ToString());
        }

        return value;
    }
}
