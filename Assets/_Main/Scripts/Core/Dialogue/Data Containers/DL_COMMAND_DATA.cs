
using System.Collections.Generic;
using System.Text;

namespace DIALOGUE
{
    // Classe représentant les données de commande pour une ligne de dialogue
    public class DL_COMMAND_DATA
    {
        public List<Command> commands;

        // Délimiteur pour séparer les commandes
        private const char COMMANDSPLITTER_ID = ',';
        // Délimiteur pour séparer les arguments de la commande
        private const char ARGUMENTSCONTAINER_ID = '(';
        // Identifiant pour la commande d'attente
        private const string WAITCOMMAND_ID = "[wait]";

        // Structure représentant une commande
        public struct Command
        {
            public string name;
            public string[] arguments;
            public bool waitForCompletion;
        }

        // Constructeur prenant une chaîne brute de commandes comme argument
        public DL_COMMAND_DATA(string rawCommands)
        {
            commands = RipCommands(rawCommands);
        }

        // Méthode pour extraire les commandes à partir de la chaîne brute de commandes
        private List<Command> RipCommands(string rawCommands)
        {
            string[] data = rawCommands.Split(COMMANDSPLITTER_ID, System.StringSplitOptions.RemoveEmptyEntries);
            List<Command> result = new List<Command>();

            foreach (string cmd in data)
            {
                Command command = new Command();
                int index = cmd.IndexOf(ARGUMENTSCONTAINER_ID);
                command.name = cmd.Substring(0, index).Trim();

                if (command.name.ToLower().StartsWith(WAITCOMMAND_ID))
                {
                    command.name = command.name.Substring(WAITCOMMAND_ID.Length);
                    command.waitForCompletion = true;
                }
                else
                    command.waitForCompletion = false;

                command.arguments = GetArgs(cmd.Substring(index + 1, cmd.Length - index - 2));
                result.Add(command);
            }

            return result;
        }

        // Méthode pour extraire les arguments de la commande à partir de la chaîne d'arguments brute
        private string[] GetArgs(string args)
        {
            List<string> argList = new List<string>();
            StringBuilder currentArg = new StringBuilder();
            bool inQuotes = false;

            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == '"')
                {
                    inQuotes = !inQuotes;
                    continue;
                }

                if (!inQuotes && args[i] == ' ')
                {
                    argList.Add(currentArg.ToString());
                    currentArg.Clear();
                    continue;
                }

                currentArg.Append(args[i]);
            }

            if (currentArg.Length > 0)
                argList.Add(currentArg.ToString());

            return argList.ToArray();
        }
    }
}
