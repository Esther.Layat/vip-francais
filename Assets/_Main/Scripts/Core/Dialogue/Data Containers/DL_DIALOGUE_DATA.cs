using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;


namespace DIALOGUE
{
    public class DL_DIALOGUE_DATA
    {
        public string rawData { get; private set; } = string.Empty;
        public List<DIALOGUE_SEGMENT> segments;

        // Modèle pour identifier les segments de dialogue dans la chaîne brute
        private const string segmentIdentifierPattern = @"\{[ca]\}|\{w[ca]\s\d*\.?\d*\}";

        // Constructeur prenant une chaîne brute de dialogue comme argument
        public DL_DIALOGUE_DATA(string rawDialogue)
        {
            this.rawData = rawDialogue;
            segments = RipSegments(rawDialogue);
        }

        // Méthode pour extraire les segments de dialogue à partir de la chaîne brute
        public List<DIALOGUE_SEGMENT> RipSegments(string rawDialogue)
        {
            List<DIALOGUE_SEGMENT> segments = new List<DIALOGUE_SEGMENT>();
            MatchCollection matches = Regex.Matches(rawDialogue, segmentIdentifierPattern);

            int lastIndex = 0;
            // Trouver le premier ou le seul segment dans le fichier
            DIALOGUE_SEGMENT segment = new DIALOGUE_SEGMENT();
            segment.dialogue = (matches.Count == 0 ? rawDialogue : rawDialogue.Substring(0, matches[0].Index));
            segment.startSignal = DIALOGUE_SEGMENT.StartSignal.NONE;
            segment.signalDelay = 0;
            segments.Add(segment);

            if (matches.Count == 0)
                return segments;
            else
                lastIndex = matches[0].Index;

            for (int i = 0; i < matches.Count; i++)
            {
                Match match = matches[i];
                segment = new DIALOGUE_SEGMENT();

                // Obtenir le signal de départ du segment
                string signalMatch = match.Value; //{A}
                signalMatch = signalMatch.Substring(1, match.Length - 2);
                string[] signalSplit = signalMatch.Split(' ');

                segment.startSignal = (DIALOGUE_SEGMENT.StartSignal)Enum.Parse(typeof(DIALOGUE_SEGMENT.StartSignal), signalSplit[0].ToUpper());

                // Obtenir le signal du retard
                if (signalSplit.Length > 1)
                    float.TryParse(signalSplit[1], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out segment.signalDelay);

                // Obtenir le dialogue pour le segment
                int nextIndex = i + 1 < matches.Count ? matches[i + 1].Index : rawDialogue.Length;
                segment.dialogue = rawDialogue.Substring(lastIndex + match.Length, nextIndex - (lastIndex + match.Length));
                lastIndex = nextIndex;

                segments.Add(segment);
            }
            return segments;
        }

        // Structure représentant un segment de dialogue
        public struct DIALOGUE_SEGMENT
        {
            public string dialogue;
            public StartSignal startSignal;
            public float signalDelay;

            // Enumération des signaux de début
            public enum StartSignal { NONE, C, A, WA, WC }

            // Indique si le texte doit être ajouté au texte précédent
            public bool appendText => (startSignal == StartSignal.A || startSignal == StartSignal.WA);
        }
    }
}