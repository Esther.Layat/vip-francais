using System.Collections;
using UnityEngine;
using TMPro;

namespace DIALOGUE
{
    public class AutoReader : MonoBehaviour
    {
        // Constantes pour la vitesse de lecture automatique
        private const int DEFAULT_CHARACTERS_READ_PER_SECOND = 18;
        private const float READ_TIME_PADDING = 0.5f;
        private const float MAX_READ_TIME = 99f;
        private const float MIN_READ_TIME = 1f;
        private const string STATUS_TEXT_AUTO = "Auto";
        private const string STATUS_TEXT_SKIP = "Skipping";

        // Référence au gestionnaire de conversation
        private ConversationManager conversationManager;
        private TextArchitect architect => conversationManager.architect;

        // Propriétés pour activer ou désactiver la lecture automatique et pour définir la vitesse
        public bool skip { get; set; } = false;
        public float speed { get; set; } = 1f;

        // Indique si la lecture automatique est activée
        public bool isOn => co_running != null;
        private Coroutine co_running = null;

        // Référence au texte d'état affichant le statut de la lecture automatique
        [SerializeField] private TextMeshProUGUI statusText;
        [HideInInspector] public bool allowToggle = true;

        // Méthode pour initialiser le lecteur automatique avec le gestionnaire de conversation
        public void Initialize(ConversationManager conversationManager)
        {
            this.conversationManager = conversationManager;

            statusText.text = string.Empty;
        }

        // Méthode pour activer la lecture automatique
        public void Enable(){
            if(isOn)
                return;
            co_running = StartCoroutine(AutoRead());
        }

        // Méthode pour désactiver la lecture automatique
        public void Disable(){
            if(!isOn)
                return;
            StopCoroutine(co_running);
            skip = false;
            co_running = null;
            statusText.text = string.Empty;
        }

        // Coroutine pour gérer la lecture automatique
        private IEnumerator AutoRead(){
            // Ne rien faire s’il n’y a pas de conversation à surveiller
            if(!conversationManager.isRunning)
            {
                Disable();
                yield break;
            }

            if(!architect.isBuilding && architect.currentText != string.Empty)
                DialogueSystem.instance.OnSystemPrompt_Next();
            
            while(conversationManager.isRunning)
            {
                // Lit et attend
                if(!skip)
                {
                    while(!architect.isBuilding && !conversationManager.isWaitingOnAutoTimer)
                        yield return null;

                    float timeStarted = Time.time;

                    while(architect.isBuilding || conversationManager.isWaitingOnAutoTimer)
                        yield return null;

                    float timeToRead = Mathf.Clamp(((float)architect.tmpro.textInfo.characterCount / DEFAULT_CHARACTERS_READ_PER_SECOND), MIN_READ_TIME, MAX_READ_TIME);
                    timeToRead = Mathf .Clamp((timeToRead - (Time.time -timeStarted)), MIN_READ_TIME, MAX_READ_TIME);
                    timeToRead = (timeToRead / speed) + READ_TIME_PADDING;

                    yield return new WaitForSeconds(timeToRead);
                }
                // Skip
                else 
                {
                    architect.ForceComplete();
                    yield return new WaitForSeconds(0.05f);
                }
                DialogueSystem.instance.OnSystemPrompt_Next();
            }

            Disable();
        }

        // Méthode pour activer ou désactiver la lecture automatique en fonction de l'état actuel
        public void Toggle_Auto()
        {
            if(!allowToggle)
                return;

            bool prevState = skip;
            skip = false;

            if(prevState)
                Enable();
            else 
            {
                if(!isOn)
                    Enable();
                else 
                    Disable();
            }
            if(isOn)
                statusText.text = STATUS_TEXT_AUTO;
        }

        // Méthode pour activer ou désactiver le saut de la lecture automatique en fonction de l'état actuel
        public void Toggle_Skip()
        {
            if(!allowToggle)
                return;
                
            bool prevState = skip;
            skip = true;

            if(!prevState)
                Enable();
            else 
            {
                if(!isOn)
                    Enable();
                else 
                    Disable();
            }
            if(isOn)
                statusText.text = STATUS_TEXT_SKIP;
        }
    }
}
