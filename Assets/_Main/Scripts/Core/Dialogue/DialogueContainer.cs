using UnityEngine;
using TMPro;

namespace DIALOGUE{

    // Cette classe représente un conteneur pour les éléments associés à l'affichage du dialogue
    [System.Serializable]
    public class DialogueContainer
    {
        private const float DEFAULT_FADE_SPEED = 2f;

        public GameObject root; // GameObject racine du conteneur
        public NameContainer nameContainer; // Conteneur pour le nom du personnage parlant
        public TextMeshProUGUI dialogueText; // Composant TextMeshProUGUI pour afficher le texte du dialogue

        private CanvasGroupController cgController;

        
        // Méthode pour définir la couleur du texte du dialogue
        public void SetDialogueColor(Color color) => dialogueText.color = color;

        // Méthode pour définir la police du texte du dialogue
        public void SetDialogueFont(TMP_FontAsset font) => dialogueText.font = font;
        
        // Méthode pour définir la taille de la police du texte du dialogue
        public void SetDialogueFontSize(float size) => dialogueText.fontSize = size;

        private bool initialized = false;
        public void Initialize(){
            if (initialized)
                return;

            cgController = new CanvasGroupController(DialogueSystem.instance, root.GetComponent<CanvasGroup>());
        }

        public bool isVisible => cgController.isVisible;
        public Coroutine Show(float speed = 1f, bool immediate = false) => cgController.Show(speed, immediate);
        public Coroutine Hide(float speed = 1f, bool immediate = false) => cgController.Hide(speed, immediate);

    }
}
