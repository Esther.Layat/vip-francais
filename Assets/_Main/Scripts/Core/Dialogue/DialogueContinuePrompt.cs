
using TMPro;
using UnityEngine;

namespace DIALOGUE
{
    public class DialogueContinuePrompt : MonoBehaviour
    {
        private RectTransform root;

        [SerializeField] private Animator anim;
        [SerializeField] private TextMeshProUGUI tmpro;

        public bool isShowing => anim.gameObject.activeSelf;

        // Start is called before the first frame update
        void Start()
        {
            root = GetComponent<RectTransform>();
        }

        // Méthode pour afficher le prompt
        public void Show(){
            if(tmpro.text == string.Empty){
                if(isShowing)
                    Hide();

                return;
            }

            tmpro.ForceMeshUpdate();

            // Obtient la position du coin inférieur droit du dernier caractère affiché
            anim.gameObject.SetActive(true);
            root.transform.SetParent(tmpro.transform);

            // Calcule la position cible pour le prompt en ajoutant la moitié de la largeur du caractère
            TMP_CharacterInfo finalCharacter = tmpro.textInfo.characterInfo[tmpro.textInfo.characterCount - 1];
            Vector3 tragetPos = finalCharacter.bottomRight;
            float characterWidth = finalCharacter.pointSize * 0.5f;
            tragetPos = new Vector3(tragetPos.x + characterWidth, tragetPos.y, 0);

            root.localPosition = tragetPos;
        }
        
        // Méthode pour cacher le prompt
        public void Hide(){
            anim.gameObject.SetActive(false);
        }
    }
}

