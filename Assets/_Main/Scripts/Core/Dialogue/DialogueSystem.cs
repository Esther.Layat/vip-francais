
using System.Collections.Generic;
using UnityEngine;
using CHARACTERS;


namespace DIALOGUE{
    public class DialogueSystem : MonoBehaviour
    {
        // Référence aux paramètres de configuration du système de dialogue
        [SerializeField] private DialogueSystemConfigurationSO _config;
        public DialogueSystemConfigurationSO config => _config;

        // Conteneur pour les éléments d'interface utilisateur du dialogue
        public DialogueContainer dialogueContainer = new DialogueContainer();

        // Gestionnaire des conversations
        public ConversationManager conversationManager {get; private set;}

        // Gestionnaire pour la construction du texte
        private TextArchitect architect;

        public AutoReader autoReader {get; private set;}

        [SerializeField] private CanvasGroup mainCanvas;
        
        // Instance unique du DialogueSystem
        public static DialogueSystem instance {get; private set;}

        // Événement déclenché lorsqu'une prompte utilisateur est terminée
        public delegate void DialogueSystemEvent();
        public event DialogueSystemEvent onUserPrompt_Next;
        public event DialogueSystemEvent onClear;

        // Vérifie si une conversation est en cours
        public bool isRunningConversiation => conversationManager.isRunning;

        public DialogueContinuePrompt prompt;
        private CanvasGroupController cgController;

        private void Awake(){
            if(instance == null){
                instance = this;
                Initialize();
            }
            else 
                DestroyImmediate(gameObject);
        }

        // Vérifie si le système est initialisé
        bool _initialized = false;
        private void Initialize(){
            if (_initialized){
                return;
            }

            architect = new TextArchitect(dialogueContainer.dialogueText);
            conversationManager = new ConversationManager(architect);

            cgController = new CanvasGroupController(this, mainCanvas);
            dialogueContainer.Initialize();

            autoReader = GetComponent<AutoReader>();
            if(autoReader != null)
                autoReader.Initialize(conversationManager);
        }

        // Méthode déclenchée lorsqu'une prompte utilisateur est terminée
        public void OnUserPrompt_Next(){
            onUserPrompt_Next?.Invoke();

            if(autoReader != null && autoReader.isOn)
                autoReader.Disable();
        }

        // Méthode déclenchée lorsqu'une prompte système est terminée
        public void OnSystemPrompt_Next(){
            onUserPrompt_Next?.Invoke();
            
        }

        // Méthode déclenchée lorsqu'un prompt système de nettoyage est terminée
        public void OnSystemPrompt_Clear()
        {
            onClear?.Invoke();
        }

        // Méthode appelée lorsque l'affichage de l'historique commence
        public void OnStartViewingHistory()
        {
            prompt.Hide();
            autoReader.allowToggle = false;
            conversationManager.allowUserPrompts = false;

            if(autoReader.isOn)
                autoReader.Disable();
        }

        // Méthode appelée lorsque l'affichage de l'historique se termine
        public void OnStopViewingHistory()
        {
            prompt.Show();
            autoReader.allowToggle = true;
            conversationManager.allowUserPrompts = true;
        }

        // Applique les données du locuteur au conteneur de dialogue
        public void ApplySpeakerDataToDialogueContainer(string speakerName){
            Character character= CharacterManager.instance.GetCharacter(speakerName);
            CharacterConfigData config = character != null ? character.config : CharacterManager.instance.GetCharacterConfig(speakerName);

            ApplySpeakerDataToDialogueContainer(config);

        }

        // Surcharge de la méthode pour appliquer directement les données du personnage au conteneur de dialogue
        public void ApplySpeakerDataToDialogueContainer(CharacterConfigData config){
            //Définir les détails du dialogue
            dialogueContainer.SetDialogueColor(config.dialogueColor); 
            dialogueContainer.SetDialogueFont(config.dialogueFont); 
            float fontSize = this.config.defaultDialogueFontSize * this.config.dialogueFontScale * config.dialogueFontScale;
            dialogueContainer.SetDialogueFontSize(fontSize);

            //Définir les détails du nom
            dialogueContainer.nameContainer.SetNameColor(config.nameColor); 
            dialogueContainer.nameContainer.SetNameFont(config.nameFont);
            fontSize = this.config.defaultNameFontSize * config.nameFontScale;
            dialogueContainer.nameContainer.SetNameFontSize(fontSize);
        }
        
        // Affiche le nom du locuteur dans le conteneur de dialogue
        public void ShowSpeakerName(string speakerName = "") {
            if(speakerName.ToLower() != "pensées")
                dialogueContainer.nameContainer.Show(speakerName);
            else 
            {
                HideSpeakerName();
                dialogueContainer.nameContainer.nameText.text = "";
            }
                
        }

        // Cache le nom du locuteur dans le conteneur de dialogue
        public void HideSpeakerName() => dialogueContainer.nameContainer.Hide();

        // Méthode pour commencer une conversation avec un seul dialogue
        public Coroutine Say(string speaker, string dialogue)
        {
            List<string> conversation = new List<string>() {$"{speaker} \"{dialogue}\""};
            return Say(conversation);
        }

        // Méthode pour commencer une conversation avec une liste de dialogues
        public Coroutine Say(List<string> lines, string filePath = "")
        {
            Conversation conversation = new Conversation(lines, file: filePath);
            return conversationManager.StartConversation(conversation);
        }

        public Coroutine Say(Conversation converstaion)
        {
            return conversationManager.StartConversation(converstaion);
        }

        public bool isVisible => cgController.isVisible;
        public Coroutine Show(float speed = 1f, bool immediate = false) => cgController.Show(speed, immediate);
        public Coroutine Hide(float speed = 1f, bool immediate = false) => cgController.Hide(speed, immediate);
    }
}
