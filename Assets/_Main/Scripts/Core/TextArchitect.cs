using System.Collections;
using UnityEngine;
using TMPro;

public class TextArchitect
{
    // Références aux objets TextMeshPro
    private TextMeshProUGUI tmpro_ui;
    private TextMeshPro tmpro_world;

    // Propriété pour récupérer le TextMeshPro actif
    public TMP_Text tmpro => tmpro_ui != null ? tmpro_ui : tmpro_world;

    // Propriété pour obtenir le texte actuel
    public string currentText => tmpro.text;

    // Propriétés pour le texte cible et le texte préexistant
    public string targetText { get; private set;} = "";
    public string preText { get; private set;} = "";
    
    // Longueur du texte préexistant
    private int preTextLenght = 0;

    // Propriété pour obtenir le texte complet (préexistant + cible)
    public string fullTargetText => preText + targetText;

    // Méthodes de construction du texte
    public enum BuildMethod {instant, typewriter, fade}
    public BuildMethod buildMethod = BuildMethod.typewriter;

    // Propriété pour la couleur du texte
    public Color textColor { get { return tmpro.color;}set { tmpro.color = value;}}

    // Propriété pour la vitesse de construction du texte
    public float speed { get { return baseSpeed * speedMultiplier; } set {speedMultiplier = value;}}
    private const float baseSpeed = 1;
    private float speedMultiplier = 1; 

    // Nombre de caractères à afficher par cycle de construction
    public int characterPerCycle {get { return speed <= 2f ? characterMultiplier : speed <= 2.5f ? characterMultiplier*3 : characterMultiplier*3;}}
    private int characterMultiplier = 1; 

    // Option pour accélérer la construction
    public bool hurryUp = false;

    public TextArchitect(TextMeshProUGUI tmpro_ui){
        this.tmpro_ui = tmpro_ui;
    }
    
    public TextArchitect(TextMeshPro tmpro_world){
        this.tmpro_world = tmpro_world;
    }

    // Méthode pour commencer la construction du texte
    public Coroutine Build(string text){
        preText = "";
        targetText = text;

        Stop();
        buildProcess = tmpro.StartCoroutine(Building());
        return buildProcess;
    }

    // Méthode pour ajouter du texte à la fin du texte existant
    public Coroutine Append(string text){
        preText = tmpro.text;
        targetText = text;

        Stop();
        buildProcess = tmpro.StartCoroutine(Building());
        return buildProcess;
    }

    // Méthode pour définir directement le texte
    public void SetText(string text){
        preText = "";
        targetText = text;

        Stop();

        tmpro.text = targetText;
        tmpro.ForceMeshUpdate();
        tmpro.maxVisibleCharacters = tmpro.textInfo. characterCount;
    }


    // Coroutine pour la construction progressive du texte
    private Coroutine buildProcess = null;
    public bool isBuilding => buildProcess != null;

    // Méthode pour arrêter la construction du texte
    public void Stop(){
        if(!isBuilding)
            return;
        
        tmpro.StopCoroutine(buildProcess);
        buildProcess = null;
    }

    // Coroutine pour la construction du texte
    IEnumerator Building(){
        Prepare();
        switch (buildMethod){
            case BuildMethod.typewriter:
                yield return Build_Typewriter();
                break;
            case BuildMethod.fade : 
                yield return Build_Fade();
                break;
        }
        OnComplete();
    }

    // Méthode appelée lorsque la construction est terminée
    private void OnComplete(){
        buildProcess = null;
        hurryUp = false; 
    }

    // Méthode pour forcer la fin de la construction
    public void ForceComplete(){
        switch(buildMethod){
            case BuildMethod.typewriter :
                tmpro.maxVisibleCharacters = tmpro.textInfo.characterCount;
                break;
            case BuildMethod.fade : 
                tmpro.ForceMeshUpdate();
                break;
        }
        Stop();
        OnComplete();
    }

    // Prépare le texte avant la construction
    private void Prepare(){
        switch(buildMethod){
            case BuildMethod.instant:
                Prepare_Instant();
                break;
            case BuildMethod.typewriter:
                Prepare_Typerwriter();
                break;
            case BuildMethod.fade:
                Prepare_Fade();
                break;
        }
    }

    // Prépare le texte pour une construction instantanée
    private void Prepare_Instant(){
        tmpro.color = tmpro.color;
        tmpro.text = fullTargetText;
        tmpro.ForceMeshUpdate();
        tmpro.maxVisibleCharacters = tmpro.textInfo.characterCount;
    }

    // Prépare le texte pour une construction en mode machine à écrire
    private void Prepare_Typerwriter(){
        tmpro.color = tmpro.color;
        tmpro.maxVisibleCharacters = 0;
        tmpro.text = preText;

        if(preText != ""){
            tmpro.ForceMeshUpdate();
            tmpro.maxVisibleCharacters = tmpro.textInfo.characterCount;
        }
        tmpro.text += targetText;
        tmpro.ForceMeshUpdate();
    }

    // Prépare le texte pour une construction avec fondu
    private void Prepare_Fade(){
        tmpro.text = preText;
        if(preText != ""){
            tmpro.ForceMeshUpdate();
            preTextLenght = tmpro.textInfo.characterCount;
        } 
        else 
            preTextLenght = 0;
            
        tmpro.text += targetText;
        tmpro.maxVisibleCharacters = int.MaxValue;
        tmpro.ForceMeshUpdate();

        TMP_TextInfo textInfo = tmpro.textInfo;

        Color colorVisable = new Color(textColor.r, textColor.g, textColor.b, 1);
        Color colorHidden = new Color(textColor.r, textColor.g, textColor.b, 0);

        Color32[] vertexColors = textInfo.meshInfo[textInfo.characterInfo[0].materialReferenceIndex].colors32;
        for (int i = 0; i < textInfo.characterCount; i++){
            TMP_CharacterInfo charInfo = textInfo.characterInfo[i];

            if (!charInfo.isVisible)
                continue;

            if (i < preTextLenght){
                for (int v = 0; v < 4; v++){
                    vertexColors[charInfo.vertexIndex + v] = colorVisable;
                }
            } else {
                for (int v = 0; v < 4; v++){
                    vertexColors[charInfo.vertexIndex + v] = colorHidden;
                }
            }

            tmpro.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);
        }
    }

    // Coroutine pour la construction du texte en mode machine à écrire
    private IEnumerator Build_Typewriter(){
        while(tmpro.maxVisibleCharacters <tmpro.textInfo.characterCount){
            tmpro.maxVisibleCharacters += hurryUp? characterPerCycle * 5 : characterPerCycle;
            yield return new WaitForSeconds(0.015f/ speed);
        }

    }

    // Coroutine pour la construction du texte en mode fondu
    private IEnumerator Build_Fade(){
        int minRange = preTextLenght;
        int maxRange = minRange + 1;

        byte alphaThreshold = 15;
        TMP_TextInfo textInfo = tmpro.textInfo;

        Color32[] vertexColors = textInfo.meshInfo[textInfo.characterInfo[0].materialReferenceIndex].colors32;
        float[] alphas = new float[textInfo.characterCount];

        while(true){

            float fadeSpeed = ((hurryUp? characterPerCycle * 5 : characterPerCycle) * speed)*4f;

            for(int i = minRange; i < maxRange; i++){
                TMP_CharacterInfo charInfo = textInfo.characterInfo[i];

                if (!charInfo.isVisible)
                    continue;

                int vertexIndex = textInfo.characterInfo[i].vertexIndex;
                alphas[i] = Mathf.MoveTowards(alphas[i], 255, fadeSpeed);

                for (int v = 0; v < 4; v++){
                    vertexColors[charInfo.vertexIndex + v].a = (byte)alphas[i];
                }

                if (alphas[i] >= 255)
                    minRange++;
            }

            tmpro.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);

            bool LastCharacterIsInvisible = !textInfo.characterInfo[maxRange - 1].isVisible;
            if (alphas[maxRange-1] > alphaThreshold || LastCharacterIsInvisible){
                if (maxRange < textInfo.characterCount){
                    maxRange++;
                }else if (alphas[maxRange -1]>=255 || LastCharacterIsInvisible){
                    break;
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }

}

