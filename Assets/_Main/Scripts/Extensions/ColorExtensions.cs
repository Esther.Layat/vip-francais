using UnityEngine;

public static class ColorExtensions
{
    // Méthode d'extension pour définir l'alpha d'une couleur
    public static Color SetAlpha(this Color original, float alpha){
        return new Color(original.r, original.g, original.b, alpha);
    }

    // Méthode d'extension pour obtenir une couleur à partir de son nom
    public static Color GetColorFromName(this Color original,string colorName){
        switch (colorName. ToLower()){
            // Cas pour chaque couleur prédéfinie
            case "red":
                return Color.red;
            case "green":
                return Color.green;
            case "blue":
                return Color.blue;
            case "yellow":
                return Color.yellow;
            case "white":
                return Color.white;
            case "black":
                return Color.black;
            case "gray":
                return Color.gray;
            case "cyan":
                return Color. cyan;
            case "magenta":
                return Color.magenta;
            case "orange":
                return new Color (1f, 0.5f, 0f); // Orange n’est pas une couleur prédéfinie, nous la créons donc manuellement
            default:
                Debug.LogWarning("Unrecognized color name:" + colorName); 
                return Color.clear;
        }
    }



}
