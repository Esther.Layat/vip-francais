PlayAmbience(RainyMood), wait(1)

enter Calista "So, tell me. Do you like dogs or cats? <mainChar>" 
ShowGalleryImage(-m 18)

choice "What Pet Do You Prefer?"
{
    -Dogs
        Calista "Dogs are wonderful. They give you so much undeserved love!" 
        <mainChar> "Do you have a dog?"
        Calista "No, but one day I would love to have a big fluffy one!"
        choice "Choice 2"
        {
            -Yes
                Load (Chapter1 -enqueue true)
            -No
        }
        Calista "No, but one day I would love to have a big fluffy one!"
        Calista "No!"
    -Arachnids
        Calista "Ew! - Why spiders?!" 
        choice "Why Spiders?"
        {
            -Have you ever seen a baby jumping spider?
                Calista "Oh - yeah, youre right. Those are pretty cute."
                "But from a distance."
            -Spiders aren't the only arachnids, you know.
                Calista "If its got eight legs, it's close enough!"
            -They're cool.
                Calista "Well, we can be friends as long as you keep them away from me."
                "I don't want any of that eight-legged freak action..."
        }
        <mainChar> "Say what you will, I like them."
    -Cats
        Calista "I love cats! {a}Well, {wa 0.5} when they aren't knocking over things..."
        "But they can be so graceful at times."
        "...{c}Except when they're knocking over things, haha!"
    -I Prefer Chinchillas
        Calista "Oh, they're cute."
        "Did you know they take dust baths? I had no idea!"

}
HideGalleryImage()
Pensées "What is your name?"
input "What is your Name, bruh?"
SetPlayerName(<nameCharacter1>)
Pensées "Nice to meet you, <nameCharacter1>!"
ShowGalleryImage(flower)

<nameCharacter1> "What is your name ?"
input "What is your Name, bruh? "
SetPlayerName2(<nameCharacter2>)
<nameCharacter1> "Nice to meet you, <nameCharacter2>!"
HideGalleryImage()

Pensées "The time is <time> and you are only level <playerLevel>"
<nameCharacter1> "And so begin the adventures of <nameCharacter2>"
<nameCharacter1> "My name is <nameCharacter1>. {c} What time is it?"
<mainChar> "Fuggedaboutit!<tempVal1>"


SetLayerMedia(background Nebula), SetLayerMedia(background 1 SpaceshipInterior)
enter Calista "Yes! It worked!  <nameCharacter2>"

[wait]HideUI(-spd 0.5)
wait(1)
[wait]ShowUI(-i true)
"Done <nameCharacter1>"
wait(5)

Calista.Flip()
wait(2)
[wait]HideDB(-spd 0.1)
wait(2)
ShowDB(-i true)

enter Maman "Yes! It worked!"

Calista.Animate(Hop)
wait(2)

[wait]Calista.Animate(Shiver -s true), Maman.Move(0.7 -spd 0.7)


Maman [Maman_Surprise2] "Okay - first of all... {wa 0.5} Cold showers are not <i>nice</i> I don't care what"
Maman.FaceRight()
wait(2)
Maman.FaceLeft()

Calista.Move(0.3 -spd 0.7)
Calista.Animate(Shiver -s false)
Calista [Calista_Happy] "Hey - I think I see something over there!"

Maman "It's a lodge! Let's go!"
HideDB()
PlayAmbience(RainyMood)
PlaySong("Calm")
Calista.Move(1.5 -spd-2), Maman.Move(1.5 -spd 2), Maman.FaceRight() Calista.Hide(), Maman.Hide()
wait(3)
ClearLayerMedia(background -i true), wait(1) StopSong(), StopAmbience()
wait(1)

SetLayerMedia(background 03_2), PlaySong(Calm2)
Calista.Show(), Maman.Show(), Calista.SetPosition(-0.5), Maman.SetPosition(-0.5), Calista.FaceRight()
Calista.Move(0.3 -spd 1.5), Maman.Move(0 -spd- 1.5)
wait(1)

Maman "Finaly, out of that storm!{c}Now let's just hope they have a room here."
Calista "Let's find out. {c}Hello?{a} Is anyone here?"


HideDB()
wait(3)

Papa "Where did she go?"

Maman "I don't know!"

Papa "Did we miss her?"

Maman "I said, I don't know."

PlayAmbience(RainyMood)
PlaySong("Calm")
PlayVoice(wakeup)
enter Calista "Yes! It worked!"

StopSong("Calm")
StopAmbience(RainyMood)

wait(3)

playsfx(thunder_strong_01 -p 0.85 -v 0.3 -loop true)
Pensées "Did you know that astraphobia is the fear of thunder and lightning?"

stopsfx(thunder_strong_01)

wait(1)

SetLayerMedia(background Nebula), SetLayerMedia(background 1 SpaceshipInterior)

Calista "Welcome!"

ClearLayerMedia(background -blend hypno)

Calista "Did we clear the background?"

SetLayerMedia(cinematic -m "~/Graphics/Gallery/flower" -i true)

Calista "Have a look at the Cinematic Layer, huh?"

ClearLayerMedia(cinematic -spd 0.6 -blend hypno)

Calista "The cinematic layer should be cleared!"


CreateCharacter("Papa as Papa" )

Maman.SetPosition(1), Calista.SetPosition(0)
wait(1)

[wait]Unhighlight(Calista Maman), wait(1), [wait]Highlight(Calista Maman), wait(1)

Papa.Show(-i true), wait (0.5)

[wait]Highlight(Calista -o true), wait(1), [wait]Unhighlight(Calista -o true), wait(1)

Maman.SetSprite(Maman_Surprise2 -spd 0.75)

wait(1)

Maman.SetSprite(Maman_Triste -spd- 0.75)

wait(1), Sort(Calista Maman Papa), 

Calista.SetColor(red), wait(1)

[wait]MoveCharacter(Calista 1 -0.5 -spd 0.5)

[wait]Calista.Hide(), [wait]Calista.Show()

[wait]Calista.UnHighlight(), [wait]Papa.UnHighlight()
[wait]Calista.Highlight(), [wait]Maman.Highlight()

Calista.Unhighlight(-i true), Papa.Unhighlight(-i true)
wait(1)
Calista.Highlight(-i true), Papa.Unhighlight(-i true)
wait(1)

[wait]Calista.Move(0 1 -spd 0.5)

wait(1), Sort(Calista Maman Papa), wait(1)

Hide(Calista Papa Maman -spd 0.1)
Calista "Now we are invisible"

Show(Calista Papa Maman -spd 0.1)
wait(1), Sort(Maman Papa Calista), wait(1)

Calista "Thank you for joining me! {c}I'm looking forward to writing some commands with you!"
MoveCharacter (Papa 0 -spd 0.1), MoveCharacter (Maman 0.1 -spd 0.1), Hide(Calista)

Papa "Where did she go?"

Maman "I don't know!"

Papa "Did we miss her?"

Maman "I said, I don't know."