#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using CHARACTERS;
using UnityEngine;

public class InputPanelTesting : MonoBehaviour
{
    public InputPanel inputPanel;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Running());
    }

    IEnumerator Running()
    {
        Character Calista = CharacterManager.instance.CreateCharacter("Calista", revealAfterCreation : true);

        yield return Calista.Say("Hi! What's your name?");

        inputPanel.Show("What is your name?");

        while(inputPanel.isWaitingOnUserInput)
            yield return null;

        string characterName = inputPanel.lastInput;

        yield return Calista.Say($"It's very nice to meet you, {characterName}");
    }
}
#endif