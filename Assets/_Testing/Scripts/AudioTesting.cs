#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using CHARACTERS;
using DIALOGUE;
using UnityEngine;

namespace TESTING
{

    public class AudioTesting : MonoBehaviour
    {
        //Start is called before the first frame update
        void Start() {
            StartCoroutine(Running());
        }

        Character CreateCharacter(string name) => CharacterManager.instance. CreateCharacter (name);

        IEnumerator Running2(){
            Character_Sprite Calista = CreateCharacter("Calista") as Character_Sprite;
            Character Me = CreateCharacter("Me");
            Calista.Show();

            AudioManager.instance.PlaySoundEffect("Audio/SFX/RadioStatic", loop : true); 
            yield return Me.Say("Please turn off the radio.");

            yield return new WaitForSeconds(0.5f);

            AudioManager.instance.StopSoundEffect("RadioStatic");
            AudioManager.instance.PlayVoice("Audio/Voices/Exclamation");
            Calista.Say("Okay!");

            yield return new WaitForSeconds (10f);

            AudioManager.instance.PlaySoundEffect("Audio/SFX/thunder_strong_01");

            yield return new WaitForSeconds (1f);
            Calista.Animate("Hop");
            Calista.TransitionSprite(Calista.GetSprite("Calista_Surprise"));
            Calista.Say("Yikes!");
        }

        IEnumerator Running(){
            yield return new WaitForSeconds (1);
            Character_Sprite Calista = CreateCharacter ("Calista") as Character_Sprite;
            Calista.Show();

            yield return DialogueSystem.instance.Say("Pensées", "Can we see your ship?");

            GraphicPanelManager.instance.GetPanel("background").GetLayer(0, true).SetTexture("Graphics/BG Images/villagenight");
            AudioManager.  instance.PlayTrack("Audio/Ambience/RainyMood",0);
            AudioManager.instance.PlayTrack("Audio/Music/Calm",1, pitch : 0.7f);

            yield return Calista.Say("We can have multiple channels for playing ambience as well as music!");
            AudioManager.instance.StopTrack(1) ;
            yield return Calista.Say("Yes, of course!");

            Calista.SetSprite(Calista.GetSprite("Calista_Heureuse"));
            Calista.MoveToPosition(new Vector2(0.7f, 0), speed: 0.5f); 
            yield return Calista.Say("Yes, of course!");

            yield return Calista.Say("Let me show you the engine room.");

            GraphicPanelManager.instance.GetPanel("background").GetLayer(0, true).SetTexture("Graphics/BG Images/EngineRoom");
            AudioManager.instance.PlayTrack("Audio/Music/Comedy", volumeCap : 0.8f);

            yield return null;
        }
    }
}
#endif