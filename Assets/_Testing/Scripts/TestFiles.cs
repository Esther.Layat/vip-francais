#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestFiles : MonoBehaviour
{
    [SerializeField] private TextAsset fileName;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Run());
    }

    IEnumerator Run()
    {
        List<string> lines = FileManager.ReadTextAsset(fileName, false);
        
        foreach (string line in lines)
            Debug.Log(line);

        yield return null;
    }
}
#endif