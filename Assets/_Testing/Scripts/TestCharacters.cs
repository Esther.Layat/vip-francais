#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CHARACTERS;
using DIALOGUE;
using TMPro;

namespace TESTING
{
    public class TestCharacters : MonoBehaviour
    {
        public TMP_FontAsset tempFont;
        private Character CreateCharacter(string name) => CharacterManager.instance.CreateCharacter(name, revealAfterCreation: true);
        // Start is called before the first frame update
        void Start()
        {
            // Character Calista = CharacterManager.instance.CreateCharacter("Maman");
            // Character Calista2 = CharacterManager.instance.CreateCharacter("Calista");
            // Character Adam = CharacterManager.instance.CreateCharacter("Adam");
            StartCoroutine(Test());
        }

        IEnumerator Test(){
            
            Character_Sprite Calista = CreateCharacter("Calista") as Character_Sprite;
            Character_Live2D Rice = CreateCharacter("Rice") as Character_Live2D;
            Character_Live2D Mao = CreateCharacter("Mao") as Character_Live2D;
            Character_Live2D Natori = CreateCharacter("Natori") as Character_Live2D;
            Character_Live2D Koharu = CreateCharacter("Koharu") as Character_Live2D; //Creer une erreur mais fonctionne

            Rice.SetPosition(new Vector2(0.3f, 0));
            Mao.SetPosition(new Vector2(0.4f, 0));
            yield return new WaitForSeconds(1);

            CharacterManager.instance.SortCharacters(new string[] {"Mao", "Rice"});
            
            yield return new WaitForSeconds(1);
            Rice. SetPriority(5);

            yield return new WaitForSeconds(1);
            CharacterManager.instance.SortCharacters(new string[] {"Rice", "Mao"});
            Mao.SetPriority(8);


            // Calista. SetPosition(Vector2.zero);
            // Rice.SetPosition(new Vector2(1, 0)); 
            yield return new WaitForSeconds(0.3f);

            Calista.FaceRight();
            Rice.FaceRight();
            yield return new WaitForSeconds(0.5f);
            Calista.Flip();
            Rice.Flip();
            yield return new WaitForSeconds(0.5f);

            Calista.Hide();
            Rice.Hide();
            yield return new WaitForSeconds(0.5f);

            Calista.Show();
            Rice.Show();
            yield return new WaitForSeconds(1f);

            Calista.TransitionColor(Color.red);
            Rice.TransitionColor(Color.red);
            yield return new WaitForSeconds(0.5f);

            Calista.TransitionColor(Color.white);
            Rice.TransitionColor(Color.white);
            yield return new WaitForSeconds(0.5f);

            Calista.UnHighlight();
            Rice. Highlight();
            yield return new WaitForSeconds(0.5f);

            Calista.Highlight();
            Rice.UnHighlight();

            yield return null;
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
#endif